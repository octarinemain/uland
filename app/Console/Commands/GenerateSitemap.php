<?php
namespace App\Console\Commands;

use App\Models\Advert;
use Illuminate\Console\Command;
use Spatie\Sitemap\SitemapGenerator;
use  Spatie\Sitemap\Sitemap;


class GenerateSitemap extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate the sitemap.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        SitemapGenerator::create('https://uland.ua')->writeToFile('sitemap.xml');
        $adverts = Advert::where('search_status', 'Активно')->get();
        $slug_buy_regions = Advert::where('search_status', 'Активно')->where('type', 'Продаж')->where('slug_region', '!=', '')->where('slug_region', '!=', 'ne-viznacheno')->groupBy('slug_region')->get();
        $slug_buy_cities = Advert::where('search_status', 'Активно')->where('type', 'Продаж')->where('slug_city', '!=', '')->where('slug_city', '!=', 'ne-viznacheno')->groupBy('slug_city')->get();
        $slug_rent_regions = Advert::where('search_status', 'Активно')->where('type', 'Оренда')->where('slug_region', '!=', '')->where('slug_region', '!=', 'ne-viznacheno')->groupBy('slug_region')->get();
        $slug_rent_cities = Advert::where('search_status', 'Активно')->where('type', 'Оренда')->where('slug_city', '!=', '')->where('slug_city', '!=', 'ne-viznacheno')->groupBy('slug_city')->get();

        $sitemap = Sitemap::create()
            ->add('/')
            ->add('/articles')
            ->add('/customer/check-cadnum')
            ->add('/customer/login')
            ->add('/instruktsiya-z-publikatsii-ogoloshennya')
            ->add('/instruktsiya-z-reestratsii-v-sistemi')
            ->add('/platni-poslugi')
            ->add('/politika-konfidentsialnosti')
            ->add('/register-step-1')
            ->add('/reset-step-1')
            ->add('/search')
            ->add('/search-agent')
            ->add('/support')
            ->add('/umovi-vikoristannya')
            ->add('/vidkrittya-proektu-uland')
            ->add('/yak-pratsyuvati-z-sistemoyu');

        foreach ($adverts as $advert) {
            $sitemap->add('https://uland.ua/advert/id='.$advert->id);
        }

        foreach ($slug_buy_regions as $slug_buy_region) {
            $sitemap->add('https://uland.ua/prodaja-zemli/'.$slug_buy_region->slug_region);
        }

        foreach ($slug_buy_cities as $slug_buy_city) {
            $sitemap->add('https://uland.ua/prodaja-zemli/'.$slug_buy_city->slug_city);
        }

        foreach ($slug_rent_regions as $slug_rent_region) {
            $sitemap->add('https://uland.ua/arenda-zemli/'.$slug_rent_region->slug_region);
        }

        foreach ($slug_rent_cities as $slug_rent_city) {
            $sitemap->add('https://uland.ua/arenda-zemli/'.$slug_rent_city->slug_city);
        }
        $sitemap->writeToFile('sitemap.xml');
    }
}
