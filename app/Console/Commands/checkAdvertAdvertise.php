<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Advert;
use App\Models\Customer;


class checkAdvertAdvertise extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'advert:checkAdvertAdvertise';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Проверяем обяьвления на рекламу';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle()
    {
        $once_adverts = Advert::where('search_status', 'Активно')->where('once_top_day', '!=', NULL)->where('once_top_day', '!=', '0000-00-00 00:00:00')->get();
        /*
        * Check advertise for stopping in list UP
        */
        foreach($once_adverts as $advert){
            $date_end = date('Y-m-d', strtotime($advert->once_top_day));
            $date_end_hour = date('H', strtotime($advert->once_top_day));
            $date_now = date('Y-m-d');
            $date_now_hour = date('H');

            if($date_end == $date_now){
                if($date_end_hour == $date_now_hour){
                    Advert::where('id', $advert->id)->update([
                        'once_top_day' => NULL,
                    ]);
                }
            }else{
                if($date_end_hour == $date_now_hour){
                    Advert::where('id', $advert->id)->update([
                        'sort_date' => date('Y-m-d H:i:s'),
                    ]);
                }
            }
        }


        $other_adverts = Advert::where('search_status', 'Активно')->get();
        foreach($other_adverts as $advert){
            $date_end_top = date('Y-m-d', strtotime($advert->top));
            $date_end_red = date('Y-m-d', strtotime($advert->red));
            $date_end_hour_top = date('H', strtotime($advert->top));
            $date_end_hour_red = date('H', strtotime($advert->red));
            $date_now = date('Y-m-d');
            $date_now_hour = date('H');
            /*
             * Check advertise TOP for stopping
             */
            if($advert->top != NULL && $advert->top != '0000-00-00 00:00:00'){
                if($date_end_top == $date_now){
                    if($date_end_hour_top == $date_now_hour){
                        Advert::where('id', $advert->id)->update([
                            'top' => NULL,
                        ]);
                    }
                }
            }
            /*
             * Check advertise RED LINE for stopping
             */
            if($advert->red != NULL && $advert->red != '0000-00-00 00:00:00'){
                if($date_end_red == $date_now){
                    if($date_end_hour_red == $date_now_hour){
                        Advert::where('id', $advert->id)->update([
                            'red' => NULL,
                        ]);
                    }
                }
            }

        }
    }
}
