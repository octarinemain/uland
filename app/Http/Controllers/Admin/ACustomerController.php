<?php

namespace App\Http\Controllers\Admin;

use App\Models\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class ACustomerController extends Controller
{
    public function filter(Request $request){

        $query = Customer::orderBy('id', 'DESC')->where('id', '>', 1);

        if($request->surname){
            $query->where('surname', 'LIKE', '%' . $request->surname . '%');
        }
        if($request->phone){
            $query->where('phone', 'LIKE', '%' . $request->phone . '%');
        }

        $customers = $query->get();
        return $customers;
    }

    public function getCount(){
        return Customer::where('is_new', 1)->get()->count();
    }
}
