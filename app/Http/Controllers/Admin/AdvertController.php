<?php

namespace App\Http\Controllers\Admin;

use App\Models\Advert;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Mail\AdvertiseMessage;



class AdvertController extends Controller
{

    public function create(Request $request)
    {
        if($request->advert_id){
            Advert::where('id', $request->advert_id)->update([
                'search_status' => $request->search_status,
                'region' => $request->region,
                'district' => $request->district,
                'city' => $request->city,
                'slug_region' => str_slug($request->region, '-'),
                'slug_district' => str_slug($request->district, '-'),
                'slug_city' => str_slug($request->city, '-'),
            ]);
        }

        return response([
            'status' => 'success',
        ], 200);

    }

    public function filter(Request $request){

        $query = Advert::orderBy('id', 'DESC')->where('id', '>', 0);

        if($request->cadnum){
            $query->where('cadnum',  'LIKE', '%' . $request->cadnum . '%');
        }
        if($request->type){
            $query->where('type',  $request->type);
        }
        if($request->search_status){
            $query->where('search_status', $request->search_status);
        }
        if($request->region){
            $query->where('region', $request->region);
        }
        $adverts = $query->get();
        $adverts->load('customer');
        $adverts->load('agent_customer');
        return $adverts;
    }

    public function getCount(){
        return Advert::where('search_status', 'На модерації')->get()->count();
    }

    public function updateCount(Request $request){
        return Advert::where('id', $request->id)->update([
            'is_new' => 0
        ]);
    }

    public function unfastenAgent(Request $request){
        $advert = Advert::find($request->id);
        if($advert){
            Advert::where('id', $request->id)->update([
                'agent_id' => 0
            ]);
            return redirect()->back();
        }else{
            $title = '404';
            return view('frontend.pages.404',compact(['title']));
        }
    }

    public function unfastenOwner(Request $request){
        $advert = Advert::find($request->id);
        if($advert){
            Advert::where('id', $request->id)->update([
                'customer_id' => 0
            ]);
            return redirect()->back();
        }else{
            $title = '404';
            return view('frontend.pages.404',compact(['title']));
        }
    }

    public function updateRed(Request $request){

        Advert::with(['customer', 'agent_customer'])->where('id', $request->advert_id)->update([
            'red' => date('Y-m-d H:i:s',strtotime($request->date.' 23:00:00')),
            'sort_date' => date('Y-m-d H:i:s'),
        ]);

        if($request->send == 'Да'){

            $advert = Advert::find($request->advert_id);
            if($advert->customer){
                $email = $advert->customer->email;
            }else{
                $email = $advert->agent_customer->email;
            }
            Mail::to($email)->send(new AdvertiseMessage());
        }
    }

    public function updateOrder(Request $request){

        Advert::with(['customer', 'agent_customer'])->where('id', $request->advert_id)->update([
            'once_top_day' => date('Y-m-d H:i:s',strtotime($request->date.' 23:00:00')),
            'sort_date' => date('Y-m-d H:i:s'),
        ]);

        if($request->send == 'Да'){

            $advert = Advert::find($request->advert_id);

            if($advert->customer){
                $email = $advert->customer->email;
            }else{
                $email = $advert->agent_customer->email;
            }
            Mail::to($email)->send(new AdvertiseMessage());
        }
    }

    public function updateTop(Request $request){

        Advert::with(['customer', 'agent_customer'])->where('id', $request->advert_id)->update([
            'top' => date('Y-m-d H:i:s',strtotime($request->date.' 23:00:00')),
            'sort_date' => date('Y-m-d H:i:s'),
        ]);

        if($request->send == 'Да'){

            $advert = Advert::find($request->advert_id);

            if($advert->customer){
                $email = $advert->customer->email;
            }else{
                $email = $advert->agent_customer->email;
            }
            Mail::to($email)->send(new AdvertiseMessage());
        }
    }
}
