<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\ArticleRequest;
use App\Models\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;



class ArticleController extends Controller
{

    public function create(ArticleRequest $request)
    {
        if($request->article_id){
            $article = Article::where('id', $request->article_id)->first();

            if($request->preview){
                $image = $article->preview;
                if($image){
                    Storage::delete('Article/' . $image);
                }
                $file = $request->preview;
                $file->store('Article');
                $preview = $file->hashName();

                Article::where('id', $request->article_id)->update([
                    'title' => $request->title,
                    'slug' => str_slug($request->title, '-'),
                    'description' => $request->description,
                    'small_description' => $request->small_description,
                    'meta_keywords' => $request->meta_keywords,
                    'meta_description' => $request->meta_description,
                    'preview' => $preview
                ]);
            }else{
                Article::where('id', $request->article_id)->update([
                    'title' => $request->title,
                    'slug' => str_slug($request->title, '-'),
                    'description' => $request->description,
                    'small_description' => $request->small_description,
                    'meta_keywords' => $request->meta_keywords,
                    'meta_description' => $request->meta_description,
                ]);
            }
        }else{
            $preview = $request->preview;
            $preview->store('Article');
            $preview = $preview->hashName();

            Article::create([
                'preview' => $preview,
                'title' => $request->title,
                'slug' => str_slug($request->title, '-'),
                'description' => $request->description,
                'small_description' => $request->small_description,
                'meta_keywords' => $request->meta_keywords,
                'meta_description' => $request->meta_description,
            ]);
        }

        return response([
            'status' => 'success'
        ], 200);

    }

    public function delete($id)
    {
        $article = Article::where('id', $id)->first();
        Storage::delete('Article/' . $article->preview);
        $article->delete();
        return response([
            'status' => 'success',
        ], 200);
    }

}
