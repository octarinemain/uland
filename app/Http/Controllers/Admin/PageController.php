<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\PageRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Support\Facades\DB;



class PageController extends Controller
{
    public function create(PageRequest $request)
    {

        if($request->page_id){
            Page::where('id', $request->page_id)->update([
                'title' => $request->title,
                'slug' => str_slug($request->title, '-'),
                'description' => $request->description,
                'meta_keywords' => $request->meta_keywords,
                'meta_description' => $request->meta_description,
            ]);
        }else{
            Page::create([
                'title' => $request->title,
                'slug' => str_slug($request->title, '-'),
                'description' => $request->description,
                'meta_keywords' => $request->meta_keywords,
                'meta_description' => $request->meta_description,
            ]);
        }

        return response([
            'status' => 'success',
        ], 200);

    }

    public function delete($id)
    {
        $page = Page::where('id', $id)->first();
        $page->delete();
        return response([
            'status' => 'success',
        ], 200);
    }



}
