<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Support;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;


class SupportController extends Controller
{
    public function delete($id)
    {
        $support = Support::where('id', $id)->first();
        Storage::delete('File/' . $support->file);
        $support->delete();
        return response([
            'status' => 'success',
        ], 200);
    }

    public function create(Request $request)
    {
        Support::where('id', $request->support_id)->update([
            'status' => $request->status
        ]);
        return response([
            'status' => 'success',
        ], 200);
    }



}
