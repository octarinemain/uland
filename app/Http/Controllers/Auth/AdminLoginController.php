<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Http\Requests\Admin\LoginFormRequest;



class AdminLoginController extends Controller
{

    public function showLoginForm()
    {
        return view('auth.admin-login');
    }

    public function login(Request $request)
    {
        if(Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember))
        {
            $url = '/admin/customers';
            return response([
                'status' => 'success',
                'url' => $url
            ], 200);
        }else{
            return response([
                'custom_error' => 'You entered incorrect data, please try again.',
            ], 422);
        }
    }
}
