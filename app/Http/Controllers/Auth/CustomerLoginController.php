<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Frontend\Login;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use \Validator;
use Illuminate\Support\Facades\Input;



class CustomerLoginController extends Controller
{
//    public function __construct()
//    {
//        $this->middleware('guest:customer');
//    }

    public function showLoginForm()
    {
        $title = 'Авторизація';
        return view('auth.customer-login',compact(['title']));
    }

    public function login(Login $request)
    {
        if(Auth::guard('customer')->attempt(['phone' => $request->phone, 'password' => $request->password], $request->remember))
        {
            return response()->json([
                'profile'
            ]);
        }else{
            $error = array();
            $error['login_error'][0] = 'Ви ввели неправильні дані, спробуйте ще раз.';
            return response()->json([
                'errors' => $error
            ], 422);
        }
    }
}
