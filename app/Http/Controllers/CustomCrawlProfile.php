<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Models\Advert;
use Spatie\Crawler\CrawlProfile;
use Psr\Http\Message\UriInterface;
use  Spatie\Sitemap\Sitemap;


class CustomCrawlProfile extends CrawlProfile
{
    public function shouldCrawl(UriInterface $url): bool
    {
        return true;
    }

}
