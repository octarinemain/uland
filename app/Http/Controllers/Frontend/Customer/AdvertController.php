<?php

namespace App\Http\Controllers\Frontend\Customer;

use App\Http\Requests\Frontend\CheckCadnum;
use App\Http\Requests\Frontend\createAdvert;
use App\Http\Requests\Frontend\sendMessagetoOwner;
use App\Mail\DetachAgent;
use App\Mail\sendMessagetoOwner as NewMessage;
use App\Models\AdvertHistory;
use App\Models\AdvertImage;
use App\Models\Favorite;
use App\Models\Payment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\LiqPay;
use App\Models\Advert;
use Auth;
use App\Models\Customer;
use App\Events\CustomerRegistered;
use \Session;
use Illuminate\Support\Facades\Storage;
use Mail;
use Intervention\Image\ImageManagerStatic as Image;


class AdvertController extends Controller
{
    public function CheckCadnum(CheckCadnum $request)
    {
        Session::forget('cadnum_info');
        Session::forget('cadnum_coordinate');
        Session::forget('cadnum');

        Session::push('cadnum', $request->cadnum);
        $cadnum_info = $this->getCadnumInfo($request->cadnum);


        if (!empty($cadnum_info->data[0])) {

            $parcel = substr($request->cadnum, 18, 4);
            $quartal = substr($request->cadnum, 15, 2);
            $zone = substr($request->cadnum, 11, 2);
            $koatuu = substr($request->cadnum, 0, 10);

            $cadnum_info->data[0]->koatuu = $koatuu;
            $cadnum_info->data[0]->parcel = $parcel;
            $cadnum_info->data[0]->quartal = $quartal;
            $cadnum_info->data[0]->zone = $zone;

            /*
             * Convert api area field to numeric or remove all string value
             */
            $cadnum_info->data[0]->area = str_replace(' га', '', $cadnum_info->data[0]->area);

            Session::push('cadnum_info', $cadnum_info->data[0]);
            /* If advert isset */
            $advert = Advert::where('cadnum', $request->cadnum)->first();
            if ($advert) {
                if ($advert->customer_id == 0 && $advert->agent_id == 0 && Auth::guard('customer')->user()->role == 'Я агент по продаже') {
                    return response()->json(
                        '/customer/agent-confirm-cadnum'
                    );
                }
                if ($advert->customer_id == 0 && $advert->agent_id != 0) {
                    if (Auth::guard('customer')->user()->role == 'Я агент по продаже') {
                        return response()->json(
                            '/customer/agent-confirm-cadnum-with-advertising'
                        );
                    } else {
                        return response()->json(
                            '/customer/owner-confirm-cadnum-with-advertising'
                        );
                    }
                }
                if ($advert->customer_id == 0) {
                    return response()->json(
                        '/customer/owner-confirm-cadnum'
                    );
                }
                /* If current user agent */
                if (Auth::guard('customer')->user()->role == 'Я агент по продаже') {
                    if ($advert->agent_id == 0) {
                        return response()->json(
                            '/customer/agent-message-cadnum'
                        );
                    } else {
                        return response()->json(
                            '/customer/agent-isset-cadnum'
                        );
                    }
                } else {
                    /* If current user owner */
                    return response()->json(
                        '/customer/owner-message-cadnum'
                    );
                }
            } else {
                /* If cadnum isset and in our DB null */
                return response()->json(
                    '/customer/create-advert'
                );
            }
        } else {
            return response()->json(
                '/customer/error-cadnum'
            );
        }
    }

    public function sendMessagetoOwner(sendMessagetoOwner $request)
    {
        $advert = Advert::where('cadnum', $request->cadnum)->first();
        Mail::to($advert->customer->email)->send(new NewMessage($request->message, $advert->id, $advert->x));
    }

    public function create(createAdvert $request)
    {
        $cadnum = Session::get('cadnum_info')[0];

        $check = Advert::where('cadnum', $cadnum->cadnum)->first();
        if(!$check){
            if ($request->type == 'Продаж') {
                $price_per_ga = str_replace(' ', '',$request->price) / $cadnum->area;
            } else {
                $price_per_ga = str_replace(' ', '',$request->price)  / $request->ga;
            }

            if (Auth::guard('customer')->user()->role == 'Я агент по продаже') {
                $agent_id = Auth::guard('customer')->user()->id;
                $customer_id = 0;
            } else {
                $customer_id = Auth::guard('customer')->user()->id;
                $agent_id = 0;
            }
            $phone = str_replace('+', '',$request->phone);
            $advert = Advert::create([
                'customer_id' => $customer_id,
                'agent_id' => $agent_id,
                'type' => $request->type,
                'ga_to_sell' => $request->ga,
                'price' => intval(str_replace(' ', '',$request->price)),
                'price_per_ga' => intval($price_per_ga),
                'currency' => $request->currency,
                'discount' => $request->discount,
                'cadnum' => $cadnum->cadnum,
                'region' => $request->region,
                'slug_region' => str_slug($request->region, '-'),
                'district' => $request->district,
                'slug_district' => str_slug($request->district, '-'),
                'city' => $request->city,
                'slug_city' => str_slug($request->city, '-'),
                'coatuu' => $cadnum->koatuu,
                'zone' => $cadnum->zone,
                'quartal' => $cadnum->quartal,
                'ga' => $cadnum->area,
                'use_с' => $cadnum->right,
                'purpose' => $cadnum->purpose,
                'x' => $cadnum->x,
                'y' => $cadnum->y,
                'attr_1' => $request->attr_1,
                'attr_2' => $request->attr_2,
                'attr_3' => $request->attr_3,
                'attr_4' => $request->attr_4,
                'attr_5' => $request->attr_5,
                'comment_1' => $request->comment_1,
                'comment_2' => $request->comment_2,
                'comment_3' => $request->comment_3,
                'sort_date' => date('Y-m-d H:i:s'),
            ]);
            Session::forget('cadnum_info');
            Session::forget('cadnum_coordinate');
            Session::forget('cadnum');

            if ($request->image) {
                foreach ($request->image as $image) {
                    Image::make($image)->resize(1200, 900, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save(public_path('storage/AdvertPhoto/' . $image->hashName()));
                    AdvertImage::create([
                        'advert_id' => $advert->id,
                        'image' => $image->hashName()
                    ]);
                }
            }
            return response()->json(
                '/customer/successful-publish'
            );
        }else{
            $error['message'][0] = 'Щось пішло не так спробуйте пізніше або поновіть сторінку.';
            return response()->json([
                'errors' => $error
            ], 422);
        }

    }

    public function edit(createAdvert $request)
    {
        if ($request->id) {
            $advert = Advert::find($request->id);
            if ($advert) {

                if ($request->type == 'Продаж') {
                    $price_per_ga = str_replace(' ', '',$request->price)/ $advert->ga;
                } else {
                    $price_per_ga = str_replace(' ', '',$request->price)/ $advert->ga_to_sell;
                }
                $advert = Advert::where('id', $request->id)->update([
                    'type' => $request->type,
                    'ga_to_sell' => $request->ga,
                    'price' => intval(str_replace(' ', '',$request->price)),
                    'price_per_ga' => $price_per_ga,
                    'currency' => $request->currency,
                    'discount' => $request->discount,
                    'attr_1' => $request->attr_1,
                    'attr_2' => $request->attr_2,
                    'attr_3' => $request->attr_3,
                    'attr_4' => $request->attr_4,
                    'attr_5' => $request->attr_5,
                    'comment_1' => $request->comment_1,
                    'comment_2' => $request->comment_2,
                    'comment_3' => $request->comment_3,
                ]);
                if ($request->image) {
                    foreach ($request->image as $image) {
                        Image::make($image)->resize(1200, 900, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save(public_path('storage/AdvertPhoto/' . $image->hashName()));
                        AdvertImage::create([
                            'advert_id' => $request->id,
                            'image' => $image->hashName()
                        ]);
                    }
                }
            }
        }

        return response()->json(
            'true'
        );

    }

    public function deletePhoto(Request $request)
    {
        if (isset($request->id)) {
            $image = AdvertImage::where('id', $request->id)->first();
            if ($image->advert->customer_id == Auth::guard('customer')->user()->id || $image->advert->agent_id == Auth::guard('customer')->user()->id) {
                Storage::delete('AdvertPhoto/' . $image->image);
                $image->delete();
                return response()->json(
                    '/true'
                );
            }

        }
    }

    public function getCadnumInfo($cadnum)
    {

        $url = 'https://gisfile.com/layer/cadmap/search?cadnum='.$cadnum.'&token=1d2da10604604ea58f92c2d1f7ed9b82';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.67 Safari/537.36');
        $data = curl_exec($ch);
        $data = json_decode($data);
        curl_close($ch);

        return $data;
    }

    public function getCoordinateInfo(Request $request)
    {
        $data = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $request->y . ',' . $request->x . '&key=AIzaSyBT9V7PtXQCVT5wFlkamJueRXOLgYSnNQk&language=uk';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Устанавливаем параметр, чтобы curl возвращал данные, вместо того, чтобы выводить их в браузер.
        curl_setopt($ch, CURLOPT_URL, $data);
        $data = curl_exec($ch);
        $data = json_decode($data);
        curl_close($ch);
        $cadnum = Session::get('cadnum_info')[0];
        Session::forget('cadnum_info');
        /* Find district and region in random array */
        $place = array();
        foreach ($data->results[0]->address_components as $result) {

            if (isset($result->long_name)) {
                $district = strpos($result->long_name, "район");
                $region = strpos($result->long_name, "область");
                $city = strpos($result->long_name, "місто");
                if ($district === false) {
                } else {
                    $cadnum->district = $result->long_name;
                }
                if ($region === false) {
                } else {
                    $cadnum->region = $result->long_name;
                }
                if ($city === false) {
                } else {
                    $cadnum->city = str_replace('місто ', '', $result->long_name);
                }
            }
        }

        if (!isset($cadnum->region) || $cadnum->region == '') {
            $cadnum->region = 'Не визначено';
        }
        if (!isset($cadnum->district) || $cadnum->district == '') {
            $cadnum->district = 'Не визначено';
        }
        if (!isset($cadnum->city) || $cadnum->city == '') {
            $cadnum->city = 'Не визначено';
        }
        $place['district'] = $cadnum->district;
        $place['region'] = $cadnum->region;
        $place['city'] = $cadnum->city;

        $cadnum->x = $request->x;
        $cadnum->y = $request->y;
        Session::push('cadnum_info', $cadnum);
        return response()->json([
            'place' => $place
        ]);

    }

    public function deactivate(Request $request)
    {
        $advert = Advert::find($request->id);
        if ($advert->customer_id == Auth::guard('customer')->user()->id || $advert->agent_id == Auth::guard('customer')->user()->id) {
            Advert::where('id', $request->id)->update([
                'search_status' => 'Відключено'
            ]);
            return 'true';
        }
    }

    public function deactivateAndSelling(Request $request)
    {
        $advert = Advert::find($request->id);
        if ($advert->customer_id == Auth::guard('customer')->user()->id || $advert->agent_id == Auth::guard('customer')->user()->id) {
            Advert::where('id', $request->id)->update([
                'search_status' => 'Відключено'
            ]);
            if($advert->type = 'Продаж'){
                Advert::where('id', $request->id)->update([
                    'customer_id' => 0
                ]);
            }

            AdvertHistory::create([
                'advert_id' => $advert->id,
                'customer_id' => $advert->customer_id,
                'agent_id' => $advert->agent_id,
                'price' => intval($request->price),
                'type' => $advert->type
            ]);
            return 'true';
        }
    }

    public function activate(Request $request)
    {
        $advert = Advert::find($request->id);
        if ($advert->customer_id == Auth::guard('customer')->user()->id || $advert->agent_id == Auth::guard('customer')->user()->id) {
            Advert::where('id', $request->id)->update([
                'search_status' => 'Активно'
            ]);
            return 'true';
        }
    }

    public function delete($id)
    {
        $advert = Advert::find($id);
        if ($advert->customer_id == Auth::guard('customer')->user()->id || $advert->agent_id == Auth::guard('customer')->user()->id) {
            $photos = AdvertImage::where('advert_id', $id)->get();
            $favorites = Favorite::where('advert_id', $id)->get();
            foreach ($favorites as $favorite) {
                $favorite->delete();
            }
            foreach ($photos as $photo) {
                Storage::delete('AdvertPhoto/' . $photo->image);
                $photo->delete();
            }
            $advert->delete();
            return redirect()->back();
        } else {
            $title = '404';
            return view('frontend.pages.404', compact(['title']));
        }
    }

    public function get($id)
    {
        $advert = Advert::find($id);
        if ($advert) {
            Advert::where('id', $id)->update([
                'views' => $advert->views + 1
            ]);
            $advert = Advert::find($id);

            $percent_ga = $advert->ga_to_sell * 0.2;
            $percent_price = $advert->price * 0.2;
            $similar_ga_max = $advert->ga_to_sell + $percent_ga;
            $similar_ga_min = $advert->ga_to_sell - $percent_ga;
            $similar_price_max = $advert->price + $percent_price;
            $similar_price_min = $advert->price - $percent_price;

            $similar_adverts = Advert::whereBetween('price', [$similar_price_min, $similar_price_max])->whereBetween('ga_to_sell', [$similar_ga_min, $similar_ga_max])->where('region', $advert->region)
                ->where('search_status', 'Активно')->whereNotIn('id', [$id])->get();

            $title = $advert->region . ' ' . $advert->district;
            if (Auth::guard('customer')->check()) {
                $favorite = Favorite::where('advert_id', $id)->where('customer_id', Auth::guard('customer')->user()->id)->first();
            }
            if (isset($favorite)) {
                $advert->favorite = 1;
            }
            if ($advert->search_status == 'Активно' || Auth::guard('admin')->check()) {
                return view('frontend.pages.advert', compact(['title', 'advert', 'similar_adverts']));
            } else {
                if (Auth::guard('customer')->check() && $advert->customer_id == Auth::guard('customer')->user()->id
                    || Auth::guard('customer')->check() && $advert->agent_id == Auth::guard('customer')->user()->id) {
                    return view('frontend.pages.advert', compact(['title', 'advert', 'similar_adverts']));
                }

                $title = '404';
                return view('frontend.pages.404', compact(['title']));
            }
        }
    }

    public function getMapAdvert(Request $request)
    {
        $advert = Advert::find($request->id);
        if (Auth::guard('customer')->check()) {
            $favorite = Favorite::where('advert_id', $request->id)->where('customer_id', Auth::guard('customer')->user()->id)->first();
        }
        if (isset($favorite)) {
            $advert->favorite = 1;
        }
        return $advert;
    }

    public function unfastenAgent($id)
    {
        $advert = Advert::find($id);
        if ($advert) {
            Mail::to($advert->agent_customer->email)->send(new DetachAgent($advert));
            Advert::where('id', $id)->update([
                'agent_id' => 0
            ]);
            return redirect()->back();
        } else {
            $title = '404';
            return view('frontend.pages.404', compact(['title']));
        }
    }

    public function unfastenOwner($id)
    {
        $advert = Advert::find($id);
        if ($advert) {
            Advert::where('id', $id)->update([
                'customer_id' => 0,
                'search_status' => 'Відключено'
            ]);
            return redirect()->back();
        } else {
            $title = '404';
            return view('frontend.pages.404', compact(['title']));
        }
    }

    public function setAgent(Request $request)
    {
        $advert = Advert::where('cadnum', $request->cadnum)->first();
        if ($advert && $advert->agent_id == 0) {
            Advert::where('cadnum', $request->cadnum)->update([
                'agent_id' => Auth::guard('customer')->user()->id
            ]);
            return response()->json(
                'true'
            );
        }
    }

    public function setOwner(Request $request)
    {
        $advert = Advert::where('cadnum', $request->cadnum)->first();
        if ($advert && $advert->customer_id == 0) {
            Advert::where('cadnum', $request->cadnum)->update([
                'customer_id' => Auth::guard('customer')->user()->id
            ]);
            return response()->json(
                'true'
            );
        }
    }

    public function search(Request $request)
    {
        $query_top = Advert::where('search_status', 'Активно')->where('top', '!=', NULL)->where('top', '!=', '0000-00-00 00:00:00')->orderby('sort_date', 'DESC');

        if ($request->cadnumSearch) {
            $query_top->where('cadnum', 'LIKE', '%' . $request->cadnumSearch . '%');
        }

        if ($request->s_region) {
            $query_top->where('region', $request->s_region);
        }

        if ($request->s_district) {
            $query_top->where('district', $request->s_district);
        }

        if ($request->s_city) {
            $query_top->where('city', $request->s_city);
        }

        if ($request->type) {
            $query_top->where('type', $request->type);
        } else {
            $query_top->where('type', 'Оренда');
        }

        if ($request->maxGa) {
            $check = '.';
            $result = strpos($request->maxGa, $check);
            if ($result === false) {
                $request->maxGa = intval($request->maxGa);
            } else {
                $request->maxGa = floatval($request->maxGa);
            }
            unset($check);
            unset($result);
        }

        if ($request->minGa) {
            $check = '.';
            $result = strpos($request->minGa, $check);
            if ($result === false) {
                $request->minGa = intval($request->minGa);
            } else {
                $request->minGa = floatval($request->minGa);
            }
        }

        if ($request->type == 'Оренда') {
            if ($request->minGa && $request->maxGa) {
                $query_top->whereBetween('ga_to_sell', [$request->minGa - 0.01, $request->maxGa]);

            } elseif ($request->minGa) {
                $query_top->where('ga_to_sell', '>=', $request->minGa);
            } elseif ($request->maxGa) {
                $query_top->where('ga_to_sell', '<=', $request->maxGa);
            }
        } else {
            if ($request->minGa && $request->maxGa) {
                $query_top->whereBetween('ga', [$request->minGa - 0.01, $request->maxGa]);
            } elseif ($request->minGa) {
                $query_top->where('ga', '>=', $request->minGa);
            } elseif ($request->maxGa) {
                $query_top->where('ga', '<=', $request->maxGa);
            }
        }

        if ($request->minPrice && $request->maxPrice) {
            $query_top->whereBetween('price', [intval(str_replace(' ', '',$request->minPrice)), intval(str_replace(' ', '',$request->maxPrice))]);
        } elseif ($request->minPrice) {
            $query_top->where('price', '>=', intval(str_replace(' ', '', $request->minPrice)));
        } elseif ($request->maxPrice) {
            $query_top->where('price', '<=', intval(str_replace(' ', '', $request->maxPrice)));
        }

        $adverts_top = $query_top->get();
        $i = 0;
        foreach ($adverts_top as $advert) {
            if ($advert->customer_id == 0 && $advert->agent_id == 0) {
                $adverts_top->forget($i);
            }
            $advert->created_date = $advert->created_at->format('d.m.Y');
            $i++;
            if (Auth::guard('customer')->check()) {
                $favorite = Favorite::where('advert_id', $advert->id)->where('customer_id', Auth::guard('customer')->user()->id)->first();
                if ($favorite) {
                    $advert->favorite = 1;
                }
                unset($favorite);
            }
        }

        $adverts_top = $adverts_top->values();

        $query = Advert::where('search_status', 'Активно')->orderby('sort_date', 'DESC')->orderby('top', 'DESC');

        if ($request->cadnumSearch) {
            $query->where('cadnum', 'LIKE', '%' . $request->cadnumSearch . '%');
        }

        if ($request->s_region) {
            $query->where('region', $request->s_region);
        }

        if ($request->s_district) {
            $query->where('district', $request->s_district);
        }

        if ($request->s_city) {
            $query->where('city', $request->s_city);
        }

        if ($request->type) {
            $query->where('type', $request->type);
        } else {
            $query->where('type', 'Аренда');
        }

        if ($request->maxGa) {
            $check = '.';
            $result = strpos($request->maxGa, $check);
            if ($result === false) {
                $request->maxGa = intval($request->maxGa);
            } else {
                $request->maxGa = floatval($request->maxGa);
            }
            unset($check);
            unset($result);
        }

        if ($request->minGa) {
            $check = '.';
            $result = strpos($request->minGa, $check);
            if ($result === false) {
                $request->minGa = intval($request->minGa);
            } else {
                $request->minGa = floatval($request->minGa);
            }
        }

        if ($request->type == 'Оренда') {
            if ($request->minGa && $request->maxGa) {
                $query->whereBetween('ga_to_sell', [$request->minGa - 0.01, $request->maxGa]);

            } elseif ($request->minGa) {
                $query->where('ga_to_sell', '>=', $request->minGa);
            } elseif ($request->maxGa) {
                $query->where('ga_to_sell', '<=', $request->maxGa);
            }
        } else {
            if ($request->minGa && $request->maxGa) {
                $query->whereBetween('ga', [$request->minGa - 0.01, $request->maxGa]);
            } elseif ($request->minGa) {
                $query->where('ga', '>=', $request->minGa);
            } elseif ($request->maxGa) {
                $query->where('ga', '<=', $request->maxGa);
            }
        }

        if ($request->minPrice && $request->maxPrice) {
            $query->whereBetween('price', [intval(str_replace(' ', '',$request->minPrice)), intval(str_replace(' ', '',$request->maxPrice))]);
        } elseif ($request->minPrice) {
            $query->where('price', '>=', intval(str_replace(' ', '', $request->minPrice)));
        } elseif ($request->maxPrice) {
            $query->where('price', '<=', intval(str_replace(' ', '', $request->maxPrice)));
        }

        $adverts = $query->get();
        $i = 0;
        foreach ($adverts as $advert) {
            if ($advert->customer_id == 0 && $advert->agent_id == 0) {
                $adverts->forget($i);
            }
            $advert->created_date = $advert->created_at->format('d.m.Y');
            $i++;
            if (Auth::guard('customer')->check()) {
                $favorite = Favorite::where('advert_id', $advert->id)->where('customer_id', Auth::guard('customer')->user()->id)->first();
                if ($favorite) {
                    $advert->favorite = 1;
                }
                unset($favorite);
            }
        }
        $adverts = $adverts->values();
        $adverts_top = $adverts_top->merge($adverts);

        /*
         * New functional for find cadnum in map land gov
         */
        $check_cadnum = Advert::where('search_status', 'Активно')->where('cadnum', 'LIKE', '%' . $request->cadnumSearch . '%')->first();

        if(!$check_cadnum){
            /*
             * Get main cadnum data
             */
            $cadnum_info = $this->getCadnumInfo($request->cadnumSearch);

            /* Check for cadnum data isset */
            if(isset($cadnum_info->data[0])){
                Session::forget('cadnum_info');
                Session::forget('cadnum_coordinate');
                Session::forget('cadnum');

                Session::push('cadnum', $request->cadnumSearch);
                Session::push('cadnum_info', $cadnum_info->data[0]);
                // УСЛОВИЕ ЕСЛИ ЕСТЬ ОБЬЯМА СКРЫТАЯ ТО ЕСТЬ есть хозяин но она не активна, то над опоказать эту обьяву на карет
                /*
                 * Create url for create advert for current cadnum
                 */
                $check_cadnum = '/customer/create-advert';
                $close_advert = Advert::where('cadnum', 'LIKE', '%' . $request->cadnumSearch . '%')->first();
                if($close_advert && Auth::guard('customer')->check()){
                    $check_cadnum = $this->checkCadnumInSearchPage($close_advert);
                }

                return response()->json(['cadnum' => $cadnum_info->data[0], 'check_cadnum' => $check_cadnum]);
            }else{
                return response()->json([ 'adverts' => []]);
            }
        }

        $percent_ga = $check_cadnum->ga_to_sell * 0.2;
        $percent_price = $check_cadnum->price * 0.2;
        $similar_ga_max = $check_cadnum->ga_to_sell + $percent_ga;
        $similar_ga_min = $check_cadnum->ga_to_sell - $percent_ga;
        $similar_price_max = $check_cadnum->price + $percent_price;
        $similar_price_min = $check_cadnum->price - $percent_price;

        $similar_adverts = Advert::whereBetween('price', [$similar_price_min, $similar_price_max])->whereBetween('ga_to_sell', [$similar_ga_min, $similar_ga_max])->where('region', $check_cadnum->region)
            ->where('search_status', 'Активно')->whereNotIn('id', [$check_cadnum->id])->get();

        foreach ($similar_adverts as $advert) {
            $advert->created_date = $advert->created_at->format('d.m.Y');
        }
        /* finish */

        return response()->json(['adverts' => $adverts_top, 'similar_adverts' => $similar_adverts]);

    }

    public  function checkCadnumInSearchPage($advert){
        if ($advert->customer_id == 0 && $advert->agent_id == 0 && Auth::guard('customer')->user()->role == 'Я агент по продаже') {
            return '/customer/agent-confirm-cadnum';
        }
        if ($advert->customer_id == 0 && $advert->agent_id != 0) {
            if (Auth::guard('customer')->user()->role == 'Я агент по продаже') {
                return '/customer/agent-confirm-cadnum-with-advertising';
            } else {
                return '/customer/owner-confirm-cadnum-with-advertising';
            }
        }
        if ($advert->customer_id == 0) {
            return '/customer/owner-confirm-cadnum';
        }
        /* If current user agent */
        if (Auth::guard('customer')->user()->role == 'Я агент по продаже') {
            if ($advert->agent_id == 0) {
                return '/customer/agent-message-cadnum';
            } else {
                return '/customer/agent-isset-cadnum';
            }
        } else {
            /* If current user owner */
            return '/customer/owner-message-cadnum';
        }
    }

    public function createFavorite(Request $request)
    {
        $advert = Advert::where('id', $request->id)->first();
        if ($advert) {
            Favorite::create([
                'advert_id' => $request->id,
                'customer_id' => Auth::guard('customer')->user()->id
            ]);
            return response()->json(
                'true'
            );
        }
    }

    public function deleteFavorite(Request $request)
    {
        $advert = Advert::find($request->id);
        if ($advert) {
            $favorite = Favorite::where('advert_id', $request->id)->where('customer_id', Auth::guard('customer')->user()->id)->first();
            $favorite->delete();
            return response()->json(
                'true'
            );
        }
    }

    public function payType($id,$type)
    {
        $advert = Advert::find($id);

        if ($advert && $advert->customer_id == Auth::guard('customer')->user()->id || $advert->agent_id == Auth::guard('customer')->user()->id) {
            $private_key = 'YoXSTpeq4yhgzeb0dWUvmrfXfjXhFXLZtS90VVe9';
            $liqpay = new LiqPay('i21256651840', $private_key);
            $token = rand(0, 9) . '' . rand(0, 9) . '' . rand(0, 9) . '' . rand(0, 9) . '' . rand(0, 9) . '' . rand(0, 9);

            if($type == 1){
                $price = 60;
                $once_top_day = date('Y-m-d H:i:s',strtotime("+1 days"));
                $top = date('Y-m-d H:i:s',strtotime("+7 days"));
                $red = date('Y-m-d H:i:s',strtotime("+30 days"));
                $type = 'СТАНДАРТНИЙ';
            }elseif ($type == 2){
                $price = 120;
                $once_top_day = date('Y-m-d H:i:s',strtotime("+7 days"));
                $top = date('Y-m-d H:i:s',strtotime("+14 days"));
                $red = date('Y-m-d H:i:s',strtotime("+30 days"));
                $type = 'СУПЕР ВИГІДНИЙ';
            }elseif ($type == 3){
                $price = 250;
                $once_top_day = date('Y-m-d H:i:s',strtotime("+30 days"));
                $top = date('Y-m-d H:i:s',strtotime("+30 days"));
                $red = date('Y-m-d H:i:s',strtotime("+30 days"));
                $type = 'МАКСИМАЛЬНИЙ';
            }else{
                $title = '404';
                return view('frontend.pages.404',compact(['title']));
            }

            $payment = Payment::create([
                'customer_id' => Auth::guard('customer')->user()->id,
                'advert_id' => $id,
                'once_top_day' => $once_top_day,
                'top' => $top,
                'red' => $red,
                'type' => $type,
                'price' => $price
            ]);

            $data = array(
                'public_key' => 'i21256651840',
                'action' => 'pay',
                'amount' => $price,
                'currency' => 'UAH',
                'description' => $type,
                'order_id' => $id.' '.$payment->id.' '.$token,
                'version' => '3',
//                'sandbox' => 1,
                'result_url' => 'https://uland.ua/thank',
                'server_url'        => 'https://uland.ua/checkPacketPayment',
            );

            $data = base64_encode(json_encode($data));
            $sign_string = $private_key . $data . $private_key;
            $signature = $liqpay->str_to_sign($private_key . $data . $private_key);

            $url = 'https://www.liqpay.ua/api/3/checkout?data=' . $data . '&signature=' . $signature;
            return redirect($url);
        }else{
            $title = '404';
            return view('frontend.pages.404',compact(['title']));
        }
    }

    public function checkPacketPayment(Request $request)
    {
        $data = base64_decode($request->data);
        $data = json_decode($data);
        $advert_data = explode(" ", $data->order_id);
        $advert = Advert::where('id', $advert_data[0])->first();

        if($data->description == 'СТАНДАРТНИЙ'){
            if($advert->once_top_day != NULL && $advert->once_top_day != '0000-00-00 00:00:00'){
                $once_top_day = date('Y-m-d H:i:s',strtotime($advert->once_top_day. "+1 days"));
            }else{
                $once_top_day = date('Y-m-d H:i:s',strtotime("+1 days"));
            }
            if($advert->top != NULL && $advert->top != '0000-00-00 00:00:00'){
                $top = date('Y-m-d H:i:s',strtotime($advert->top. "+7 days"));
            }else{
                $top = date('Y-m-d H:i:s',strtotime("+7 days"));
            }
            if($advert->red != NULL && $advert->red != '0000-00-00 00:00:00'){
                $red = date('Y-m-d H:i:s',strtotime($advert->red. "+30 days"));
            }else{
                $red = date('Y-m-d H:i:s',strtotime("+30 days"));
            }
        }elseif ($data->description == 'СУПЕР ВИГІДНИЙ'){
            if($advert->once_top_day != NULL && $advert->once_top_day != '0000-00-00 00:00:00'){
                $once_top_day = date('Y-m-d H:i:s',strtotime($advert->once_top_day. "+7 days"));
            }else{
                $once_top_day = date('Y-m-d H:i:s',strtotime("+7 days"));
            }
            if($advert->top != NULL && $advert->top != '0000-00-00 00:00:00'){
                $top = date('Y-m-d H:i:s',strtotime($advert->top. "+14 days"));
            }else{
                $top = date('Y-m-d H:i:s',strtotime("+14 days"));
            }
            if($advert->red != NULL && $advert->red != '0000-00-00 00:00:00'){
                $red = date('Y-m-d H:i:s',strtotime($advert->red. "+30 days"));
            }else{
                $red = date('Y-m-d H:i:s',strtotime("+30 days"));
            }
        }elseif ($data->description == 'МАКСИМАЛЬНИЙ'){
            if($advert->once_top_day != NULL && $advert->once_top_day != '0000-00-00 00:00:00'){
                $once_top_day = date('Y-m-d H:i:s',strtotime($advert->once_top_day. "+30 days"));
            }else{
                $once_top_day = date('Y-m-d H:i:s',strtotime("+30 days"));
            }
            if($advert->top != NULL && $advert->top != '0000-00-00 00:00:00'){
                $top = date('Y-m-d H:i:s',strtotime($advert->top. "+30 days"));
            }else{
                $top = date('Y-m-d H:i:s',strtotime("+30 days"));
            }
            if($advert->red != NULL && $advert->red != '0000-00-00 00:00:00'){
                $red = date('Y-m-d H:i:s',strtotime($advert->red. "+30 days"));
            }else{
                $red = date('Y-m-d H:i:s',strtotime("+30 days"));
            }
        }

        Payment::where('id', $advert_data[1])->update([
            'status' => 'Оплачено'
        ]);
        Advert::where('id', $advert_data[0])->update([
            'once_top_day' => $once_top_day,
            'top' => $top,
            'red' => $red,
            'sort_date' => date('Y-m-d H:i:s'),
        ]);
    }

    public function payCustomType(Request $request){
        $price = 0;
        $advert = Advert::where('id', $request->advert_id)->first();
        $once_top_day = $advert->once_top_day;
        $top = $advert->top;
        $red = $advert->red;
        $type = 'Власна збірка';

        if($request->allotment == 'true'){
            if($advert->red != NULL && $advert->red != '0000-00-00 00:00:00'){
                $red = date('Y-m-d H:i:s',strtotime($advert->red. "+30 days"));
            }else{
                $red = date('Y-m-d H:i:s',strtotime("+30 days"));
            }
            $price = 30;
        }
        if($request->upAdvert['isActive'] == 'true'){
            if($request->upAdvert['period'] == 1){
                $price = 10+$price;
                if($advert->once_top_day != NULL && $advert->once_top_day != '0000-00-00 00:00:00'){
                    $once_top_day = date('Y-m-d H:i:s',strtotime($advert->once_top_day. "+1 days"));
                }else{
                    $once_top_day = date('Y-m-d H:i:s',strtotime("+1 days"));
                }
            }
            if($request->upAdvert['period'] == 7){
                $price = 56+$price;
                if($advert->once_top_day != NULL && $advert->once_top_day != '0000-00-00 00:00:00'){
                    $once_top_day = date('Y-m-d H:i:s',strtotime($advert->once_top_day. "+7 days"));
                }else{
                    $once_top_day = date('Y-m-d H:i:s',strtotime("+7 days"));
                }
            }
            if($request->upAdvert['period'] == 30){
                $price = 180+$price;
                if($advert->once_top_day != NULL && $advert->once_top_day != '0000-00-00 00:00:00'){
                    $once_top_day = date('Y-m-d H:i:s',strtotime($advert->once_top_day. "+30 days"));
                }else{
                    $once_top_day = date('Y-m-d H:i:s',strtotime("+30 days"));
                }
            }
        }
        if($request->top['isActive'] == 'true'){
            if($request->top['period'] == 7){
                $price = 70+$price;
                if($advert->top != NULL && $advert->top != '0000-00-00 00:00:00'){
                    $top = date('Y-m-d H:i:s',strtotime($advert->top. "+7 days"));
                }else{
                    $top = date('Y-m-d H:i:s',strtotime("+7 days"));
                }
            }
            if($request->top['period'] == 30){
                $price = 240+$price;
                if($advert->top != NULL && $advert->top != '0000-00-00 00:00:00'){
                    $top = date('Y-m-d H:i:s',strtotime($advert->top. "+30 days"));
                }else{
                    $top = date('Y-m-d H:i:s',strtotime("+30 days"));
                }
            }
        }

        $payment = Payment::create([
            'customer_id' => Auth::guard('customer')->user()->id,
            'advert_id' => $request->advert_id,
            'once_top_day' => $once_top_day,
            'top' => $top,
            'red' => $red,
            'type' => $type,
            'price' => $price
        ]);

        $private_key = 'YoXSTpeq4yhgzeb0dWUvmrfXfjXhFXLZtS90VVe9';
        $liqpay = new LiqPay('i21256651840', $private_key);
        $token = rand(0, 9) . '' . rand(0, 9) . '' . rand(0, 9) . '' . rand(0, 9) . '' . rand(0, 9) . '' . rand(0, 9);
        $data = array(
            'public_key' => 'i21256651840',
            'action' => 'pay',
            'amount' => $price,
            'currency' => 'UAH',
            'description' => $payment->id,
            'order_id' => $token,
            'version' => '3',
//            'sandbox' => 1,
            'result_url' => 'https://uland.ua/thank',
            'server_url'        => 'https://uland.ua/checkCustomPacketPayment',
        );

        $data = base64_encode(json_encode($data));
        $sign_string = $private_key . $data . $private_key;
        $signature = $liqpay->str_to_sign($private_key . $data . $private_key);

        $url = 'https://www.liqpay.ua/api/3/checkout?data=' . $data . '&signature=' . $signature;

        return response()->json(
            $url
        );
    }

    public function checkCustomPacketPayment(Request $request)
    {
        $data = base64_decode($request->data);
        $data = json_decode($data);
        $payment = Payment::where('id', $data->description)->first();
        $advert = Advert::where('id', $payment->advert_id)->first();

        Payment::where('id', $data->description)->update([
            'status' => 'Оплачено'
        ]);
        if(date('Y-m-d', strtotime($advert->once_top_day)) != date('Y-m-d', strtotime($payment->once_top_day))){
            Advert::where('id', $advert->id)->update([
                'once_top_day' => $payment->once_top_day,
                'top' => $payment->top,
                'red' => $payment->red,
                'sort_date' => date('Y-m-d H:i:s'),
            ]);
        }else{
            Advert::where('id', $advert->id)->update([
                'once_top_day' => $payment->once_top_day,
                'top' => $payment->top,
                'red' => $payment->red,
            ]);
        }

    }

    public function getSimilarAdvert(Request $request){

        $percent_ga = $request->ga * 0.2;
        $similar_ga_max = $request->ga + $percent_ga;
        $similar_ga_min = $request->ga - $percent_ga;

        $similar_adverts = Advert::where('search_status', 'Активно');

        if($request->ga){
            $similar_adverts->whereBetween('ga_to_sell', [$similar_ga_min, $similar_ga_max]);
        }

        if($request->district){
            $similar_adverts->where('district', $request->district);
        }

        if($request->region){
            $similar_adverts->where('region', $request->region);
        }
        $similar_adverts = $similar_adverts->get();

        foreach ($similar_adverts as $advert) {
            $advert->created_date = $advert->created_at->format('d.m.Y');
        }

        return response()->json(['similar_adverts' => $similar_adverts]);

    }

}
