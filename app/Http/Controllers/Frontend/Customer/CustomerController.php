<?php

namespace App\Http\Controllers\Frontend\Customer;

use App\Http\Requests\Frontend\EditCustomer;
use App\Http\Requests\Frontend\RegisterFinish;
use App\Http\Requests\Frontend\RegisterStepOne;
use App\Http\Requests\Frontend\RegisterStepTwo;
use App\Http\Requests\Frontend\ResetFinish;
use App\Http\Requests\Frontend\ResetStepOne;
use App\Mail\SendRegisterMessage;
use App\Models\Advert;
use App\Models\Favorite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Customer;
use Illuminate\Support\Facades\Hash;
use App\Events\CustomerRegistered;
use App\Http\Controllers\Frontend\EpMessageController;
use \Session;
use \File;
use Illuminate\Support\Facades\Storage;
use Mail;
use Carbon\Carbon;


class CustomerController extends Controller
{
    public function register_step_1(RegisterStepOne $request)
    {
        $message = new EpMessageController();
        Session::forget('phone');
        Session::forget('message_token');
        Session::forget('reg_status');
        $phone = $request->phone;
        $message_token = rand(0,9).''.rand(0,9).''.rand(0,9).''.rand(0,9).''.rand(0,9).''.rand(0,9);
        $message->sendRegisterCode($phone, $message_token);
        Session::push('phone', $phone);
        Session::push('message_token', $message_token);
        return response()->json(
            'register-step-2'
        );
    }

    public function register_step_2(RegisterStepTwo $request)
    {
        $message_token = Session::get('message_token')[0];
        if($request->code == $message_token){
            Session::push('reg_status', 'true');
            return response()->json(
                'register-step-3'
            );
        }else{
            $error = array();
            $error['message_token'][0] = 'Код неправильний.';
            return response()->json([
                'errors' => $error
            ], 422);
        }
    }

    public function create(RegisterFinish $request)
    {
        $token = str_random();
        Mail::to($request->email)->send(new SendRegisterMessage($token));

        $customer = Customer::create([
            'role' => $request->role,
            'name' => $request->name,
            'email' => $request->email,
            'surname' => $request->surname,
            'city' => $request->city,
            'birthday' => $request->birthday,
            'token' => $token,
            'phone' => Session::get('phone')[0],
            'password' => Hash::make($request->password),
        ]);
        if($request->avatar){
            $image = $request->avatar;
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $avatar = str_random(10).'.'.'png';
            \File::put(public_path(). '/storage/Avatar/' . $avatar, base64_decode($image));
            Customer::where('id', $customer->id)->update([
                'avatar' => $avatar
            ]);
        }else{
            $avatar = '';
        }
        /* delete reg info about customer */

        /* create real time notification about new user */
//        $count = Customer::count();
        broadcast(new CustomerRegistered($avatar))->toOthers();
        if(Auth::guard('customer')->attempt(['phone' => Session::get('phone')[0], 'password' => $request->password]))
        {
            Session::forget('phone');
            Session::forget('message_token');
            Session::forget('reg_status');
            return response()->json(
                '/successful-register'
            );
        }
    }

    public function edit(EditCustomer $request)
    {
        $customer = Customer::where('id', Auth::guard('customer')->user()->id)->update([
            'name' => $request->name,
            'email' => $request->email,
            'surname' => $request->surname,
            'city' => $request->city,
            'birthday' => $request->birthday,
        ]);
        if($request->avatar){
            $image = $request->avatar;
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $avatar = str_random(10).'.'.'png';
            \File::put(public_path(). '/storage/Avatar/' . $avatar, base64_decode($image));
            if(Auth::guard('customer')->user()->avatar){
                Storage::delete('Avatar/' . Auth::guard('customer')->user()->avatar);
            }
            Customer::where('id', Auth::guard('customer')->user()->id)->update([
                'avatar' => $avatar
            ]);
        }
        return response()->json(
            '/true'
        );

    }

    public function editPassword(ResetFinish $request)
    {
        if(Hash::check($request->old_password, Auth::guard('customer')->user()->password)) {
            Customer::where('id', Auth::guard('customer')->user()->id)->update([
                'password' => Hash::make($request->password)
            ]);
            return response()->json(
                '/customer/edit'
            );
        }else{
            $error = array();
            $error['message_token'][0] = 'Старий пароль введений невірно.';
            return response()->json([
                'errors' => $error
            ], 422);
        }
    }

    public function reSend()
    {
        Session::forget('message_token');
        $message = new EpMessageController();
        $message_token = rand(0,9).''.rand(0,9).''.rand(0,9).''.rand(0,9).''.rand(0,9).''.rand(0,9);
        $phone = Session::get('phone')[0];
        Session::push('message_token', $message_token);
        $message->sendRegisterCode($phone, $message_token);

        return response()->json(
            'true'
        );
    }

    public function reset_step_1(ResetStepOne $request)
    {
        $message = new EpMessageController();
        Session::forget('phone');
        Session::forget('message_token');
        Session::forget('reset_status');
        $phone = $request->phone;
        $message_token = rand(0,9).''.rand(0,9).''.rand(0,9).''.rand(0,9).''.rand(0,9).''.rand(0,9);
        $message->sendResetCode($phone, $message_token);
        Session::push('phone', $phone);
        Session::push('message_token', $message_token);
        return response()->json(
            'reset-step-2'
        );
    }

    public function reset_step_2(RegisterStepTwo $request)
    {
        $message_token = Session::get('message_token')[0];

        if($request->code ==  $message_token){
            Session::push('reset_status', 'true');
            return response()->json(
                'reset-step-3?token='.Hash::make($message_token)
            );
        }else{
            $error = array();
            $error['message_token'][0] = 'Код неправильный.';
            return response()->json([
                'errors' => $error
            ], 422);
        }
    }

    public function reset_step_3(ResetFinish $request)
    {
        $message_token = $request->message_token;
        if(Hash::check(Session('message_token')[0], $message_token)){
            Customer::where('phone', Session('phone')[0])->update([
                'password' => Hash::make($request->password)
            ]);
            return response()->json(
                '/successful-reset'
            );
        }else{
            $error = array();
            $error['message_token'][0] = 'Не намагайтеся зламати систему :).';
            return response()->json([
                'errors' => $error
            ], 422);
        }
    }

    public function reSendResetToken()
    {
        Session::forget('message_token');
        $message = new EpMessageController();
        $message_token = rand(0,9).''.rand(0,9).''.rand(0,9).''.rand(0,9).''.rand(0,9).''.rand(0,9);
        $phone = Session::get('phone')[0];
        Session::push('message_token', $message_token);
        $message->sendResetCode('+380'.$phone, $message_token);

        return response()->json(
            'true'
        );
    }

    public function activateEmail($token){
        Customer::where('token', $token)->update([
            'email_is_active' => 1
        ]);
        $title = 'Успішно активували пошту';
        return view('frontend.pages.successful-activate',compact(['title']));
    }

    public function reSendActivateMail(){
        if(Auth::guard('customer')->user()->email_is_active == 0) {
            Mail::to(Auth::guard('customer')->user()->email)->send(new SendRegisterMessage(Auth::guard('customer')->user()->token));
        }
        return redirect()->back();
    }

    public function get($id){
        $customer = Customer::find($id);
        if($customer){
            if(Auth::guard('customer')->check() && $customer->id == Auth::guard('customer')->user()->id){
                $title = 'Мій профіль';
                return view('frontend.customer.profile',compact(['title']));
            }else{
                $title = $customer->surname.' '.$customer->name;
                if($customer->role == 'Я собственник'){
                    $adverts = Advert::where('customer_id',$id)->where('agent_id', 0)->where('search_status', 'Активно')->paginate(env('APP_PAGINATION'));
                }else{
                    $adverts = Advert::where('agent_id',$id)->where('search_status', 'Активно')->paginate(env('APP_PAGINATION'));
                }
                if (Auth::guard('customer')->check()) {
                    foreach ($adverts as $advert) {
                        $favorite = Favorite::where('advert_id', $advert->id)->where('customer_id', Auth::guard('customer')->user()->id)->first();
                        if ($favorite) {
                            $advert->favorite = 1;
                        }
                        unset($favorite);
                    }
                }
                return view('frontend.pages.customer',compact(['title','customer', 'adverts']));
            }
        }else{
            $title = '404';
            return view('frontend.pages.404',compact(['title']));
        }
    }

    public function searchAgent(Request $request){
        if($request->city){
            $agents = Customer::where('role', 'Я агент по продаже')->where('city', $request->city)->get();
            $agents->load('agent_adverts_for_search');
        }else{
            $agents = Customer::where('role', 'Я агент по продаже')->get();
            $agents->load('agent_adverts_for_search');
        }
        foreach($agents as $agent){
            $agent->surname = substr($agent->surname, 0, 2).'.';
            $cDate = Carbon::parse($agent->created_at);
            $agent->date_count =  $cDate->diffInDays();
            $agent->date_count_word = $this->date_count(
                $cDate->diffInDays(),
                /* варианты написания для количества 1, 2 и 5 */
                array('добу', 'доби', 'діб')
            );
        }
        return $agents;
    }

    public function date_count($number, $after)
    {
        $cases = array(2, 0, 1, 1, 1, 2);
        return $after[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
    }

    public function getShortData(Request $request){
        $customer = Customer::where('id',$request->id)->first(['phone', 'email']);
        return $customer;
    }

}
