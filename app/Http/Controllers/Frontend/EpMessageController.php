<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class EpMessageController extends Controller
{
    public function sendRegisterCode($phone,$message_token)
    {
        $src = '<?xml version="1.0" encoding="UTF-8"?>
    <SMS>
        <operations>
        <operation>SEND</operation>
        </operations>
        <authentification>
        <username>Lighthunterua@gmail.com</username>
        <password>429232</password>
        </authentification>
        <message>
        <sender>Uland</sender>
        <text>Код для підтвердження реєстрації: '.$message_token.'</text>
        </message>
        <numbers>
        <number messageID="msg11">'.str_replace("+","",$phone).'</number>
        </numbers>
    </SMS>';

        $Curl = curl_init();
        $CurlOptions = array(
            CURLOPT_URL=>'http://api.atompark.com/members/sms/xml.php',
            CURLOPT_FOLLOWLOCATION=>false,
            CURLOPT_POST=>true,
            CURLOPT_HEADER=>false,
            CURLOPT_RETURNTRANSFER=>true,
            CURLOPT_CONNECTTIMEOUT=>15,
            CURLOPT_TIMEOUT=>100,
            CURLOPT_POSTFIELDS=>array('XML'=>$src),
        );
        curl_setopt_array($Curl, $CurlOptions);
        if(false === ($Result = curl_exec($Curl))) {
            throw new Exception('Http request failed');
        }

        curl_close($Curl);
    }

public function sendResetCode ($phone,$message_token)
    {
        $src = '<?xml version="1.0" encoding="UTF-8"?>
    <SMS>
        <operations>
        <operation>SEND</operation>
        </operations>
        <authentification>
        <username>Lighthunterua@gmail.com</username>
        <password>429232</password>
        </authentification>
        <message>
        <sender>Uland</sender>
        <text>Код для відновлення пароля: '.$message_token.'</text>
        </message>
        <numbers>
        <number messageID="msg11">'.str_replace("+","",$phone).'</number>
        </numbers>
    </SMS>';

        $Curl = curl_init();
        $CurlOptions = array(
            CURLOPT_URL=>'http://api.atompark.com/members/sms/xml.php',
            CURLOPT_FOLLOWLOCATION=>false,
            CURLOPT_POST=>true,
            CURLOPT_HEADER=>false,
            CURLOPT_RETURNTRANSFER=>true,
            CURLOPT_CONNECTTIMEOUT=>15,
            CURLOPT_TIMEOUT=>100,
            CURLOPT_POSTFIELDS=>array('XML'=>$src),
        );
        curl_setopt_array($Curl, $CurlOptions);
        if(false === ($Result = curl_exec($Curl))) {
            throw new Exception('Http request failed');
        }

        curl_close($Curl);
    }


}
