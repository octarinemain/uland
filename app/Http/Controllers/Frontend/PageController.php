<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Models\Support;
use App\Models\Customer;
use App\Models\Advert;
use App\Models\Article;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use \Session;
use Auth;
//use Spatie\Sitemap\Sitemap;
//use Spatie\Sitemap\SitemapGenerator;

//use proj4php\Proj4php;
//use proj4php\Proj;
//use proj4php\Point;

class PageController extends Controller
{
    public function home(Request $request)
    {

//        $proj4 = new Proj4php();
//
//        $projL93    = new Proj('EPSG:3857', $proj4);
//        $projWGS84  = new Proj('EPSG:4326', $proj4);
//
//        $pointSrc = new Point(3555996.49305805, 6489873.2399244, $projL93);
//
//        $pointDest = $proj4->transform($projWGS84, $pointSrc);
//        dd($pointSrc->toShortString()) ;

//        SitemapGenerator::create('https://uland')->writeToFile('sitemap.xml');
//        SitemapGenerator::create(config('app.url'))
//            ->writeToFile(public_path('sitemap.xml'));

        $customer = Customer::count();
        $advert = Advert::count();
        $advert_today = Advert::where('created_at',  '>=', date('Y-m-d').' 00:00:00')->count();
        $top_rent_adverts_count = Advert::all()->where('search_status', 'Активно')->
        where('top', '!=', NULL)->where('type', 'Оренда')->count();
        $top_sale_adverts_count = Advert::all()->where('search_status', 'Активно')->
        where('top', '!=', NULL)->where('type', 'Продаж')->count();

        $articles = Article::orderBy('id', 'DESC')->get();

        if($top_rent_adverts_count >= 5){
            $top_rent_adverts = Advert::all()->where('search_status', 'Активно')->
            where('type', 'Оренда')->where('top', '!=', NULL)->random(5);
        }else{
            $top_rent_adverts = Advert::all()->where('search_status', 'Активно')->
            where('type', 'Оренда')->where('top', '!=', NULL)->random($top_rent_adverts_count);
        }

        if($top_sale_adverts_count >= 5){
            $top_sale_adverts = Advert::all()->where('search_status', 'Активно')->
            where('type', 'Продаж')->where('top', '!=', NULL)->random(5);
        }else{
            $top_sale_adverts = Advert::all()->where('search_status', 'Активно')->
            where('type', 'Продаж')->where('top', '!=', NULL)->random($top_sale_adverts_count);
        }

        $title = 'Сервіс Uland - земельна дошка оголошень з продажу, оренди, купівлі земельних ділянок в Україні';
        $meta_key = '';
        $meta_description = 'Сервіс Uland - це '.$advert.' оголошень з продажу, оренди та купівлі земельних ділянок в Україні. Обирай ділянку за допомою зручного фільтру!';
        return view('frontend.pages.home',compact(['title', 'customer', 'advert', 'advert_today', 'articles', 'meta_key', 'meta_description', 'top_sale_adverts', 'top_rent_adverts']));
    }

    public function articles()
    {
        $title = 'Новини';
        $meta_key = '';
        $meta_description = '';
        $articles = Article::orderBy('id', 'DESC')->get();
        return view('frontend.pages.articles',compact(['title', 'articles', 'meta_key', 'meta_description']));
    }

    public function support()
    {
        $title = 'Звернення в Тех Підтримку';
        $meta_key = '';
        $meta_description = '';
        return view('frontend.pages.support',compact(['title', 'meta_key', 'meta_description']));
    }

    public function registerStepOne()
    {
        $title = 'Реєстрація крок 1';
        $meta_key = '';
        $meta_description = '';
        return view('frontend.customer.register-step-1',compact(['title', 'meta_key', 'meta_description']));
    }

    public function registerStepTwo()
    {
        if(Session::has('phone')){
            $title = 'Реєстрація крок 2';
            $meta_key = '';
            $meta_description = '';
            $phone = Session::get('phone')[0];
            return view('frontend.customer.register-step-2',compact(['title', 'phone', 'meta_key', 'meta_description']));
        }else{
            $title = '404';
            $meta_key = '';
            $meta_description = '';
            return view('frontend.pages.404',compact(['title', 'meta_key', 'meta_description']));
        }
    }

    public function registerStepThree()
    {
        if(Session::has('reg_status')){
            $title = 'Реєстрація крок 3';
            $meta_key = '';
            $meta_description = '';
            $phone = Session::get('phone');
            return view('frontend.customer.register-step-3',compact(['title', 'phone', 'meta_key', 'meta_description']));
        }else{
            $title = '404';
            $meta_key = '';
            $meta_description = '';
            return view('frontend.pages.404',compact(['title', 'meta_key', 'meta_description']));
        }
    }

    public function successfulRegister()
    {
        $title = 'Дякуємо за реєстрацію';
        $meta_key = '';
        $meta_description = '';
        return view('frontend.pages.successful-register',compact(['title', 'meta_key', 'meta_description']));
    }

    public function searchAgent()
    {
        $title = 'Пошук агентів';
        $meta_key = '';
        $meta_description = '';
        return view('frontend.pages.search-agent',compact(['title', 'meta_key', 'meta_description']));
    }

    public function thank()
    {
        $title = 'Дякуємо';
        $meta_key = '';
        $meta_description = '';
        return view('frontend.pages.thank',compact(['title', 'meta_key', 'meta_description']));
    }

    public function resetStepOne()
    {
        $title = 'Відновлення пароля крок 1';
        $meta_key = '';
        $meta_description = '';
        return view('auth.reset-step-1',compact(['title', 'meta_key', 'meta_description']));
    }

    public function resetStepTwo()
    {
        if(Session::has('phone')){
            $title = 'Відновлення пароля крок 2';
            $meta_key = '';
            $meta_description = '';
            $phone = Session::get('phone')[0];
            return view('auth.reset-step-2',compact(['title', 'phone', 'meta_key', 'meta_description']));
        }else{
            $title = '404';
            $meta_key = '';
            $meta_description = '';
            return view('frontend.pages.404',compact(['title', 'meta_key', 'meta_description']));
        }
    }

    public function resetStepThree()
    {
        if(Session::has('reset_status')){
            $title = 'Відновлення пароля крок 3';
            $meta_key = '';
            $meta_description = '';
            $phone = Session::get('phone');
            return view('auth.reset-step-3',compact(['title', 'phone', 'meta_key', 'meta_description']));
        }else{
            $title = '404';
            $meta_key = '';
            $meta_description = '';
            return view('frontend.pages.404',compact(['title', 'meta_key', 'meta_description']));
        }
    }

    public function successfulReset()
    {
        $title = 'Успішно відновили пароль';
        $meta_key = '';
        $meta_description = '';
        return view('frontend.pages.successful-reset',compact(['title', 'meta_key', 'meta_description']));
    }

    public function search()
    {
        $title = 'Пошук оголошень';
        $meta_key = '';
        $meta_description = '';
        return view('frontend.pages.search',compact(['title', 'meta_key', 'meta_description']));
    }

    public function searchHome(Request $request)
    {
        $type = $request->type;
        $region = $request->row;
        $hidden_region = $request->s_region;
        $district = $request->s_district;
        $city = $request->s_city;
        $cadnum = $request->cadnum;

        $title = 'Пошук оголошень';
        $meta_key = '';
        $meta_description = '';
        return view('frontend.pages.search',compact(['title', 'type', 'region', 'hidden_region', 'district', 'city', 'cadnum', 'meta_key', 'meta_description']));
    }

    public function seoSearchArenda($area)
    {
        $type = 'Оренда';
        $region = Advert::select('city','region','district', 'slug_region')->orWhere('slug_region', $area)->orWhere('slug_city', $area)->where('type', 'Оренда')->first();
        if($region->slug_region == $area){
            $hidden_region = $region->region;
            $district = '';
            $city = '';
            $region = $region->region;

            $title = 'Оренда землі в '.$region.'. Взяти, здати в оренду земельну ділянку – Uland';
            $meta_key = '';
            $meta_description = 'Швидко та просто здати або ✅ взяти в оренду земельну ділянку в '.$region.'. Вигідні ціни. Широкий ⚡ вибір оголошень. Зручний пошук!';
        }else{
            $hidden_region = $region->region;
            $district = $region->district;
            $city = $region->city;

            $title = 'Оренда землі в '.$city.'. Взяти, здати в оренду земельну ділянку – Uland';
            $meta_key = '';
            $meta_description = 'Швидко здати чи ✅ взяти в оренду земельну ділянку в '.$city.'. Низькі ціни. Широкий ⚡ вибір оголошень. Зручний пошук!';
            if($region->disctrict == 'Не визначено'){
                $region = $region->city.', '.$region->region;
            }else{
                $region = $region->city.', '.$region->district.', '.$region->region;
            }
        }


        return view('frontend.pages.search',compact(['title', 'type', 'region', 'hidden_region', 'district', 'city', 'meta_key', 'meta_description']));
    }

    public function seoSearchProdaja($area)
    {
        $type = 'Продаж';
        $region = Advert::select('city','region','district', 'slug_region')->orWhere('slug_region', $area)->orWhere('slug_city', $area)->where('type', 'Продаж')->first();
        if($region->slug_region == $area){
            $hidden_region = $region->region;
            $district = '';
            $city = '';
            $region = $region->region;

            $title = 'Придбати землю в '.$region.' області. Продаж земельних ділянок – Uland';
            $meta_key = '';
            $meta_description = 'Продаж і купівля земельних ділянок в '.$region.' області за ✅ низькими цінами. Широкий ⚡ вибір оголошень. Зручний пошук!';
        }else{
            $hidden_region = $region->region;
            $district = $region->district;
            $city = $region->city;

            $title = ' Придбати земельну ділянку в '.$city.'. Продаж землі різного призначення – Uland';
            $meta_key = '';
            $meta_description = 'Купівля і продаж земельних ділянок в '.$city.' за ✅ низькими цінами. Широкий ⚡ вибір оголошень. Зручний пошук землі!';

            if($region->disctrict == 'Не визначено'){
                $region = $region->city.', '.$region->region;
            }else{
                $region = $region->city.', '.$region->district.', '.$region->region;
            }
        }

        return view('frontend.pages.search',compact(['title', 'type', 'region', 'hidden_region', 'district', 'city', 'meta_key', 'meta_description']));
    }

    public function mapRegions(Request $request)
    {
        //1 - области
        //2 - город без района
        //3 - город с районом
        //4 - район
        $type_1 = Advert::select('region')->where('search_status', 'Активно')->where('region', 'LIKE', '%'.$request->row.'%')->groupBy('region')->get();
        $check = Advert::select('district')->where('city', 'LIKE', '%'.$request->row.'%')->groupBy('district')->get();

        if(count($check) > 1){
            $type_2 = Advert::select('city','region')->where('search_status', 'Активно')->where('city', 'LIKE', '%'.$request->row.'%')->groupBy('city')->get();
        }else{
            $type_2 = Advert::select('city','region')->where('search_status', 'Активно')->where('city', 'LIKE', '%'.$request->row.'%')->where('district', 'Не визначено')->groupBy('city')->get();
        }
        $type_3 = Advert::select('city','district','region')->where('search_status', 'Активно')->where('city', 'LIKE', '%'.$request->row.'%')->where('district', '!=', 'Не визначено')->groupBy('district')->get();
        $type_4 = Advert::select('district','region')->where('search_status', 'Активно')->where('district', 'LIKE', '%'.$request->row.'%')->groupBy('district')->get();

        return response()->json([
            'type_1' => $type_1,
            'type_2' => $type_2,
            'type_3' => $type_3,
            'type_4' => $type_4,
        ]);
    }

    public function profile()
    {
        $title = 'Мій профіль';
        $meta_key = '';
        $meta_description = '';
        return view('frontend.customer.profile',compact(['title', 'meta_key', 'meta_description']));
    }

    public function edit()
    {
        $title = 'Редагування даних';
        $meta_key = '';
        $meta_description = '';
        return view('frontend.customer.edit',compact(['title', 'meta_key', 'meta_description']));
    }

    public function editPassword()
    {
        $title = 'Оновлення пароля';
        $meta_key = '';
        $meta_description = '';
        return view('frontend.customer.edit-password',compact(['title', 'meta_key', 'meta_description']));
    }

    public function checkCadnum()
    {
        $title = 'Перевірка кадастрового номеру';
        $meta_key = '';
        $meta_description = '';
        return view('frontend.customer.advert.check-cadnum',compact(['title', 'meta_key', 'meta_description']));
    }

    public function errorCadnum()
    {
        if(Session::has('cadnum')){
            $title = 'Помилка публікації';
            $meta_key = '';
            $meta_description = '';
            $cadnum = Session::get('cadnum')[0];
            return view('frontend.customer.advert.error.error-cadnum',compact(['title', 'cadnum', 'meta_key', 'meta_description']));
        }else{
            $title = '404';
            $meta_key = '';
            $meta_description = '';
            return view('frontend.pages.404',compact(['title', 'meta_key', 'meta_description']));
        }
    }

    public function agentMessageCadnum()
    {
        if(Session::has('cadnum_info')){
            $title = 'Надіслати повідомлення Володарю';
            $meta_key = '';
            $meta_description = '';
            $cadnum = Session::get('cadnum')[0];
            return view('frontend.customer.advert.error.agent-message-cadnum',compact(['title', 'cadnum', 'meta_key', 'meta_description']));
        }else{
            $title = '404';
            $meta_key = '';
            $meta_description = '';
            return view('frontend.pages.404',compact(['title', 'meta_key', 'meta_description']));
        }
    }

    public function agentConfirm($id, $agent_id, $x)
    {
        $advert = Advert::where('id', $id)->where('x', $x)->first();
        if($advert){
            $title = 'Ви успішно закріпили агента';
            $meta_key = '';
            $meta_description = '';
            Advert::where('id', $id)->where('x', $x)->update([
                'agent_id' => $agent_id
            ]);
            return view('frontend.pages.successful-confirm-agent',compact(['title', 'meta_key', 'meta_description']));
        }else{
            $title = '404';
            $meta_key = '';
            $meta_description = '';
            return view('frontend.pages.404',compact(['title', 'meta_key', 'meta_description']));
        }
    }

    public function agentIssetCadnum()
    {
        if(Session::has('cadnum_info')){
            $title = 'Помилка публікації';
            $meta_key = '';
            $meta_description = '';
            $cadnum = Session::get('cadnum')[0];
            return view('frontend.customer.advert.error.agent-isset-cadnum',compact(['title', 'cadnum', 'meta_key', 'meta_description']));
        }else{
            $title = '404';
            $meta_key = '';
            $meta_description = '';
            return view('frontend.pages.404',compact(['title', 'meta_key', 'meta_description']));
        }
    }

    public function ownerMessageCadnum()
    {
        if(Session::has('cadnum_info')){
            $title = 'Помилка публікації';
            $meta_key = '';
            $meta_description = '';
            $cadnum = Session::get('cadnum')[0];
            return view('frontend.customer.advert.error.owner-message-cadnum',compact(['title', 'cadnum', 'meta_key', 'meta_description']));
        }else{
            $title = '404';
            $meta_key = '';
            $meta_description = '';
            return view('frontend.pages.404',compact(['title', 'meta_key', 'meta_description']));
        }
    }

    public function createAdvert()
    {
        if(Session::has('cadnum_info')){
            $title = 'Публікація оголошення';
            $meta_key = '';
            $meta_description = '';
            $cadnum = Session::get('cadnum_info')[0];
            return view('frontend.customer.advert.create',compact(['title', 'cadnum', 'meta_key', 'meta_description']));
        }else{
            $title = '404';
            $meta_key = '';
            $meta_description = '';
            return view('frontend.pages.404',compact(['title', 'meta_key', 'meta_description']));
        }
    }

    public function successfulPublish()
    {
        $title = 'Дякуємо за публікацію';
        $meta_key = '';
        $meta_description = '';
        return view('frontend.customer.advert.successful-publish',compact(['title', 'meta_key', 'meta_description']));
    }

    public function editAdvert($id)
    {
        $advert = Advert::find($id);
        if(Auth::guard('customer')->user()->role == 'Я агент по продаже'){
            if($advert && $advert->agent_id != Auth::guard('customer')->user()->id){
                $title = '404';
                $meta_key = '';
                $meta_description = '';
                return view('frontend.pages.404',compact(['title', 'meta_key', 'meta_description']));
            }else{
                $title = 'Редагування оголошення';
                $meta_key = '';
                $meta_description = '';
                return view('frontend.customer.advert.edit',compact(['title', 'advert', 'meta_key', 'meta_description']));
            }
        }else{
            if($advert && $advert->agent_id == 0 && $advert->customer_id == Auth::guard('customer')->user()->id){
                $title = 'Редагування оголошення';
                $meta_key = '';
                $meta_description = '';
                return view('frontend.customer.advert.edit',compact(['title', 'advert', 'meta_key', 'meta_description']));
            }else{
                $title = '404';
                $meta_key = '';
                $meta_description = '';
                return view('frontend.pages.404',compact(['title', 'meta_key', 'meta_description']));
            }
        }
    }

    public function agentConfirmCadnum()
    {
        if(Session::has('cadnum_info')){
            $title = 'Закріпити за собою оголошення';
            $meta_key = '';
            $meta_description = '';
            $meta_key = '';
            $meta_description = '';
            $cadnum = Session::get('cadnum')[0];
            return view('frontend.customer.advert.error.agent-confirm-cadnum',compact(['title', 'cadnum', 'meta_key', 'meta_description']));
        }else{
            $title = '404';
            $meta_key = '';
            $meta_description = '';
            return view('frontend.pages.404',compact(['title', 'meta_key', 'meta_description']));
        }
    }

    public function ownerConfirmCadnum()
    {
        if(Session::has('cadnum_info')){
            $title = 'Стати власником';
            $meta_key = '';
            $meta_description = '';
            $cadnum = Session::get('cadnum')[0];
            return view('frontend.customer.advert.error.owner-confirm-cadnum',compact(['title', 'cadnum', 'meta_key', 'meta_description']));
        }else{
            $title = '404';
            $meta_key = '';
            $meta_description = '';
            return view('frontend.pages.404',compact(['title', 'meta_key', 'meta_description']));
        }
    }

    public function agentConfirmCadnumWithAdvertising()
    {
        if(Session::has('cadnum_info')){
            $title = 'Агент вже рекламує оголошення';
            $meta_key = '';
            $meta_description = '';
            $cadnum = Session::get('cadnum')[0];
            return view('frontend.customer.advert.error.agent-confirm-cadnum-with-advertising',compact(['title', 'cadnum']));
        }else{
            $title = '404';
            $meta_key = '';
            $meta_description = '';
            return view('frontend.pages.404',compact(['title', 'meta_key', 'meta_description']));
        }
    }

    public function ownerConfirmCadnumWithAdvertising()
    {
        if(Session::has('cadnum_info')){
            $title = 'Стати власником';
            $meta_key = '';
            $meta_description = '';
            $cadnum = Session::get('cadnum')[0];
            return view('frontend.customer.advert.error.owner-confirm-cadnum-with-advertising',compact(['title', 'cadnum', 'meta_key', 'meta_description']));
        }else{
            $title = '404';
            $meta_key = '';
            $meta_description = '';
            return view('frontend.pages.404',compact(['title', 'meta_key', 'meta_description']));
        }
    }

    public function favorites()
    {
        $title = 'Вибрані оголошення';
        $meta_key = '';
        $meta_description = '';
        return view('frontend.customer.favorite',compact(['title', 'meta_key', 'meta_description']));
    }

    public function advertise($id)
    {
        $advert = Advert::find($id);
        if($advert && $advert->customer_id == Auth::guard('customer')->user()->id || $advert->agent_id == Auth::guard('customer')->user()->id){
            $title = 'Реклама оголошення';
            $meta_key = '';
            $meta_description = '';
            return view('frontend.customer.advertise',compact(['title', 'advert', 'meta_key', 'meta_description']));
        }else{
            $title = '404';
            $meta_key = '';
            $meta_description = '';
            return view('frontend.pages.404',compact(['title', 'meta_key', 'meta_description', 'meta_key', 'meta_description']));
        }
    }

}
