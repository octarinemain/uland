<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Models\Support;
use App\Http\Controllers\Controller;
use App\Mail\ContactMessage;
use Illuminate\Support\Facades\Mail;

class SupportController extends Controller
{
    public function create(Request $request)
    {
        $file = $request->file;
        if($file){
            $file->store('File');
            $file = $file->hashName();
        }else{
            $file = '';
        }

        Mail::to(env("MAIL_USERNAME"))->send(new ContactMessage($request));

        Support::create([
           'type' => $request->type,
           'name' => $request->name,
           'email' => $request->email,
           'description' => $request->description,
           'file' => $file,
        ]);

        return redirect()->back()->with('success', ['Send']);

    }

}
