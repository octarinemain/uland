<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Models\SessionItem;


class Customer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard('customer')->check()) {
            return $next($request);
        } else {
            return redirect('/customer/login');
        }
    }
}
