<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;

class EditCustomer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'birthday' => 'required',
            'name' => 'required|min:2|max:50',
            'surname' => 'required|min:2|max:50',
            'city' => 'required|min:1|max:100',
            'email' => 'required|min:1|max:50',
        ];
    }
}