<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class Login extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function prepareForValidation()
    {
        $this->merge(['phone'=> '+380'.$this->phone]);
    }

    public function rules(Request $request)
    {
        return [
            'phone' => 'required|exists:customers',
            'password' => 'required|min:6',
            'g-recaptcha-response' => 'required|captcha',

        ];
    }
}