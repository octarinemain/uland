<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;



class DetachAgent extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($advert)
    {
        $this->advert = $advert;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $advert = $this->advert;
        $from = env('MAIL_FROM');
        return $this->from($from, 'Uland')->subject("Зміни в оголошенні")->view('mail.detach-agent')->with(compact(['advert']));
    }
}
