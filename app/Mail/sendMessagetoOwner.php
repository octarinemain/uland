<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;



class sendMessagetoOwner extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($message,$id,$x)
    {
        $this->message = $message;
        $this->id = $id;
        $this->x = $x;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $new_message = $this->message;
        $id = $this->id;
        $x = $this->x;
        $customer = Auth::guard('customer')->user();
        $from = env('MAIL_FROM');
        return $this->from($from, 'Uland')->subject("Запит на права продажу землі")->view('mail.send-message-to-owner')->with(compact(['new_message', 'customer', 'id', 'x']));
    }
}
