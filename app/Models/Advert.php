<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\CustomerResetPasswordNotification;

class Advert extends Authenticatable
{


    protected $fillable = [
        'customer_id', 'agent_id', 'type', 'cadnum', 'ga', 'ga_to_sell', 'price', 'currency', 'purpose', 'use_с' ,'region', 'slug_region', 'district', 'slug_district', 'city', 'slug_city',  'coatuu', 'zone', 'quartal', 'comment_1', 'comment_2',
        'comment_3', 'discount', 'attr_1', 'attr_2', 'attr_3', 'attr_4', 'attr_5', 'views','search_status', 'x', 'y', 'price_per_ga','is_new',
        'once_top_day', 'top', 'red', 'sort_date', 'meta_keywords', 'meta_description'
    ];


    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }

    public function agent_customer()
    {
        return $this->belongsTo('App\Models\Customer', 'agent_id', 'id');
    }

    public function images()
    {
        return $this->hasMany('App\Models\AdvertImage');
    }


}
