<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\CustomerResetPasswordNotification;

class AdvertHistory extends Authenticatable
{

    protected $fillable = [
        'advert_id','agent_id', 'customer_id', 'price', 'type'
    ];

    protected $table = 'advert_histories';

    public function advert()
    {
        return $this->belongsTo('App\Models\Advert');
    }

}
