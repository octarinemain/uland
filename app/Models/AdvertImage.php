<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\CustomerResetPasswordNotification;

class AdvertImage extends Authenticatable
{


    protected $fillable = [
        'advert_id', 'image'
    ];


    public function advert()
    {
        return $this->belongsTo('App\Models\Advert');
    }


}
