<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
        'small_description', 'title', 'description', 'slug', 'preview',  'meta_keywords', 'meta_description',
    ];
}
