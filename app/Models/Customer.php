<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\CustomerResetPasswordNotification;

class Customer extends Authenticatable
{
    use Notifiable;

    protected $guard = 'customer';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'role', 'email', 'password', 'phone','surname', 'birthday',  'city', 'avatar', 'token', 'email_is_active', 'is_new'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function all_adverts()
    {
        return $this->hasMany('App\Models\Advert')->orderByDesc('sort_date')->paginate(env('APP_PAGINATION'));
    }

    public function adverts()
    {
        return $this->hasMany('App\Models\Advert')->where('agent_id', 0)->orderByDesc('sort_date')->paginate(env('APP_PAGINATION'));
    }

    public function agent_adverts()
    {
        return $this->hasMany('App\Models\Advert', 'agent_id','id')->paginate(env('APP_PAGINATION'));
    }

    public function agent_adverts_for_search()
    {
        return $this->hasMany('App\Models\Advert', 'agent_id','id')->where('search_status', 'Активно');
    }

    public function favorites()
    {
        return $this->hasMany('App\Models\Favorite', 'customer_id','id');
    }
}
