<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\CustomerResetPasswordNotification;

class Favorite extends Authenticatable
{


    protected $fillable = [
        'customer_id', 'advert_id',
    ];


    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }

    public function advert()
    {
        return $this->belongsTo('App\Models\Advert');
    }



}
