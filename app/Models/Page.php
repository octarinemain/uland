<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class Page extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'slug', 'description', 'meta_keywords', 'meta_description', 'is_dynamic'
    ];
}
