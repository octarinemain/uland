<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class Support extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', 'name', 'email', 'description', 'file', 'status'
    ];

}
