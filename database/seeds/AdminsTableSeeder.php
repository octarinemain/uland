<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User;

class AdminsTableSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\Admin::create( [
            'name' => 'Радик' ,
            'email' => 'rozhden.t@gmail.com' ,
            'role' => 'admin' ,
            'password' => Hash::make( '45^$@HEFR#%$@#QDSSfsaw3@#' ),
        ] );
        App\Models\Admin::create( [
            'name' => 'Анатолий',
            'email' => '1sakhno.tolik@gmail.com' ,
            'role' => 'admin' ,
            'password' => Hash::make( '45^$@HEFR#%$@#QDSSfsaw3@#' ),
        ] );
        App\Models\Admin::create( [
            'name' => 'Александр',
            'email' => 'lighthunterua@gmail.com' ,
            'role' => 'admin' ,
            'password' => Hash::make( '45^$@HEFR#%$@#QDSSfsaw3@#' ),
        ] );
    }
}
