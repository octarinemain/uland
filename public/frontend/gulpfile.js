const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const babel = require('gulp-babel');
const rename = require('gulp-rename');
const mincss = require('gulp-clean-css');
const sourcemaps = require('gulp-sourcemaps');
const browserSync = require('browser-sync').create();
const wait = require('gulp-wait');

// SASS/SCSS to CSS
gulp.task('sass', function() {
    return gulp
        .src('src/style/scss/**/*.scss')
        .pipe(wait(500))
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(mincss({compatibility: 'ie8', level: {1: {specialComments: 0}}}))
        .pipe(rename({suffix: '.min'}))
        .pipe(sourcemaps.write('./maps/'))
        .pipe(gulp.dest('css'));
});

//All CSS libs to library.min.css
gulp.task('css-libs', function() {
    return gulp
        .src('src/style/library-css/**/*.css')
        .pipe(concat('library.css'))
        .pipe(mincss({compatibility: 'ie8', level: {1: {specialComments: 0}}}))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('css'));
});

//All JavaScript libs to library.min.js
gulp.task('library', function() {
    return gulp
        .src('src/js/library-js/**/*.js')
        .pipe(concat('library.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('js'));
});

//Main JavaScript babel + uglify
gulp.task('js', function() {
    return gulp
        .src(['node_modules/@babel/polyfill/dist/polyfill.js', 'src/js/main.js'])
        .pipe(sourcemaps.init())
        .pipe(babel())
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(sourcemaps.write('./maps/'))
        .pipe(gulp.dest('js'));
});

// Browser Sync
gulp.task('browser-sync', () =>
    browserSync.init({
        open: 'external',
        host: 'uland',
        proxy: 'http://uland',
        port: 3000,
        notify: false,
        ghostMode: false,
    })
);

// Builder
gulp.task('build', gulp.parallel('sass', 'css-libs', 'library', 'js'));

// All Watch
gulp.task(
    'watch',
    () => (
        gulp.watch('src/style/scss/**/*.scss', gulp.series('sass')),
        gulp.watch('src/js/library-js/**/*.js', gulp.series('library')),
        gulp.watch('src/js/main.js', gulp.series('js')),
        gulp.watch('src/style/library-css/**/*.css', gulp.series('css-libs')),
        gulp.watch('src/style/scss/**/*.scss').on('change', browserSync.reload),
        gulp.watch('src/js/library-js/**/*.js').on('change', browserSync.reload),
        gulp.watch('src/js/main.js').on('change', browserSync.reload),
        gulp.watch('src/style/library-css/**/*.css').on('change', browserSync.reload)
    )
);

// Default Start
gulp.task('default', gulp.series('build', gulp.parallel('watch', 'browser-sync')));
