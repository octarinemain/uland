$(document).ready(function () {
    // Clamping Footer
    function clampingFooter() {
        const footerW = $('.footer').outerHeight();

        $('.height-helper').css('margin-top', '-' + footerW + 'px');
        $('.content-wrap').css('padding-top', footerW + 'px');
    }

    if ($('section').hasClass('section-dark')) {
        $('.height-helper').addClass('dark-theme');
    }

    // Select
    // $('select').selectric({
    //     maxHeight: 200,
    //     disableOnMobile: false,
    //     nativeOnMobile: false
    // });
    $('select').each(function () {
        let $this = $(this);
        let parent = $this.parent();

        initSelect(parent.find('#region'), parent);
        initSelect(parent.find('#district'), parent);
        initSelect(parent.find('#city'), parent);
    });

    function initSelect(el, parentEl) {
        $(el)
            .select2({
                dropdownParent: parentEl,
                language: {
                    noResults() {
                        return 'Нічого не знайдено';
                    },
                },
            })
            .on('select2:opening', function () {
                let search = $(this).data('select2').$dropdown.find(':input.select2-search__field');

                if (!search.attr('placeholder')) {
                    search.attr('placeholder', 'Пошук...');
                }
                return;
            });
    }

    // Connection regions
    function initCheckDataCoordinate() {
        let count = 0;

        let checkDataCoordinate = setInterval(() => {
            if (!!document.getElementById('publish-advert') === !!null) return clearInterval(checkDataCoordinate);
            if (window.dataCoordinate) {
                clearInterval(checkDataCoordinate);
                return initConnectionRegions();
            }
            if (count === 50) return clearInterval(checkDataCoordinate);

            count++;
        }, 200);
    }
    initCheckDataCoordinate();

    function initConnectionRegions() {
        const regionSelect = $('#region');
        const districtSelect = $('#district');
        const citySelect = $('#city');
        let getCoordinates = [
            window.dataCoordinate.place.region,
            window.dataCoordinate.place.district,
            window.dataCoordinate.place.city,
        ];
        let [getRegion, getDistrict, getCity] = getCoordinates;
        let notFound = 'Не визначено';
        let count = 0;
        let cacheData, district, city;

        regionSelect.on('select2:select', function (e) {
            let curRegion = e.params.data.text;

            startFetch(curRegion, getDistricts);
        });

        districtSelect.on('select2:select', function (e) {
            let curDistrict = e.params.data.text;

            startFetch(curDistrict, getCities);
        });

        if (getRegion !== notFound) {
            startFetch(getRegion, getDistricts);
            initCheckCacheData();
        }

        function initCheckCacheData() {
            let checkCacheData = setTimeout(function check() {
                if (getDistrict !== notFound && cacheData) {
                    clearInterval(checkCacheData);
                    return startFetch(getDistrict, getCities);
                }
                if (count === 100) return clearInterval(checkCacheData);

                checkCacheData = setTimeout(check, 100);
                count++;
            }, 100);
        }

        function startFetch(option, nameFn) {
            if (cacheData) return nameFn(cacheData, option);

            $.getJSON('../public/cadastre/cities.json', function (data) {
                cacheData = data;
                nameFn(cacheData, option);
            });
        }

        function getDistricts(data, region) {
            if (districtSelect[0].options.length > 1) removeOptions(districtSelect[0]);
            if (citySelect[0].options.length > 1) removeOptions(citySelect[0]);

            return new Promise(resolve => {
                let findRegion = data.find(item => item[region]);

                for (let prop in findRegion) {
                    regionSelect.val(region).trigger('change');
                    return resolve(findRegion[prop]);
                }
            }).then(fulfilled => {
                fulfilled.forEach(item => {
                    for (let prop in item) {
                        district = new Option(prop);
                        districtSelect.append(district);
                    }
                });
            });
        }

        function getCities(data, district) {
            if (citySelect[0].options.length > 1) removeOptions(citySelect[0]);

            let curRegion = regionSelect.find(':selected').text().toString();

            return new Promise(resolve => {
                let findRegion = data.find(item => item[curRegion]);

                for (let prop in findRegion) {
                    return resolve(findRegion[prop]);
                }
            })
                .then(fulfilled => {
                    let findDistrict = fulfilled.find(item => item[district]);

                    for (let prop in findDistrict) {
                        districtSelect.val(prop).trigger('change');
                        return findDistrict[prop];
                    }
                })
                .then(fulfilled => {
                    if (typeof fulfilled === 'undefined') return console.log('Немає співпадань...');
                    if (!fulfilled.length) return false;

                    fulfilled.forEach(item => {
                        city = new Option(item);
                        citySelect.append(city);

                        if (getDistrict !== notFound && getCity !== notFound && city.innerText === getCity)
                            citySelect.val(getCity).trigger('change');
                    });
                });
        }

        function removeOptions(select) {
            for (let i = select.options.length - 1; i > 0; i--) {
                select.removeChild(select.options[i]);
            }
        }
    }

    // Price num
    const priceNum = () => {
        $('.js-price-num').each(function () {
            const text = $(this).text();

            $(this).text(text.replace(/ /g, '').replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
        });
    };

    priceNum();

    // News Section
    const newsSection = new Swiper('.news-section__swiper', {
        slidesPerView: 4,
        spaceBetween: 65,
        navigation: {
            nextEl: '.slider-control .next',
            prevEl: '.slider-control .prev',
        },
        breakpoints: {
            1366: {
                slidesPerView: 3,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 50,
            },
            599: {
                slidesPerView: 1,
                spaceBetween: 0,
            },
        },
    });

    var advertSwiper = new Swiper('.advert__swiper', {
        slidesPerView: 'auto',
        spaceBetween: 10,
        navigation: {
            nextEl: '.slider-control .next',
            prevEl: '.slider-control .prev',
        },
    });

    const advertControl = $('.slider-control.js-slider-control');
    const advertArrNext = $('.slider-control.js-slider-control .next');
    const advertArrPrev = $('.slider-control.js-slider-control .prev');

    if (advertArrNext.hasClass('swiper-button-disabled') && advertArrPrev.hasClass('swiper-button-disabled')) {
        advertControl.remove();
    }

    var advertBigSwiper = new Swiper('.popup__advert-swiper', {
        slidesPerView: 1,
        spaceBetween: 0,
        autoHeight: true,
        navigation: {
            nextEl: '.next',
            prevEl: '.prev',
        },
    });

    $('.advert__swiper .swiper-slide').on('click', function () {
        var indexHead = $(this).index();
        advertBigSwiper.slideTo(indexHead, 0);
        $('.popup.swiperBig').addClass('js-popup-show');
    });

    if (!!$('.advert__swiper').length && !$('.advert__swiper .swiper-slide img').length) $('.advert__swiper').remove();

    // Сlipping
    if ($('section').hasClass('news-section')) {
        $('.news-section__content p').html((index, currentText) => {
            let valueTextStill = 180;
            let valueText = $(this).text().length;

            if (valueText <= valueTextStill) {
                return currentText;
            }

            return currentText.substr(0, valueTextStill) + '...';
        });
    }

    // Header Profile Menu
    function dropdownFn() {
        let dropdown = $('.ads').find('.dropdown-active');

        dropdown.each(function () {
            if (!$(this).siblings('.dropdown').children().length) $(this).addClass('dropdown-disable');
        });

        $('.dropdown-active').on('click', function () {
            let isActive = $(this).closest('.dropdown-main').hasClass('active');
            let hasChildren = $(this).siblings('.dropdown').children().length;

            $('.dropdown-main').removeClass('active');

            if (!hasChildren) return;

            if (isActive) {
                $(this).closest('.dropdown-main').removeClass('active');
            } else {
                $(this).closest('.dropdown-main').addClass('active');
            }
        });

        $(document).on('click', function (e) {
            let div = $('.dropdown-main');
            if (!div.is(e.target) && div.has(e.target).length === 0) {
                div.removeClass('active');
            }
        });
    }

    dropdownFn();

    // Header user
    $('.header__profile-wrap').on('click', function () {
        $('body').addClass('overflow-min');
    });

    // Header Nav
    $('.header__burger').on('click', function () {
        $('.header__nav').toggleClass('active');
        $('body, html').toggleClass('overflow');
        $(this).toggleClass('active');
    });

    $('.exit-menu').on('click', function () {
        $('.header__profile').removeClass('active');
        $('body, html').removeClass('overflow overflow-min');
    });

    // Resize
    $(window).on('load resize', function () {
        let w = $(this).outerWidth();

        clampingFooter();

        if (w <= 991) {
            if ($('.personCard').hasClass('js-adaptive-person')) {
                $('.personCard__data .personCard__grade').prependTo('.personCard__btn');
                $('.personCard__informWrap .personCard__name').prependTo('.personCard__otherData');
                $('.personCard__roleWrap .personCard__role').appendTo('.personCard__otherData');
            }
        }

        if (w <= 599 || w > 991) {
            if ($('.personCard').hasClass('js-adaptive-person')) {
                $('.personCard__btn .personCard__grade').appendTo('.personCard__data');
                $('.personCard__otherData .personCard__name').prependTo('.personCard__informWrap');
                $('.personCard__otherData .personCard__role').prependTo('.personCard__roleWrap');
            }
        }

        if (w <= 599) {
            if ($('section').hasClass('advert')) {
                $('.advert__main .advert__map').appendTo('.advert__wrap');
            }
        } else {
            if ($('section').hasClass('advert')) {
                $('.advert__wrap > .advert__map').appendTo('.advert__main');
            }
        }

        if (w > 991) {
            $('.header__nav').removeClass('active');
            $('.header__burger').removeClass('active');
        }

        if (w > 599) {
            $('.header__profile').removeClass('active');
            $('body, html').removeClass('overflow overflow-min');
        }

        if (w > 695) {
            $('.search__form').removeClass('active');
        }
    });

    // Input
    function customInput() {
        const inputs = $('.input-group input');

        inputs.each(function () {
            checkInput($(this));
        });

        inputs.on('keyup change', function () {
            checkInput($(this));
        });

        inputs.on('focusin', function () {
            $(this).closest('.input-group').addClass('focus');
        });

        inputs.on('focusout', function () {
            $(this).closest('.input-group').removeClass('focus');
        });

        function checkInput(input) {
            var value = input.val().length;

            if (value !== 0) {
                input.closest('.input-group').addClass('val');
            } else {
                input.closest('.input-group').removeClass('val');
            }
        }
    }

    customInput();

    // Jquery Validate
    $('form').each(function () {
        $(this).validate({
            ignore: [],
            errorClass: 'error',
            validClass: 'success',
            errorElement: 'div',
            wrapper: 'span',
            rules: {
                name: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    },
                },
                surname: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    },
                },
                code: {
                    required: true,
                    minlength: 6,
                    maxlength: 6,
                },
                birthday: {
                    required: true,
                    minlength: 10,
                },
                phone: {
                    required: true,
                    minlength: 9,
                    maxlength: 9,
                },
                cadnum: {
                    required: true,
                    minlength: 22,
                    maxlength: 22,
                },
                email: {
                    required: true,
                    email: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    },
                },
                password: {
                    required: true,
                    minlength: 6,
                },
                password_confirmation: {
                    equalTo: '#password',
                },
                personalAgreement: {
                    required: true,
                },
                city: {
                    required: true,
                },
                message: {
                    required: true,
                },
                ga: {
                    required: '#rent:checked',
                },
                price: {
                    required: true,
                },
                rules: {
                    required: true,
                },
                currency: {
                    required: true,
                },
                role: {
                    required: true,
                },
                attr_1: {
                    required: '#electricity:checked',
                },
                attr_2: {
                    required: '#gas:checked',
                },
                attr_3: {
                    required: '#water:checked',
                },
                attr_4: {
                    required: '#equipment:checked',
                },
                attr_5: {
                    required: '#building:checked',
                },
            },
            messages: {
                phone: {
                    required: 'Вкажіть номер телефону',
                    minlength: 'Номер телефону повинен містити 9 цифр',
                    maxlength: 'Номер телефону повинен містити 9 цифр',
                },
                cadnum: {
                    required: 'Вкажіть кадастровий номер',
                    minlength: 'Кадастровий номер повинен містити 22 цифри',
                    maxlength: 'Кадастровий номер повинен містити 22 цифри',
                },
                email: 'Введіть правильний email',
                rules: 'Потрібна згода',
                password_confirmation: 'Паролі не співпадають',
                name: "Введіть ім'я",
                city: 'Введіть дані',
                surname: 'Введіть Прізвище',
                code: {
                    required: 'Вкажіть код підтвердження',
                    minlength: 'Код повинен містити 6 цифр',
                    maxlength: 'Код повинен містити 6 цифр',
                },
                password: {
                    required: 'Введіть пароль',
                    minlength: 'Пароль повинен містити 6 символів',
                },
                birthday: {
                    required: 'Вкажіть дату народження',
                    minlength: 'Невірний формат дати',
                },
            },
        });
    });

    // Form
    $('.checkbox').on('click', function () {
        const input = $(this).find('input');

        if (input.is(':checked')) {
            $(this).addClass('checked');
        } else {
            $(this).removeClass('checked');
        }
    });

    $('.js-checkbox-slide').on('click', function () {
        const input = $(this).find('input');

        if (input.is(':checked')) {
            $(this).addClass('checked');
            $(this).next().slideToggle();
            $(this).next().find('textarea').prop('disabled', false);
        } else {
            $(this).removeClass('checked');
            $(this).next().find('textarea').prop('disabled', true);
            $(this).next().find('textarea').val('');
        }
    });

    // Radio
    $('.radio').on('click', function () {
        const input = $(this).find('input');

        if (input.is(':checked')) {
            $('.radio').removeClass('checked');
            $(this).addClass('checked');
        } else {
            $(this).removeClass('checked');
        }
    });

    // Timer registration
    function regTimer() {
        let min = $('.js-min');
        let sec = $('.js-sec');

        let count = 240;
        let minutes = 60;

        let startMin = Math.floor(count / 60);

        min.text(startMin);
        sec.text('00');

        let countChange = setInterval(function () {
            count--;
            minutes--;

            min.text(Math.floor(count / 60));

            if (minutes < 10) {
                sec.text('0' + minutes);
            } else {
                sec.text(minutes);
            }

            if (minutes === 0) {
                minutes = 60;
                startMin--;
            }

            if (startMin === 0) {
                clearInterval(countChange);
                $('.form-send__timing').css('display', 'none');
                $('.js-timing-step-2').css('display', 'block');
            }
        }, 1000);
    }

    if ($('form').hasClass('register-step-2')) {
        regTimer();
    }

    $('.input-group--select select').on('change', function () {
        $(this).addClass('change');
        $(this).closest('.input-group--select').addClass('change');
    });

    // Mask
    $('#birthday').mask('00/00/0000');
    $('#cadnum, #cadNumSearch').mask('0000000000:00:000:0000');

    // Auto Complete
    if ($('*').hasClass('get-api-region')) {
        const region = [
            'Автономна Республіка Крим',
            'Вінницька область',
            'Волинська область',
            'Дніпропетровська область',
            'Донецька область',
            'Житомирська область',
            'Закарпатська область',
            'Запорізька область',
            'Івано-Франківська область',
            'Київська область',
            'Кіровоградська область',
            'Луганська область',
            'Львівська область',
            'Миколаївська область',
            'Одеська область',
            'Полтавська область',
            'Рівненська область',
            'Сумська область',
            'Тернопільська область',
            'Харківська область',
            'Херсонська область',
            'Хмельницька область',
            'Черкаська область',
            'Чернівецька область',
            'Чернігівська область',
        ];
        let citys = [];

        $.getJSON('../public/citys.json', data => {
            data.map(e => {
                citys.push(e);
            });
        });

        function autocomplete(id, param) {
            $('#' + id).autoComplete({
                minChars: 1,
                source: function (term, suggest) {
                    term = term.toLowerCase();
                    var choices = param;
                    var suggestions = [];

                    for (let i = 0; i < choices.length; i++) {
                        if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
                    }

                    suggest(suggestions);
                },
            });
        }

        if ($('*').hasClass('city-request')) {
            autocomplete('city', citys);

            $('.js-selectCurrent-1').addClass('js-active-input');

            $('.js-select-form select').on('change', function () {
                var value = $(this).val();

                $('.js-selectCurrent-1, .js-selectCurrent-2').removeClass('val').find('input').val('');

                if (value === 'Я агент по продаже') {
                    $('.js-selectCurrent-1').removeClass('js-active-input');
                    $('.js-selectCurrent-2').addClass('js-active-input');
                    $('.js-selectCurrent-1')
                        .css('display', 'none')
                        .find('input')
                        .prop('disabled', true)
                        .prop('hidden', true);
                    $('.js-selectCurrent-2')
                        .css('display', 'block')
                        .find('input')
                        .prop('disabled', false)
                        .prop('hidden', false);
                    $('#city').autoComplete('destroy');
                    autocomplete('region', region);
                } else if (value === 'Я собственник') {
                    $('.js-selectCurrent-2').removeClass('js-active-input');
                    $('.js-selectCurrent-1').addClass('js-active-input');
                    $('.js-selectCurrent-1')
                        .css('display', 'block')
                        .find('input')
                        .prop('disabled', false)
                        .prop('hidden', false);
                    $('.js-selectCurrent-2')
                        .css('display', 'none')
                        .find('input')
                        .prop('disabled', true)
                        .prop('hidden', true);
                    $('#region').autoComplete('destroy');
                    autocomplete('city', citys);
                }
            });
        }

        if ($('*').hasClass('search-agent')) {
            autocomplete('searchRegionArent', region);
        }

        // if ($('*').hasClass('searchRegion')) {
        //     autocomplete('searchRegion', region);
        // }

        if ($('.city-request-api').length !== 0) {
            autocomplete('city', citys);
        }

        if ($('.region-request-api').length !== 0) {
            $('.region-request-api').autoComplete({
                minChars: 1,
                source: function (term, suggest) {
                    term = term.toLowerCase();
                    var choices = region;
                    var suggestions = [];

                    for (let i = 0; i < choices.length; i++) {
                        if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
                    }

                    suggest(suggestions);
                },
            });
        }
    }

    // Radio Change form
    $('.js-change-radio input').on('change', function () {
        var value = $(this).val();

        if (value === 'Продаж') {
            $('#ga').closest('.input-group').css('display', 'none');
        } else if (value === 'Оренда') {
            $('#ga').closest('.input-group').css('display', 'block');
        }
    });

    if ($('#rent').is(':checked')) {
        $('#ga').closest('.input-group').css('display', 'block');
    } else if ($('#sale').is(':checked')) {
        $('#ga').closest('.input-group').css('display', 'none');
    }

    // Input file
    function inputFile() {
        var $uploadCrop;

        function readFile(input) {
            var size = input.files[0].size;
            var sizeMax = 5000000;

            var type = input.files[0].type;
            var png = 'image/png';
            var jpeg = 'image/jpeg';
            var jpg = 'image/jpg';

            if ((type === png || type === jpeg || type === jpg) && size <= sizeMax) {
                $('.registration__photo-wrap .error-custom').removeClass('error-file');
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $uploadCrop
                            .croppie('bind', {
                                url: e.target.result,
                            })
                            .then(function () {
                                $('.popup.cropper').addClass('js-popup-show');
                                $('body').addClass('no-scrolling');
                            });
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            } else {
                $('.registration__photo-wrap .error-custom').addClass('error-file');
            }
        }

        $uploadCrop = $('#upload-demo').croppie({
            viewport: {
                width: 120,
                height: 120,
                type: 'circle',
            },
            enableExif: true,
        });

        $('#upload').on('change', function () {
            readFile(this);
        });

        $('.upload-result').on('click', function (e) {
            e.preventDefault();
            $uploadCrop
                .croppie('result', {
                    type: 'base64',
                    size: 'viewport',
                })
                .then(function (resp) {
                    $('#userPhoto').attr('src', resp);
                    $('#upload-success').attr('value', resp).attr('src', resp);

                    $('.registration__photo-wrap').addClass('upload-success');

                    $('.popup.cropper').removeClass('js-popup-show');
                    $('body').removeClass('no-scrolling');
                });
        });
    }

    inputFile();

    // Change text in popup
    function changeTextPopup(el) {
        const elDescription = $('#text-description');
        const elNoSoldBtn = $('#close-advert-btn');
        const elSoldBtn = $('#close-advert-btn-ok');
        let description;

        if (el === 'Оренда') {
            description = 'Вкажіть вартість, по якій Ви здали в оренду землю:';
            elDescription.text(description);
            elNoSoldBtn.text('Не здана');
            elSoldBtn.text('Земля здана');
        }

        if (el === 'Продаж') {
            description = 'Вкажіть вартість, по якій Ви продали землю:';
            elDescription.text(description);
            elNoSoldBtn.text('Не продана');
            elSoldBtn.text('Замля продана');
        }
    }

    // PopUp
    function popUp() {
        $('body').on('click', '.js-popup-button', function (e) {
            e.preventDefault();
            let $this = $(this);

            $('.popup').removeClass('js-popup-show');
            var popupClass = '.' + $this.attr('data-popupShow');
            $(popupClass).addClass('js-popup-show');
            $('body').addClass('no-scrolling');

            if ($this.hasClass('btn--deactivate') && $this.attr('data-id') && $this.attr('data-type')) {
                $('.close-advert').attr({
                    'data-id': $this.attr('data-id'),
                    'data-type': $this.attr('data-type'),
                });
                changeTextPopup($this.attr('data-type'));
                $('#soldPrice').removeClass('error');
                window.btnDeactivate = $this;
            }
        });

        // Close PopUp
        $('.js-close-popup').on('click', function (e) {
            e.preventDefault();
            $('.popup').removeClass('js-popup-show');
            $('body').removeClass('no-scrolling');
        });

        $('.popup').on('click', function (e) {
            var div = $('.popup__wrap');
            if (!div.is(e.target) && div.has(e.target).length === 0) {
                $('.popup').removeClass('js-popup-show');
                $('body').removeClass('no-scrolling');
            }

            // if ($('div').hasClass("popup__advert-wrap")) {
            //     var divSlider = $(".popup__advert-wrap");
            //     if (!divSlider.is(e.target) && divSlider.has(e.target).length === 0) {
            //         $('.popup').removeClass('js-popup-show');
            //         $('body').removeClass('no-scrolling');
            //     }
            // }
        });
    }

    popUp();

    // Proof
    $('.js-show-proof').on('click', function () {
        $(this).closest('.reviews__card').find('.reviews__proof').slideToggle();
    });

    // Upload photos advert
    function readURL(input, parent, _this) {
        var size = input.files[0].size;
        var sizeMax = 10000000;

        var type = input.files[0].type;
        var png = 'image/png';
        var jpeg = 'image/jpeg';
        var jpg = 'image/jpg';

        if ((type === png || type === jpeg || type === jpg) && size <= sizeMax) {
            if (input.files && input.files[0]) {
                parent.find('.error-custom').removeClass('error-file');
                parent.addClass('load');

                var reader = new FileReader();

                reader.onload = function (e) {
                    parent.addClass('active');
                    parent.removeClass('load');
                    parent.css('background', 'url(' + e.target.result + ') no-repeat center / cover');
                };

                reader.readAsDataURL(input.files[0]);
            }
        } else {
            parent.removeClass('active').css('background', 'transparent');
            clearInput(_this);
            parent.find('.error-custom').addClass('error-file');
        }
    }

    function clearInput(elem) {
        elem.replaceWith('<input type="file" name="image[]" accept="image/*,image/jpeg" class="upload-photos__file">');
    }

    $('.upload-photos').on('change', '.upload-photos__file', function () {
        readURL($(this)[0], $(this).closest('.upload-photos__card'), $(this));
    });

    $('.upload-photos').on('click', '.upload-photos__exit', function (e) {
        e.preventDefault();
        let indexLast = $('.upload-photos .upload-photos__card').length - 1;

        $(this).closest('.upload-photos__card').removeClass('active').css('background', 'transparent');
        clearInput($(this).closest('.upload-photos__card').find('input'));

        $('.upload-photos__card').eq(indexLast).after($(this).closest('.upload-photos__card'));

        // $(this).closest('.upload-photos__card').appendTo('.upload-photos');
    });

    // Edit photos advert
    if ($('*').hasClass('js-edit-upload-photos')) {
        $('.js-edit-upload-photos .upload-photos__card.active').each(function () {
            let urlPhoto = $(this).attr('data-photo');

            $(this).css('background', 'url(' + urlPhoto + ') no-repeat center / cover');

            $(this).removeAttr('data-photo');
        });

        let uploadLength = $('.upload-photos__card.active').length;
        let uploadNew = 10 - uploadLength;

        if (uploadLength !== 10) {
            for (let i = 1; i < uploadNew; i++) {
                $('.js-default-upload').clone().removeClass('js-default-upload').appendTo('.upload-photos');
            }
        } else if (uploadLength === 10) {
            $('.js-default-upload').css('display', 'none');
        }
    }

    // Map Advert
    if ($('*').hasClass('advert__map')) {
        google.maps.event.addDomListener(window, 'load', init);
    }

    function init() {
        const x = $('#advert__map').attr('data-x');
        const y = $('#advert__map').attr('data-y');

        const mapOptions = {
            zoomControl: false,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            rotateControl: false,
            fullscreenControl: false,
            zoom: 12,
            center: new google.maps.LatLng(x, y),
        };

        const mapElement = document.getElementById('advert__map');

        const map = new google.maps.Map(mapElement, mapOptions);

        const customMarker = '/public/frontend/img/svg/location-red.svg';

        const marker = new google.maps.Marker({
            position: new google.maps.LatLng(x, y),
            icon: {
                url: customMarker,
                scaledSize: new google.maps.Size(40, 40),
            },
            map: map,
            title: 'Marker',
        });
    }

    // Toggle map
    $('.search__toggleMap').on('click', function () {
        $('body').toggleClass('overflow-search');

        const formWrap = $('.search__formWrap');

        formWrap.addClass('hidden');
        setTimeout(function () {
            $('.search__form').toggleClass('active');
        }, 200);
        setTimeout(function () {
            formWrap.removeClass('hidden');
        }, 800);
    });

    // Function for Search Adverts----------
    function searchAdverts(formData, checkSwitch) {
        const wrap = $('#similar-result');

        function checkSwitchFn(data) {
            if (checkSwitch) {
                quantitySearch(data.length, 'Advert');
                $('.js-loadMore').off();
                $('.ads__wrap').html('');
                $('.search__advert').removeClass('active');
                searchMapInit(data);
                checkData(data, '.ads__wrap', $('#cadNumSearch'));
                showCardSearch(data, createAdvert, '.ads__wrap', 50);
                priceNum();
            } else {
                quantitySearch(data.length, 'Advert');
                searchMapInit(data);
                checkData(data, '.ads__wrap', $('#cadNumSearch'));
                showCardSearch(data, createAdvert, '.ads__wrap', 50);
                $('.js-loadMoreWrap').removeClass('preload');
                priceNum();
            }
        }

        function cleanInfo() {
            quantitySearch([].length, 'Advert');
            searchMapInit([]);
            checkData([], '.ads__wrap');
            showCardSearch([], createAdvert, '.ads__wrap', 50);
            $('.js-loadMoreWrap').removeClass('preload');
        }

        function checkSimilarAdverts(data) {
            if (![...data].length) {
                wrap.removeClass('active').find('.ads__wrap-similar').html('');
            } else {
                wrap.addClass('active').find('.ads__wrap-similar').html('');
                showCardSearch(data, createAdvert, '.ads__wrap-similar', 50);
            }
        }

        // OLD
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '/search',
            type: 'POST',
            contentType: false,
            processData: false,
            data: formData,
            beforeSend: function () {
                $('.loader').addClass('active');
            },
            success: function (data) {
                $('.loader').removeClass('active');

                /*
                    Now we have 2 type of response
                    1. if data.adverts - it's default response with array of adverts
                    2. if data.cadnum - it's tell for us that we don't have this cadnum, in our DB, but in general he isset
                */
                if (data.hasOwnProperty('adverts')) {
                    let inputHasVal;

                    !$('#cadNumSearch').val() ? (inputHasVal = false) : (inputHasVal = true);

                    if ([...data.adverts].length) {
                        checkSwitchFn(data.adverts);

                        data.hasOwnProperty('similar_adverts') && inputHasVal
                            ? checkSimilarAdverts(data.similar_adverts)
                            : wrap.removeClass('active').find('.ads__wrap-similar').html('');
                    } else {
                        cleanInfo();
                        wrap.removeClass('active').find('.ads__wrap-similar').html('');
                    }
                } else {
                    // data.cadnum and data.coordinate
                    quantitySearch([].length, 'Advert');
                    searchMapInit([]);
                    checkData([], '.ads__wrap', $('#cadNumSearch'));
                    showCardSearch([], createAdvert, '.ads__wrap', 50);
                    $('.js-loadMoreWrap').removeClass('preload');

                    proj4.defs([
                        ['EPSG:4326', '+proj=longlat+ellps=WGS84+datum=WGS84+no_defs+towgs84=0,0,0'],
                        [
                            'EPSG:4284',
                            '+proj=longlat +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +no_defs',
                        ],
                    ]);
                    const coordinate = proj4('EPSG:3857', 'EPSG:4326', [
                        parseInt(data.coordinate.st_xmin),
                        parseInt(data.coordinate.st_ymin),
                    ]);
                    formData = new FormData();
                    formData.append('x', coordinate[0]);
                    formData.append('y', coordinate[1]);
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        },
                        url: '/customer/getCoordinateInfo',
                        type: 'POST',
                        contentType: false,
                        processData: false,
                        data: formData,
                        success: function (response) {
                            /*
                            1. coordinate - coordinates for google map point for new advert point
                            2. response - Сумська область and Какой-то район, то есть в этом обьекте из прошло аякса
                            лежат данные об регионе и районе, котоыре нужно будет вывести на метку
                            3. data.cadnum - в этом обьекте из прошло аякса лежат данные про гектары обьекта, которые
                            выведем на карточку которую построим по координатам
                            */
                            console.log(data);
                            if (!$('meta').is('[name="customer"]')) {
                                // значит что юез не залогинен и нужно показать ему попап реги или логина при клике на добавить себе обьявление,
                                // после того, как не нашли его в нашей БД но нашли в кадастре
                                alert('login');
                            } else {
                                //редирект на этот урл - data.check_cadnum
                                alert(data.check_cadnum);
                            }
                        },
                    });
                }
            },
        });
        // END OLD

        // return new Promise(resolve => {
        //     $.ajax({
        //         headers: {
        //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        //         },
        //         url: '/search',
        //         type: 'POST',
        //         contentType: false,
        //         processData: false,
        //         data: formData,
        //         beforeSend() {
        //             $('.loader').addClass('active');
        //         },
        //         success(data) {
        //             $('.loader').removeClass('active');
        //             console.log(data);
        //             return resolve(data);
        //         },
        //     });
        // })
        //     .then(fulfilled => {
        //         /*
        //             Now we have 2 type of response
        //             1. if data.adverts - it's default response with array of adverts
        //             2. if data.cadnum - it's tell for us that we don't have this cadnum, in our DB, but in general he isset
        // 	    */
        //         if (fulfilled.hasOwnProperty('adverts')) {
        //             let inputHasVal;

        //             !$('#cadNumSearch').val() ? (inputHasVal = false) : (inputHasVal = true);

        //             if ([...fulfilled.adverts].length) {
        //                 checkSwitchFn(fulfilled.adverts);

        //                 fulfilled.hasOwnProperty('similar_adverts') && inputHasVal
        //                     ? checkSimilarAdverts(fulfilled.similar_adverts)
        //                     : wrap.removeClass('active').find('.ads__wrap-similar').html('');
        //             } else {
        //                 cleanInfo();
        //                 wrap.removeClass('active').find('.ads__wrap-similar').html('');
        //             }
        //         } else {
        //             // data.cadnum and data.coordinate
        //             quantitySearch([].length, 'Advert');
        //             searchMapInit([]);
        //             checkData([], '.ads__wrap', $('#cadNumSearch'));
        //             showCardSearch([], createAdvert, '.ads__wrap', 50);
        //             $('.js-loadMoreWrap').removeClass('preload');

        //             return fulfilled;
        //         }
        //     })
        //     .then(fulfilled => {
        //         if (typeof fulfilled === 'undefined') return;

        //         let data = fulfilled;
        //         proj4.defs([
        //             ['EPSG:4326', '+proj=longlat+ellps=WGS84+datum=WGS84+no_defs+towgs84=0,0,0'],
        //             [
        //                 'EPSG:4284',
        //                 '+proj=longlat +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +no_defs',
        //             ],
        //         ]);
        //         let coordinate = proj4('EPSG:3857', 'EPSG:4326', [
        //             parseInt(data.coordinate.st_xmin),
        //             parseInt(data.coordinate.st_ymin),
        //         ]);

        //         return {
        //             data,
        //             coordinate,
        //         };
        //     })
        //     .then(fulfilled => {
        //         if (typeof fulfilled === 'undefined') return;

        //         let dataFulfilled = fulfilled;
        //         let dataPlace;
        //         formData = new FormData();
        //         formData.append('x', fulfilled.coordinate[0]);
        //         formData.append('y', fulfilled.coordinate[1]);

        //         return new Promise(resolve => {
        //             $.ajax({
        //                 headers: {
        //                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        //                 },
        //                 url: '/customer/getCoordinateInfo',
        //                 type: 'POST',
        //                 contentType: false,
        //                 processData: false,
        //                 data: formData,
        //                 success(response) {
        //                     return resolve(response);
        //                 },
        //             });
        //         }).then(fulfilled => {
        //             dataPlace = fulfilled;

        //             return {
        //                 dataFulfilled,
        //                 dataPlace,
        //             };
        //         });
        //     })
        //     .then(fulfilled => {
        //         if (typeof fulfilled === 'undefined') return;

        //         const addAdvert = $('#add-advert');
        //         /*
        //         1. coordinate - coordinates for google map point for new advert point
        //         2. response - Сумська область and Какой-то район, то есть в этом обьекте из прошло аякса
        //         лежат данные об регионе и районе, котоыре нужно будет вывести на метку
        //         3. data.cadnum - в этом обьекте из прошло аякса лежат данные про гектары обьекта, которые
        //         выведем на карточку которую построим по координатам
        // 	    */
        //         searchMapInit(fulfilled, true);

        //         if (!$('meta').is('[name="customer"]')) {
        //             addAdvert.attr('data-popupShow', 'favorites-login').addClass('js-popup-button');
        //         } else {
        //             addAdvert.attr('href', `${fulfilled.dataFulfilled.data.check_cadnum}`);
        //         }

        //         formData = new FormData();
        //         formData.append('ga', fulfilled.dataFulfilled.data.cadnum.area);
        //         formData.append('district', fulfilled.dataPlace.place.district);
        //         formData.append('region', fulfilled.dataPlace.place.region);

        //         return new Promise(resolve => {
        //             $.ajax({
        //                 headers: {
        //                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        //                 },
        //                 url: '/search/get-similar-advert',
        //                 type: 'POST',
        //                 contentType: false,
        //                 processData: false,
        //                 data: formData,
        //                 success(response) {
        //                     return resolve(response);
        //                 },
        //             });
        //         });
        //     })
        //     .then(fulfilled => {
        //         if (typeof fulfilled !== 'undefined') checkSimilarAdverts(fulfilled.similar_adverts);
        //     });
    }

    // Search Page ----------------
    if ($('div').hasClass('search__map')) {
        let checkSwitch = false;

        searchAdverts(new FormData($('.ajax-search-form')[0]), checkSwitch);
    }

    if ($('section').hasClass('search-agent')) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '/search-agent',
            type: 'POST',
            success: function (data) {
                quantitySearch(data.length, 'Agent');
                checkData(data, '.search-agent__wrapCard');
                showCardSearch(data, createAgent, '.search-agent__wrapCard', 12);
                $('.js-loadMoreWrap').removeClass('preload');
            },
        });
    }

    function searchMapInit(dataMarker, coordinate = null) {
        const mapOptions = {
            zoomControl: false,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            rotateControl: false,
            fullscreenControl: false,
            zoom: 6,
            center: new google.maps.LatLng(48.5, 31.48278),
        };
        const mapElement = document.getElementById('search__map');
        const map = new google.maps.Map(mapElement, mapOptions);
        const customMarker = '/public/frontend/img/svg/location-red.svg';
        const customActiveMarker = '/public/frontend/img/svg/location-blue.svg';

        let allMarkers = [];

        function addMarker(x, y, id = null) {
            let marker = new google.maps.Marker({
                position: new google.maps.LatLng(y, x),
                icon: {
                    url: customMarker,
                    scaledSize: new google.maps.Size(40, 40),
                },
                map: map,
                title: id ? 'Оголошення' : 'Земельна ділянка',
                marker_id: id ? id : null,
            });

            allMarkers.push(marker);

            marker.addListener('click', () => {
                $('.search__advert').removeClass('active');

                if (marker.icon.url == customActiveMarker) {
                    marker.setIcon({
                        url: customMarker,
                        scaledSize: new google.maps.Size(40, 40),
                    });
                    return;
                }

                allMarkers.map(e => e.setIcon({url: customMarker, scaledSize: new google.maps.Size(40, 40)}));
                marker.setIcon({
                    url: customActiveMarker,
                    scaledSize: new google.maps.Size(40, 40),
                });

                if (id) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        },
                        url: '/map/getAdvert',
                        type: 'POST',
                        data: {id: marker.marker_id},
                        success: function (data) {
                            advertInfo(data);
                        },
                    });
                } else {
                    advertInfo(dataMarker);
                }
            });
        }

        if (coordinate) {
            addMarker(dataMarker.dataFulfilled.coordinate[0], dataMarker.dataFulfilled.coordinate[1]);
        } else {
            dataMarker.map(({x, y, id}) => {
                addMarker(x, y, id);
            });
        }

        const markerCluster = new MarkerClusterer(map, allMarkers, {
            imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m',
        });

        closeAdvertInfo(allMarkers, customMarker);
    }

    function advertInfo(data) {
        $('#js-search_region').text(
            `${data.hasOwnProperty('dataFulfilled') ? data.dataPlace.place.region : data.region}`
        );

        $('#js-search_district').text(
            `${data.hasOwnProperty('dataFulfilled') ? data.dataPlace.place.district : data.district}`
        );

        data.hasOwnProperty('dataFulfilled')
            ? $('#js-search_viewers').closest('.viewers').css('display', 'none')
            : $('#js-search_viewers').text(data.views).closest('.viewers').css('display', 'flex');

        $('#js-search_ga').text(
            `${
                data.hasOwnProperty('dataFulfilled')
                    ? `${data.dataFulfilled.data.cadnum.area} ${data.dataFulfilled.data.cadnum.unit_area}`
                    : data.type == 'Оголошення'
                    ? `${data.ga_to_sell} Га`
                    : `${data.ga} Га`
            }`
        );

        data.hasOwnProperty('dataFulfilled')
            ? $('#js-search_price_per_ga').closest('.search__advertCard').css('display', 'none')
            : $('#js-search_price_per_ga')
                  .text(`${data.price_per_ga} ${data.currency} за 1 Га`)
                  .closest('.search__advertCard')
                  .css('display', 'flex');

        data.hasOwnProperty('dataFulfilled')
            ? $('#js-search_price').closest('.search__advertCard').css('display', 'none')
            : $('#js-search_price')
                  .text(`${data.price} ${data.currency}`)
                  .closest('.search__advertCard')
                  .css('display', 'flex');

        data.hasOwnProperty('dataFulfilled')
            ? $('.search__advert .js-favorite').css('display', 'none')
            : $('.search__advert .js-favorite')
                  .attr('data-advert-id', data.id)
                  .removeClass('active')
                  .css('display', 'flex');

        !data.hasOwnProperty('dataFulfilled')
            ? $('#js-search_link')
                  .attr('href', `/advert/id=${data.id}`)
                  .find('span')
                  .text('Перейти на сторінку оголошення')
            : $('meta').is('[name="customer"]')
            ? $('#js-search_link')
                  .attr('href', `${data.dataFulfilled.data.check_cadnum}`)
                  .find('span')
                  .text('Додати оголошення')
            : $('#js-search_link')
                  .attr({href: '#', 'data-popupShow': 'favorites-login'})
                  .addClass('js-popup-button')
                  .find('span')
                  .text('Додати оголошення');

        if (data.favorite == 1) {
            $('.search__advert .js-favorite').addClass('active');
        }

        if (data.discount == 1) {
            $('#discount').show(0);
        } else {
            $('#discount').hide(0);
        }

        if (data.attr_5 != null) {
            $('#attr_5').show(0);
        } else {
            $('#attr_5').hide(0);
        }

        $('.search__advert').addClass('active');
    }

    function closeAdvertInfo(allMarkers, customMarker) {
        $('.search__advertClose').on('click', function () {
            $('.search__advert').removeClass('active');
            allMarkers.map(e => {
                e.setIcon({
                    url: customMarker,
                    scaledSize: new google.maps.Size(40, 40),
                });
            });
        });
    }

    // if ($('input').hasClass('searchRegion')) {
    //
    //     $.ajax({
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //         },
    //         url: '/map/regions',
    //         type: 'POST',
    //         success: function (data) {
    //             let regionsUk = [];
    //
    //             data.map((e) => regionsUk.push(e.region));
    //
    //             $('#searchRegion').autoComplete({
    //                 minChars: 1,
    //                 source: function (term, suggest) {
    //                     term = term.toLowerCase();
    //                     var choices = regionsUk;
    //                     var suggestions = [];
    //
    //                     for (let i = 0; i < choices.length; i++) {
    //                         if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
    //                     }
    //
    //                     suggest(suggestions);
    //                 }
    //             });
    //         },
    //     });
    // }

    // Search region
    const searchRegion = (function (input) {
        let onMask = false;

        input.on('keyup', function (e) {
            let $this = $(this);

            if (!$this.val() && onMask) {
                $this.unmask();
                onMask = false;
            }

            if ($this.closest('.callboard__form').length && !isNaN(Number($this.val()[0]))) {
                if (!onMask) {
                    $this.mask('0000000000:00:000:0000');
                    onMask = true;
                    $this.attr('name', 'cadnum');
                }
            } else {
                if ($this.attr('name') !== 'row') $this.attr('name', 'row');
                $('.drop-search-region').html('');
                $this.attr('data-type', '');
                if (e.target.value.length <= 2) return;

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    },
                    url: '/map/regions',
                    type: 'POST',
                    data: {row: e.target.value},
                    success: data => {
                        let html = '';

                        for (let type in data) {
                            if (data[type].length) {
                                data[type].map(el => {
                                    let liData = [];

                                    for (let key in el) liData.push(el[key]);

                                    html += `<li data-type="${type}">${liData.join(', ')}</li>`;
                                });
                            }
                        }

                        $('.drop-search-region').addClass('active').html(html);
                    },
                });
            }
        });

        function clearSearchRegion() {
            if (onMask) {
                input.unmask();
                onMask = false;
            }

            $('.search_type').val('');
            $('.input-group--searchRegion').removeClass('val');
            input.val('');
            $('.drop-search-region').html('');
            $('.drop-search-region').removeClass('active');
            $('.input-group__clear').removeClass('active');
        }

        return {
            clearSearchRegion,
        };
    })($('#searchRegion'));

    $('.drop-search-region').on('click', 'li', function ({target}) {
        const curentType = target.getAttribute('data-type');
        const curentText = target.innerText;
        const separationArr = curentText.split(', ');
        const city = $('.search_type[name="s_city"]');
        const region = $('.search_type[name="s_region"]');
        const district = $('.search_type[name="s_district"]');

        $('.search_type').val('');

        if (curentType == 'type_1') {
            region.val(curentText);
        }
        if (curentType == 'type_2') {
            city.val(separationArr[0]);
            region.val(separationArr[1]);
        }
        if (curentType == 'type_3') {
            city.val(separationArr[0]);
            district.val(separationArr[1]);
            region.val(separationArr[2]);
        }
        if (curentType == 'type_4') {
            district.val(separationArr[0]);
            region.val(separationArr[1]);
        }

        $('#searchRegion').val(target.innerText);
        $('.input-group--searchRegion').addClass('val');
        $('.drop-search-region').removeClass('active');
    });

    $('.input-group__clear').on('click', function () {
        searchRegion.clearSearchRegion();
    });

    $('.callboard__form').on('submit', function () {
        if ($('#searchRegion').val().length && $('.drop-search-region li').length) {
            let allList = [];
            $('.drop-search-region li').map((index, el) => allList.push(el.innerText));
            const comparison = allList.indexOf($('#searchRegion').val());

            if (comparison != -1) {
                $('.drop-search-region li').eq(comparison).trigger('click');
            } else {
                $('.drop-search-region li').eq(0).trigger('click');
            }
        } else if (/^(\d{10,10}?\:)(\d{2,2}?(\:))(\d{3,3}?\:)(\d{4,4})$/g.test($('#searchRegion').val())) {
            return true;
        } else {
            searchRegion.clearSearchRegion();
        }

        return true;
    });

    function checkInput(val) {
        if (val.length) {
            $('.input-group__clear').addClass('active');
        } else {
            $('.input-group__clear').removeClass('active');
        }
    }

    $('.callboard__data #searchRegion').on('keyup', function (e) {
        checkInput(e.target.value);
    });

    $('.callboard__data #searchRegion').focusout(function (e) {
        checkInput(e.target.value);
    });

    $('.callboard__data #searchRegion').focusin(function (e) {
        checkInput(e.target.value);
    });

    function createAdvert(elem) {
        if (elem.top != null && elem.top != '0000-00-00 00:00:00') {
            var top = 'active';
        } else {
            var top = '';
        }

        if (elem.red != null && elem.red != '0000-00-00 00:00:00') {
            var red = 'allotment';
        } else {
            var red = '';
        }

        const discount = `<div class="extra-card">
                <img src="/public/frontend/img/svg/Auction.svg"
                     alt="">
                <div class="extra-card__inform">
                    <div class="text text--mini">
                        <p>Можливий торг</p>
                    </div>
                </div>
            </div>`;

        const attr_5 = `<div class="extra-card">
                <img src="/public/frontend/img/svg/house.svg" alt="">
                <div class="extra-card__inform">
                    <div class="text text--mini">
                        <p>Є споруди</p>
                    </div>
                </div>
            </div>`;

        const views = `<div class="viewers">
                <div class="viewers__icon">
                    <img src="/public/frontend/img/svg/eye.svg"
                         alt="">
                </div>
                <div class="viewers__value">
                    <div class="text text--gray text--normal">
                        <p>${elem.views}</p>
                    </div>
                </div>
            </div>`;

        return `<div class="ads__cardContent ${top} ${red}">
                <a class="ads__card" href="/advert/id=${elem.id}" target="_blank">
                <div class="ads__cardWrap">
                    <div class="ads__allotment">
                        <p>top</p>
                    </div>
                    <div class="ads__addressInfoWrap">
                        <div class="ads__addressInfo">
                            <div class="ads__section ads__section--first">
                                <div class="text text--gray text--normal">
                                    <p>Регіон</p>
                                </div>
                                <div class="text text--black">
                                    <p>${elem.region}</p>
                                </div>
                            </div>
                            <div class="ads__section">
                                <div class="text text--gray text--normal">
                                    <p>Район</p>
                                </div>
                                <div class="text text--black">
                                    <p>${elem.district}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ads__section">
                        <div class="text text--gray text--normal">
                            <p>Площа, Га</p>
                        </div>
                        <div class="text text--black">
                            <p>${elem.type == 'Продаж' ? elem.ga : elem.ga_to_sell}</p>
                        </div>
                    </div>
                    <div class="ads__priceInfo">
                        <div class="ads__section">
                            <div class="text text--gray text--normal">
                                <p>Ціна за 1 Га</p>
                            </div>
                            <div class="text text--black">
                                <p><span class="js-price-num">${elem.price_per_ga}</span> грн.</p>
                            </div>
                        </div>
                        <div class="ads__section">
                            <div class="text text--gray text--normal">
                                <p>Ціна за все</p>
                            </div>
                            <div class="text text--black">
                                <p><span class="js-price-num">${elem.price}</span> грн.</p>
                            </div>
                        </div>
                        <div class="ads__section ads__section--extra flex-cont">
                            ${elem.discount == 1 ? discount : ''}
                            ${elem.attr_5 != null ? attr_5 : ''}
                            ${elem.views != 0 ? views : ''}
                        </div>
                        <div class="ads__section ads__section--date">
                            <div class="text text--gray text--normal">
                                <p>Дата публікації</p>
                            </div>
                            <div class="text text--black">
                                <p>${elem.created_date}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
                <div class="ads__section ads__section--star js-favorite ${
                    elem.favorite ? 'active' : ''
                }" data-advert-id="${elem.id}">
                    <i class="icon-star-outline">
                        <img src="/public/frontend/img/svg/star-outline.svg" alt="">
                    </i>
                    <i class="icon-star">
                        <img src="/public/frontend/img/svg/star.svg" alt="">
                    </i>
                </div>
            </div>`;
    }

    function createAgent(elem) {
        let adv = quantitySearch(elem.agent_adverts_for_search.length, 'Advert', true);
        const avatar = elem.avatar
            ? `<img src="/public/storage/Avatar/${elem.avatar}" alt="">`
            : '<img src="public/frontend/img/no-avatar.png" alt="">';

        return `<div class="personCard personCard--customView">
                <input hidden class="customer_id" value="${elem.id}" >
                <div class="personCard__fullInfo">
                    <div class="personCard__data">
                        <a href="/customer/id=${elem.id}" target="_blank" class="personCard__photo">
                            ${avatar}
                        </a>
                        <div class="personCard__grade">
                            <div class="personCard__gradeCard">
                                <div class="text text--big">
                                    <p>${elem.agent_adverts_for_search.length}</p>
                                </div>
                                <div class="text text--grayColor text--miniPlus">
                                    <p>${adv}</p>
                                </div>
                            </div>
                            <div class="personCard__gradeCard">
                                <div class="text text--big">
                                    <p>${elem.date_count}</p>
                                </div>
                                <div class="text text--grayColor text--miniPlus">
                                    <p>${elem.date_count_word} з нами</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="personCard__informWrap">
                        <a href="/customer/id=${elem.id}" target="_blank" class="personCard__name">
                            <p>${elem.name} ${elem.surname}</p>
                        </a>
                        <ul class="personCard__info">
                            <li>
                                <div class="personCard__icon">
                                    <img src="/public/frontend/img/svg/location-red.svg" alt="">
                                </div>
                                <div class="text text--grayColor">
                                    <p>${elem.city}</p>
                                </div>
                            </li>
                        </ul>
                        <ul class="personCard__info js-contacts">
                            <li>
                                <div class="personCard__icon">
                                    <img src="/public/frontend/img/svg/telephone.svg" alt="">
                                </div>
                                <a href="tel:" class="text short_phone_attr">
                                    <p class="short_phone"></p>
                                </a>
                            </li>
                            <li class="personCard__infoError">
                                <div class="personCard__icon">
                                    <img src="/public/frontend/img/svg/mail.svg" alt="">
                                </div>
                                <a href="mailto:" class="text short_email_attr">
                                    <p class="short_email"></p>
                                </a>
                            </li>
                            <li class="text js-show-contacts">
                                <p class="text__link">Показати контакти</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>`;
    }

    function showCardSearch(data, create, wrap, paginate) {
        let respData = data;

        if (paginate >= data.length) {
            $('.js-loadMoreWrap').fadeOut(0);
            respData.map(e => {
                $(create(e)).appendTo(wrap);
            });
            respData.splice(0, paginate);
        } else {
            respData.map((e, i) => {
                if (i < paginate) {
                    $(create(e)).appendTo(wrap);
                }
            });
            respData.splice(0, paginate);
        }

        $('.js-loadMore').on('click', function () {
            const button = $('.js-loadMoreWrap');

            button.addClass('load');

            setTimeout(() => {
                respData.map((e, i) => {
                    if (i < paginate) {
                        $(create(e)).appendTo(wrap);
                    }
                });
                respData.splice(0, paginate);

                if (respData.length == 0) button.fadeOut(0);

                button.removeClass('load');
            }, 1000);
        });
    }

    function requestFormSearch() {
        if ($('#searchRegion').val().length && $('.drop-search-region li').length) {
            let allList = [];
            $('.drop-search-region li').map((index, el) => allList.push(el.innerText));
            const comparison = allList.indexOf($('#searchRegion').val());

            if (comparison != -1) {
                $('.drop-search-region li').eq(comparison).trigger('click');
            } else {
                $('.drop-search-region li').eq(0).trigger('click');
            }
        } else {
            searchRegion.clearSearchRegion();
        }

        let checkSwitch = true;

        searchAdverts(new FormData($('.ajax-search-form')[0]), checkSwitch);
    }

    function requestFormSearchAgent(city) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '/search-agent',
            type: 'POST',
            data: {city},
            beforeSend: function () {
                $('.loader').addClass('active');
            },
            success: function (data) {
                $('.loader').removeClass('active');
                quantitySearch(data.length, 'Agent');

                $('.js-loadMore').off();
                $('.search-agent__wrapCard').html('');

                checkData(data, '.search-agent__wrapCard');
                showCardSearch(data, createAgent, '.search-agent__wrapCard', 12);
            },
        });
    }

    $('.ajax-search-form, .ajax-searchAgent-form').on('submit', function (e) {
        e.preventDefault();
        let city = $('#searchRegionArent').val();

        $(this).hasClass('ajax-search-form') ? requestFormSearch() : requestFormSearchAgent(city);
    });

    $('.js-submit-radio-search input').on('change', function () {
        requestFormSearch();
    });

    function checkData(data, wrap, cadastreVal = null) {
        let defaultText = `<div class="text text--blackColor">На жаль, за обраними параметрами нічого не знайдено. Спробуйте змінити параметри пошуку.</div>`;
        let cadastreNotFound = `<div class="text text--blackColor">На жаль, даного кадастрового номеру немає в нашій базі. Ви можете <a href="#" target="_blank" id="add-advert" class="text text--turquoise">додати оголошення</a>.</div>`;

        if (data.length == 0) {
            $('.js-loadMoreWrap').fadeOut(0);

            return !!cadastreVal
                ? !!cadastreVal.val()
                    ? $(wrap).html(cadastreNotFound)
                    : $(wrap).html(defaultText)
                : $(wrap).html(defaultText);
        }

        $('.js-loadMoreWrap').fadeIn(0);
    }

    function quantitySearch(len, param, retur) {
        let input = $('.js-quantity-search');
        let paramSearch =
            param == 'Advert' ? ['оголошення', 'оголошення', 'оголошень'] : ['агент', 'агентa', 'агентів'];
        let cases = [2, 0, 1, 1, 1, 2];

        let result = `${paramSearch[len % 100 > 4 && len % 100 < 20 ? 2 : cases[Math.min(len % 10, 5)]]}`;

        if (retur) return result;

        input.text(`${len} ${result}`);
    }

    $('.helper--ga input').on('change', function (e) {
        $(this).val(e.target.value.replace(/,/, '.'));
    });

    if ($('*').hasClass('search')) {
        searchFilterScroll();
    }

    function searchFilterScroll() {
        const form = $('.search__form');

        $(window).on('scroll', function () {
            if ($(this).scrollTop() > 1 && !form.hasClass('scroll')) {
                form.addClass('scroll');
            } else if ($(this).scrollTop() <= 1 && form.hasClass('scroll')) {
                form.removeClass('scroll');
            }
        });
    }

    // Favorite
    $('body').on('click', '.js-favorite', function () {
        const star = $(this);
        const id = star.attr('data-advert-id');
        const parent = $(this).closest('.ads__cardContent');

        if (!$('meta').is('[name="customer"]')) {
            $('.favorites-login-second').addClass('js-popup-show');
            return;
        }

        if (!star.hasClass('active')) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                },
                url: '/customer/create-favorite',
                data: {id},
                type: 'POST',
                success: function () {
                    star.addClass('active');
                },
            });
        } else {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                },
                url: '/customer/delete-favorite',
                data: {id},
                type: 'POST',
                success: function () {
                    if ($('section').hasClass('favorites')) {
                        parent.addClass('js-remove');
                        setTimeout(() => parent.remove(), 600);
                    }
                    star.removeClass('active');
                },
            });
        }
    });

    // personal card show contacts
    $('body').on('click', '.js-show-contacts', function () {
        const parent = $(this).closest('.js-contacts');

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '/getShortData',
            type: 'POST',
            data: {id: $(this).closest('.personCard').find('.customer_id').val()},
            success: data => {
                $(this).closest('.personCard').find('.short_email').text(data.email);
                $(this).closest('.personCard').find('.short_phone').text(data.phone);
                $(this)
                    .closest('.personCard')
                    .find('.short_email_attr')
                    .attr('href', 'mailto:' + data.email);
                $(this)
                    .closest('.personCard')
                    .find('.short_phone_attr')
                    .attr('href', 'tel:' + data.phone);
                parent.addClass('active');
            },
        });
    });

    $('.support__addFile').on('change', 'input[type="file"]', function ({target}) {
        const error = $('.error-custom');
        const info = target.files[0];
        let size = info.size;
        let sizeMax = 10000000;
        let input = $('<input name="file" type="file">');
        let quantityFiles = $('.support__file').length == 1 ? false : true;

        if (size <= sizeMax && target.files && info && quantityFiles) {
            error.removeClass('error-file');

            $(createFileCard(info)).prependTo('.support__addingFiles');
            $(this).appendTo($('.support__file').eq(0));
            input.prependTo('.support__addFile');
        } else {
            error.addClass('error-file');

            $(this).remove();
            input.prependTo('.support__addFile');
        }
    });

    $('.support__addingFiles').on('click', '.support__fileDel', function () {
        $(this).closest('.support__file').remove();
    });

    function createFileCard({name, size}) {
        let sizeFile = size < 1000000 ? `${(size / 1024).toFixed(1)} KB` : `${(size / 1024 / 1024).toFixed(1)} MB`;
        let nameFile = name.length > 20 ? name.substr(0, 20) : name;

        return `<div class="support__file">
                <div class="support__fileIcon">
                    <img src="/public/frontend/img/svg/uploadFile.svg" alt="">
                </div>
                <div class="support__fileText">
                    <div class="text text--min">
                        <p>${nameFile}</p>
                    </div>
                    <div class="text text--grayColor text--mini">
                        <p>${sizeFile}</p>
                    </div>
                </div>
                <div class="support__fileDel">
                    <img src="/public/frontend/img/svg/Close-dark.svg" alt="">
                </div>
            </div>`;
    }

    // Promote rules
    function promote() {
        let valUpAdvert = 1;
        let valTop = 7;

        let data = {
            allotment: false,
            advert_id: $('#advert-id').val(),
            upAdvert: {
                isActive: false,
                period: valUpAdvert,
            },
            top: {
                isActive: false,
                period: valTop,
            },
        };

        function calc(id, days) {
            if (id === 'allotment') {
                data.allotment = !data.allotment;
            }

            if (id === 'upAdvert') {
                data.upAdvert.isActive = !data.upAdvert.isActive;
            }

            if (id === 'top') {
                data.top.isActive = !data.top.isActive;
            }

            if (id === 'upAdvert-select') {
                data.upAdvert.period = days;
                valUpAdvert = days;
            }

            if (id === 'top-select') {
                data.top.period = days;
                valTop = days;
            }

            if (data.allotment || data.upAdvert.isActive || data.top.isActive) {
                $('#promote-form .btn').removeAttr('disabled');
            } else {
                $('#promote-form .btn').attr('disabled', 'true');
            }
        }

        $('.promote__navItem input[type="checkbox"]').on('change', function () {
            const isChecked = $(this).closest('.checkbox').hasClass('checked');
            const selectWrap = $(this).closest('li').find('.input-group');
            const parent = $(this).closest('.promote__navItem');

            if (isChecked) {
                selectWrap.removeClass('js-disabled');
                parent.addClass('active');
            } else {
                parent.removeClass('active');
                selectWrap.addClass('js-disabled');
            }

            calc($(this).attr('id'));

            promoteSum();
        });

        function promoteSum() {
            let sum = 0;

            $('.promote__navItem.active').map(function () {
                sum += parseInt($(this).find('.js-single-price').text());
            });

            $('.js-all-sum').text(sum);
        }

        $('.promote__navServeces select').on('change', function () {
            const value = parseInt($(this).find(':selected').attr('data-value'));
            const days = parseInt($(this).find(':selected').attr('data-days'));

            $(this).closest('li').find('.js-single-price').text(value);

            calc($(this).attr('id'), days);
            promoteSum();
        });

        $('#promote-form').on('submit', function (e) {
            e.preventDefault();

            if (!data.upAdvert.isActive) {
                data.upAdvert.period = null;
            } else {
                data.upAdvert.period = valUpAdvert;
            }

            if (!data.top.isActive) {
                data.top.period = null;
            } else {
                data.top.period = valTop;
            }

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                },
                url: '/customer/custom-type',
                type: 'POST',
                data: data,
                success: function (resp) {
                    window.location = resp;
                },
            });
        });
    }

    promote();

    // Price num
    $('#price, #minPriceGa, #maxPriceGa, #soldPrice').each(function () {
        const val = $(this).val();

        $(this).val(val.replace(/ /g, '').replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
    });

    $('#price, #minPriceGa, #maxPriceGa, #soldPrice').on('change keyup input click', function ({target}) {
        if (target.value.match(/[^0-9\s]/g)) {
            $(this).val(target.value.replace(/[^0-9\s]/g, ''));
        }

        $(this).val(target.value.replace(/ /g, '').replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
    });

    // Check cookie name
    function getCookie(name) {
        const matches = document.cookie.match(
            new RegExp('(?:^|; )' + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)')
        );

        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    // advert action
    function advertAction() {
        const advert = $('.advert-action');
        const data = getCookie('advertAction');

        if (data) {
            return advert.addClass('js-mini');
        }

        setTimeout(() => advert.addClass('js-mini'), 5000);
        document.cookie = 'advertAction=true';
    }

    advertAction();

    // Animation for advert action
    const advertActionScroll = () => {
        let lastScrollTop = 0;
        const advert = $('.advert-action');

        setTimeout(() => advert.addClass('active'), 400);

        $(window).scroll(function () {
            let nowScrollTop = $(this).scrollTop();

            if (nowScrollTop > lastScrollTop) {
                advert.removeClass('active');
            } else {
                advert.addClass('active');
            }

            if (nowScrollTop == $(document).height() - $(window).height()) {
                advert.removeClass('active');
            }

            lastScrollTop = nowScrollTop;
        });
    };

    advertActionScroll();

    // Show Adverts
    function showAdverts(el, mq) {
        const btn = el.closest('.ads').find('.btn--show-adverts');
        let sliceEl;

        mq.addListener(widthChange);
        widthChange(mq);

        function widthChange(mq) {
            if (mq.matches && el.length > 2) {
                btn.addClass('active');
                sliceEl = el.slice(-el.length - -2);
                sliceEl.hide();
                btn.on('click', function (e) {
                    e.preventDefault();
                    sliceEl.show();
                    $(this).removeClass('active');
                });
            } else {
                btn.removeClass('active');
                sliceEl = el;
                sliceEl.removeAttr('style');
            }
        }
    }

    if (!!$('#top-adverts-sale').length)
        showAdverts($('#top-adverts-sale').find('.ads__cardContent'), window.matchMedia('(max-width: 695px)'));
    if (!!$('#top-adverts-rent').length)
        showAdverts($('#top-adverts-rent').find('.ads__cardContent'), window.matchMedia('(max-width: 695px)'));

    // Add class for pagination dots
    function addClassPagination(el) {
        const dots = el.find('.page-link:contains(...)');
        dots.parent().addClass('dots');
    }

    function splicePagination(el) {
        const dots = el.find('.page-link:contains(...)');
        if (!dots.length) return;

        const items = el.find('.page-item');
        const activeItem = el.find('.page-item.active');
        const links = el.find('a.page-link');
        const sortedItems = links
            .filter(function () {
                return /\d/.test($(this).text());
            })
            .parent();
        let dotStart = items.eq(3).hasClass('dots');
        let dotEnd = items.eq(9).hasClass('dots');

        if (dots.length > 1) {
            $(sortedItems.splice(1, 3)).remove();
            $(sortedItems.splice(-4, 3)).remove();
        } else if (dots.length === 1 && dotEnd) {
            $(sortedItems.splice(-4, 3)).remove();
        } else if (dots.length === 1 && dotStart) {
            $(sortedItems.splice(1, 3)).remove();
        }
    }

    if (!!$('.pagination').length) {
        addClassPagination($('.pagination'));
        splicePagination($('.pagination'));
    }

    // On advert
    function onAdvert() {
        $('body').on('click', '.btn--activate[data-id]', function (e) {
            e.preventDefault();
            let $this = $(this);
            let data = {id: $this.attr('data-id')};
            $this.addClass('btn--loading');

            return new Promise(resolve => {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    },
                    url: '/customer/activate',
                    type: 'POST',
                    processData: false,
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify(data),
                    beforeSend() {
                        $('.loader').addClass('active');
                    },
                    success(data) {
                        return resolve(data);
                    },
                });
            }).then(fulfilled => {
                if (fulfilled) {
                    $this
                        .text('Відключити')
                        .attr({
                            'data-type': $('#ads-main-info').length
                                ? $this.closest('.advert__top-wrap').find('.advert__main-type').attr('data-type')
                                : $this.closest('.ads__cardContent').find('.ads__type p').text(),
                            'data-popupShow': 'close-advert',
                        })
                        .addClass('btn--deactivate js-popup-button')
                        .removeClass('btn--loading btn--activate')
                        .closest('.ads__top-section')
                        .find('.text--red-second.ads__status')
                        .removeClass('text--red-second')
                        .addClass('active-state')
                        .find('p')
                        .text('АКТИВНО');
                    $('.loader').removeClass('active');
                }
            });
        });
    }
    if (!!$('.btn--active[data-id]')) onAdvert();

    // Off advert
    function offAdvert() {
        const elNoSoldBtn = $('#close-advert-btn');
        const elSoldBtn = $('#close-advert-btn-ok');
        const soldPrice = $('#soldPrice');

        elNoSoldBtn.on('click', function (e) {
            e.preventDefault();
            let $this = $(this);
            let data = getData($this);

            $this.addClass('btn--loading');
            requestOffAdvert($this, '/customer/deactivate', JSON.stringify(data));
        });

        elSoldBtn.on('click', function (e) {
            e.preventDefault();
            if (!soldPrice.val()) {
                soldPrice.addClass('error');
                return false;
            }
            soldPrice.removeClass('error');
            let $this = $(this);
            let data = getData($this);

            $this.addClass('btn--loading');
            requestOffAdvert($this, '/customer/deactivate-and-selling', JSON.stringify(data));
        });

        function getData(btn) {
            let id = btn.closest('.close-advert').attr('data-id');

            if (btn.is('[id="close-advert-btn-ok"]')) {
                let price = $('#soldPrice').val().replace(/\s/g, '');

                return {
                    id,
                    price,
                };
            }

            return {
                id,
            };
        }
    }
    if (!!$('.btn--deactivate[data-id]')) offAdvert();

    function requestOffAdvert(el, url, data) {
        return new Promise(resolve => {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                },
                url: url,
                type: 'POST',
                processData: false,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                data: data,
                success(data) {
                    return resolve(data);
                },
            });
        }).then(fulfilled => {
            if (fulfilled) {
                let popupCloseAdvertEnd = $('.close-advert-end');
                let popupCloseAdvertEndTitle = popupCloseAdvertEnd.find('.title p');

                window.btnDeactivate
                    .text('Активувати')
                    .removeAttr('data-type data-popupShow')
                    .removeClass('btn--loading btn--deactivate js-popup-button')
                    .addClass('btn--activate')
                    .closest('.ads__top-section')
                    .find('.active-state.ads__status')
                    .removeClass('active-state')
                    .addClass('text--red-second')
                    .find('p')
                    .text('ВІДКЛЮЧЕНО');
                $('.close-advert').removeClass('js-popup-show');
                el.removeClass('btn--loading');
                el.is('[id="close-advert-btn-ok"]')
                    ? popupCloseAdvertEndTitle.text('Оголошення успішно відключено і відкріплено від Вашого профілю!')
                    : popupCloseAdvertEndTitle.text('Оголошення успішно відключено!');
                popupCloseAdvertEnd.addClass('js-popup-show');
                $('#soldPrice').val('');

                setTimeout(() => {
                    popupCloseAdvertEnd.removeClass('js-popup-show');
                    $('body').removeClass('no-scrolling');
                }, 2000);
            }
        });
    }

    // Trigger to search sale
    function triggerSearchSale() {
        $('.top-adverts__link--sale').on('click', function (e) {
            e.preventDefault();
            $('#sale').prop('checked', true);
            $('.callboard__form').trigger('submit');
        });
    }
    if (!!$('.top-adverts__link--sale').length) triggerSearchSale();

    // MediaQuery for advert
    function mqAdvert(el, toEl, fromEl, mq) {
        mq.addListener(widthChange);
        widthChange(mq);

        function widthChange(mq) {
            if (mq.matches) {
                toEl.append(el);
            } else {
                fromEl.append(el);
            }
        }
    }

    if (!!$('#ads-wrap-last').length) {
        mqAdvert(
            $('#ads-main-info'),
            $('#ads-wrap-first'),
            $('#ads-wrap-last'),
            window.matchMedia('(max-width: 1199px)')
        );
        mqAdvert(
            $('#ads-other-info'),
            $('#ads-wrap-first').find('.advert__headInfo'),
            $('#ads-wrap-last'),
            window.matchMedia('(max-width: 1199px)')
        );
        mqAdvert(
            $('#date-info'),
            $('#ads-wrap-first').find('.advert__main-type'),
            $('#ads-other-info'),
            window.matchMedia('(min-width: 599px) and (max-width: 991px)')
        );
    }
});
