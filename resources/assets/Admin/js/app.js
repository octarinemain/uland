require("./bootstrap");

window.Vue = require("vue");
import VueResource from "vue-resource";
import vSelect from "vue-select";
Vue.component("v-select", vSelect);
Vue.use(VueResource);
Vue.use(require("vue-moment"));
Vue.component("login", require("./components/Login.vue"));
Vue.component("admin-menu", require("./components/layouts/Menu.vue"));
Vue.component("advert-list", require("./components/AdvertList.vue"));
Vue.component("advert", require("./components/Advert.vue"));
Vue.component("advert-advertising", require("./components/AdvertAdvertising.vue"));
Vue.component("customer", require("./components/Customer.vue"));
Vue.component("article-blog", require("./components/Article.vue"));
Vue.component("page", require("./components/Page.vue"));
Vue.component("support", require("./components/Support.vue"));
Vue.component("payment", require("./components/Payment.vue"));

Vue.http.headers.common["X-CSRF-TOKEN"] = $("meta[name=token]").attr("value");

const app = new Vue({
    el: "#app"
});
