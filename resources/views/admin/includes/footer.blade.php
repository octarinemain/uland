<!--end::Page Scripts -->
</div>
<script type="application/javascript" src="{{ asset('public/admin/assets/vendors/base/vendors.bundle.js') }}"></script>
<script type="application/javascript" src="{{ asset('public/admin/assets/demo/default/base/scripts.bundle.js') }}"></script>
<script type="application/javascript" src="{{ asset('public/admin/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js') }}"></script>
<script type="application/javascript" src="{{ asset('public/admin/assets/app/js/jquery.maskedinput.min.js') }}"></script>
<script type="application/javascript" src="{{ asset('public/admin/assets/demo/default/custom/crud/forms/widgets/summernote.js') }}"></script>

{{--<script src="{{ asset('public/admin/js/tinymce/tinymce.min.js') }}"></script>--}}
{{--<script>--}}
    {{--function handleFileSelect(e) {--}}
        {{--if (!e.target.files) return;--}}

        {{--var files = e.target.files;--}}
        {{--for (var i = 0; i < files.length; i++) {--}}
            {{--var f = files[i];--}}

            {{--selDiv.innerHTML += f.name + "<br/>";--}}
        {{--}--}}
    {{--}--}}
    {{--var editor_config = {--}}
        {{--selector: "#tinymce_textarea",--}}
        {{--required: true,--}}
        {{--plugins: [--}}
            {{--"advlist autolink lists link  charmap  preview hr anchor pagebreak",--}}
            {{--"searchreplace wordcount visualblocks visualchars code fullscreen",--}}
            {{--"insertdatetime  nonbreaking save table contextmenu directionality",--}}
            {{--"emoticons template paste textcolor colorpicker textpattern youtube",--}}
            {{--"table",--}}
        {{--],--}}
        {{--menu: [],--}}
        {{--toolbar: 'undo redo | alignleft aligncenter alignright alignjustify | formatselect fontsizeselect | bullist numlist | outdent indent | table | mybutton | youtube',--}}
        {{--setup: function(editor) {--}}
            {{--editor.addButton('mybutton', {--}}
                {{--text:"IMG",--}}
                {{--icon: false,--}}
                {{--onclick: function(e) {--}}
                    {{--console.log($(e.target));--}}
                    {{--if($(e.target).prop("tagName") == 'BUTTON'){--}}
                        {{--console.log($(e.target).parent().parent().find('input').attr('id'));--}}
                        {{--if($(e.target).parent().parent().find('input').attr('id') != 'tinymce-uploader') {--}}
                            {{--$(e.target).parent().parent().append('<input id="tinymce-uploader" type="file" name="pic" accept="image/*" style="display:none">');--}}
                        {{--}--}}
                        {{--$('#tinymce-uploader').trigger('click');--}}
                        {{--$('#tinymce-uploader').change(function(){--}}
                            {{--var input, file, fr, img;--}}

                            {{--if (typeof window.FileReader !== 'function') {--}}
                                {{--write("The file API isn't supported on this browser yet.");--}}
                                {{--return;--}}
                            {{--}--}}

                            {{--input = document.getElementById('tinymce-uploader');--}}
                            {{--if (!input) {--}}
                                {{--write("Um, couldn't find the imgfile element.");--}}
                            {{--} else if (!input.files) {--}}
                                {{--write("This browser doesn't seem to support the `files` property of file inputs.");--}}
                            {{--} else if (!input.files[0]) {--}}
                                {{--write("Please select a file before clicking 'Load'");--}}
                            {{--} else {--}}
                                {{--file = input.files[0];--}}
                                {{--fr = new FileReader();--}}
                                {{--fr.onload = createImage;--}}
                                {{--fr.readAsDataURL(file);--}}
                            {{--}--}}

                            {{--function createImage() {--}}
                                {{--img = new Image();--}}
                                {{--img.src = fr.result;--}}
                                {{--editor.insertContent('<img src="'+img.src+'"/>');--}}
                            {{--}--}}
                        {{--});--}}

                    {{--}--}}

                    {{--if($(e.target).prop("tagName") == 'DIV'){--}}
                        {{--if($(e.target).parent().find('input').attr('id') != 'tinymce-uploader') {--}}
                            {{--console.log($(e.target).parent().find('input').attr('id'));--}}
                            {{--$(e.target).parent().append('<input id="tinymce-uploader" type="file" name="pic" accept="image/*" style="display:none">');--}}
                        {{--}--}}
                        {{--$('#tinymce-uploader').trigger('click');--}}
                        {{--$('#tinymce-uploader').change(function(){--}}
                            {{--var input, file, fr, img;--}}

                            {{--if (typeof window.FileReader !== 'function') {--}}
                                {{--write("The file API isn't supported on this browser yet.");--}}
                                {{--return;--}}
                            {{--}--}}

                            {{--input = document.getElementById('tinymce-uploader');--}}
                            {{--if (!input) {--}}
                                {{--write("Um, couldn't find the imgfile element.");--}}
                            {{--} else if (!input.files) {--}}
                                {{--write("This browser doesn't seem to support the `files` property of file inputs.");--}}
                            {{--} else if (!input.files[0]) {--}}
                                {{--write("Please select a file before clicking 'Load'");--}}
                            {{--} else {--}}
                                {{--file = input.files[0];--}}
                                {{--fr = new FileReader();--}}
                                {{--fr.onload = createImage;--}}
                                {{--fr.readAsDataURL(file);--}}
                            {{--}--}}

                            {{--function createImage() {--}}
                                {{--img = new Image();--}}
                                {{--img.src = fr.result;--}}
                                {{--editor.insertContent('<img src="'+img.src+'"/>');--}}
                            {{--}--}}
                        {{--});--}}
                    {{--}--}}

                    {{--if($(e.target).prop("tagName") == 'I'){--}}
                        {{--console.log($(e.target).parent().parent().parent().find('input').attr('id')); if($(e.target).parent().parent().parent().find('input').attr('id') != 'tinymce-uploader') {               $(e.target).parent().parent().parent().append('<input id="tinymce-uploader" type="file" name="pic" accept="image/*" style="display:none">');--}}
                        {{--}--}}
                        {{--$('#tinymce-uploader').trigger('click');--}}
                        {{--$('#tinymce-uploader').change(function(){--}}
                            {{--var input, file, fr, img;--}}

                            {{--if (typeof window.FileReader !== 'function') {--}}
                                {{--write("The file API isn't supported on this browser yet.");--}}
                                {{--return;--}}
                            {{--}--}}

                            {{--input = document.getElementById('tinymce-uploader');--}}
                            {{--if (!input) {--}}
                                {{--write("Um, couldn't find the imgfile element.");--}}
                            {{--} else if (!input.files) {--}}
                                {{--write("This browser doesn't seem to support the `files` property of file inputs.");--}}
                            {{--} else if (!input.files[0]) {--}}
                                {{--write("Please select a file before clicking 'Load'");--}}
                            {{--} else {--}}
                                {{--file = input.files[0];--}}
                                {{--fr = new FileReader();--}}
                                {{--fr.onload = createImage;--}}
                                {{--fr.readAsDataURL(file);--}}
                            {{--}--}}

                            {{--function createImage() {--}}
                                {{--img = new Image();--}}
                                {{--img.src = fr.result;--}}
                                {{--editor.insertContent('<img src="'+img.src+'"/>');--}}
                            {{--}--}}
                        {{--});--}}
                    {{--}--}}

                {{--}--}}
            {{--});--}}
        {{--},--}}

    {{--};--}}

    {{--tinymce.init(editor_config);--}}
{{--</script>--}}

<script type="application/javascript" src="{{ asset('public/admin/js/app.js') }}"></script>
{{--<script>--}}
    {{--$("input[name='phone']").mask("+7 (999) 999-99-99");--}}
{{--</script>--}}

<!-- end::Body -->
</html>