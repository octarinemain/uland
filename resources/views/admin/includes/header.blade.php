<!DOCTYPE html>

<html lang="en" >

<!-- begin::Head -->
<!-- end::Head -->
<!-- begin::Body -->
<head>
    <meta charset="utf-8" />
    <title>Uland - панель управления</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="{{ asset('public/frontend/img/logoblack.ico') }}" type="image/x-icon">

    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <!--end::Web font -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--begin::Global Theme Styles -->
    <link href="{{ asset('public/admin/assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/admin/assets/demo/default/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/admin/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/admin/add.css') }}" rel="stylesheet" type="text/css" />
</head>
<div id="app" class="h-100">
     @yield('content')
@extends('admin.includes.footer')