@extends('frontend.includes.header')

@section('content')
    <section class="registration section-dark">
        <div class="container">
            <div class="registration__form">
                <div class="title title--sub title--center">
                    <p>Вхід</p>
                </div>
                <form method="POST" action="{{ route('customer.login.submit') }}" class="customer-login">
                    {{ csrf_field() }}
                    <div class="input-group input-group--mt input-group--mask">
                        <p class="mask-phone">+380</p>
                        <label for="phone">Номер телефону</label>
                        <input type="number" id="phone" name="phone" value="{{ old('phone') }}" required>
                        <div class="ajax-validate-error"></div>
                    </div>
                    <div class="input-group input-group--mtBig">
                        <label for="password">Пароль</label>
                        <input type="password" id="password" name="password" required>
                        <a class="input-group__link" href="/reset-step-1">Забули пароль?</a>
                    </div>
                    {!! NoCaptcha::renderJs() !!}
                    {!! NoCaptcha::display() !!}
                    <div class="form-send form-send--mt">
                        <p class="form-send__text">Немає облікового запису? <a href="/register-step-1">Зареєструватися</a></p>
                        <button type="submit" class="btn">УВІЙТИ</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <script>
        $('.customer-login').on('submit', function (e) {
            e.preventDefault();
            $('.ajax-validate-error').html('');

            if ($(this).valid()) {
                var formData = new FormData($(this)[0]);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/login',
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    data: formData,
                    beforeSend: function() {
                        $('.loader').addClass('active');
                    },
                    success: function (data) {
                        $('.loader').removeClass('active');
                        window.location = data;
                    },
                    error: function (data) {
                        grecaptcha.reset();
                        $('.loader').removeClass('active');
                        var errors = data.responseJSON;
                        $('.customer-login .ajax-validate-error').html('');
                        $.each(errors.errors, function (index, value) {
                            $('.customer-login .ajax-validate-error').append('<span><div class="error">' + value + '</div></span>');
                        });
                    }
                });
            }
        });

    </script>
@endsection
