@extends('frontend.includes.header')

@section('content')
    <section class="registration section-dark">
        <div class="container">
            <div class="registration__form">
                <div class="title title--sub title--center">
                    <p>Відновлення пароля</p>
                </div>
                <form class="reset-step-1">
                    {{ csrf_field() }}
                    <div class="input-group input-group--mt input-group--mask">
                        <p class="mask-phone">+380</p>
                        <label for="phone">Введіть номер телефону</label>
                        <input type="number" id="phone" name="phone" required>
                        <div class="ajax-validate-error"></div>
                    </div>
                    {!! NoCaptcha::renderJs() !!}
                    {!! NoCaptcha::display() !!}
                    <div class="form-send form-send--mt  justify-end">
                        <button type="submit" class="btn">ВИСЛАТИ КОД</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <script>
        $('.reset-step-1').on('submit',function(e) {
            e.preventDefault();
            $('.ajax-validate-error').html('');

            if($(this).valid()){
                var formData = new FormData($(this)[0]);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/reset-step-1',
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    data: formData,
                    beforeSend: function() {
                        $('.loader').addClass('active');
                    },
                    success: function (data) {
                        $('.loader').removeClass('active');
                        window.location = data;
                    },
                    error: function (data) {
                        $('.loader').removeClass('active');
                        grecaptcha.reset();
                        var errors = data.responseJSON;
                        $('.reset-step-1 .ajax-validate-error').html('');
                        $.each(errors.errors, function (index, value) {
                            $('.reset-step-1 .ajax-validate-error').append('<span><div class="error">' + value + '</div></span>');
                        });
                    }
                });
            }
        });

    </script>
@endsection
