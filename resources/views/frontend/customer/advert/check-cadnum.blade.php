@extends('frontend.includes.header')

@section('content')
    <section class="create-advert section-dark">
        <div class="container">
            <div class="create-advert__wrap create-advert__wrap--min">
                <div class="title title--sub title--center">
                    <p>Публікація оголошення</p>
                </div>
                <div class="text text--center text--mtMedium">
                    <p>Введіть кадастровий номер ділянки для пошуку в реєстрі</p>
                </div>
                <form method="POST" class="create-advert__form create-advert__form--column check-cadnum">
                    {{ csrf_field() }}
                    <div class="input-group">
                        <label for="cadnum">Кадастровий номер</label>
                        <input type="text" id="cadnum" name="cadnum" value="@if(Session::get('cadnum')) {{ Session::get('cadnum')[0] }} @endif" required>
                        <div class="ajax-validate-error"></div>
                    </div>
                    <div class="form-send form-send--mt justify-end">
                        <button type="submit" class="btn">ШУКАТИ</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <script>
        $('.check-cadnum').on('submit', function (e) {
            e.preventDefault();
            $('.ajax-validate-error').html('');

            if ($(this).valid()) {
                var formData = new FormData($(this)[0]);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/customer/check-cadnum',
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    data: formData,
                    beforeSend: function() {
                        $('.loader').addClass('active');
                    },
                    success: function (data) {
                        $('.loader').removeClass('active');
                        window.location = data;
                    },
                    error: function (data) {
                        $('.loader').removeClass('active');
                        var errors = data.responseJSON;
                        $('.ajax-validate-error').html('');
                        $.each(errors.errors, function (index, value) {
                            $('.check-cadnum .ajax-validate-error').append('<span><div class="error">' + value + '</div></span>');
                        });
                    }
                });
            }
        });

    </script>

@endsection
