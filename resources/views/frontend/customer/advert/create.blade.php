@extends('frontend.includes.header')

@section('content')
    <input hidden id="c-y" value="{{ $cadnum->lat }}" >
    <input hidden id="c-x" value="{{ $cadnum->lon }}" >
    <script>
		jQuery(document).ready(function () {
			formData = new FormData();
			formData.append('x', $('#c-x').val());
			formData.append('y', $('#c-y').val());
				$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: '/customer/getCoordinateInfo',
				type: 'POST',
				contentType: false,
				processData: false,
				data: formData,
				success: function (data) {
					window.dataCoordinate = data;					
				},
			});
		});

    </script>
    <section id="publish-advert" class="create-advert create-advert--min section-dark">
        <div class="container">
            <div class="create-advert__wrap">
                <div class="title title--sub title--center">
                    <p>Публікація оголошення</p>
                </div>
                <div class="text text--center text--mtMedium">
                    <p>Кадастровий номер</p>
                </div>
                <div class="setNumber">
                    <div class="setNumber__number">
                        <p>{{ $cadnum->cadnum }}</p>
                    </div>
                    <a href="/customer/check-cadnum">Змінити номер</a>
                </div>
                <form method="POST" class="create-advert__form">
                    {{ csrf_field() }}
                    <div class="create-advert__formData">
                        <div class="radio-wrap flex-cont align-center js-change-radio">
                            <label class="radio checked">
                                <input type="radio" value="Оренда" name="type" id="rent" checked>
                                <div class="radio__text">Оренда</div>
                            </label>
                            <label class="radio">
                                <input type="radio" value="Продаж" id="sale" name="type">
                                <div class="radio__text">Продаж</div>
                            </label>
                        </div>
                        <div class="input-group input-group--mt val">
                            <label for="ga">Яка площа здається, Га</label>
                            <input type="number" id="ga" name="ga" value="{{ $cadnum->area }}" required>
                        </div>
                        <div class="helper helper--mt helper--flex">
                            <div class="input-group input-group--100">
                                <label for="price">Ціна за продавану площу</label>
                                <input type="text" pattern="^[ 0-9]+$" id="price" name="price" required>
                            </div>
                            <div class="input-group input-group--select input-group--price">
                                {{--<select name="currency">--}}
                                {{--<option @if($advert->currency == '₴') selected @endif value="₴">грн</option>--}}
                                {{--<option @if($advert->currency == '$') selected @endif value="$">дол</option>--}}
                                {{--<option @if($advert->currency == '€') selected @endif value="€">евро</option>--}}
                                {{--</select>--}}
                                <input type="text" readonly  name="currency" value="грн." required>
                            </div>
                        </div>
                        <label class="checkbox checkbox--mtMin checkbox--big">
                            <input name="discount" type="checkbox"  value="1"/>
                            <div class="checkbox__text">Можливий торг</div>
                        </label>
                        <div class="mini-title mini-title--mt">
                            <p>Додаткова інформація</p>
                        </div>
                        <label class="checkbox checkbox--mtMini checkbox--big js-checkbox-slide">
                            <input type="checkbox" value="1" id="electricity"/>
                            <div class="checkbox__text">Електрика</div>
                        </label>
                        <div class="input-group input-group--mini">
                            <textarea placeholder="Ви можете додати опис" name="attr_1" disabled="true"></textarea>
                        </div>
                        <label class="checkbox checkbox--mtMini checkbox--big js-checkbox-slide">
                            <input type="checkbox" value="1" id="gas"/>
                            <div class="checkbox__text">Газ</div>
                        </label>
                        <div class="input-group input-group--mini">
                            <textarea placeholder="Ви можете додати опис" name="attr_2" disabled="true"></textarea>
                        </div>
                        <label class="checkbox checkbox--mtMini checkbox--big js-checkbox-slide">
                            <input type="checkbox"  value="1" id="water"/>
                            <div class="checkbox__text">Вода, можливість поливу</div>
                        </label>
                        <div class="input-group input-group--mini">
                            <textarea placeholder="Ви можете додати опис" name="attr_3" disabled="true"></textarea>
                        </div>
                        <label class="checkbox checkbox--mtMini checkbox--big js-checkbox-slide">
                            <input type="checkbox"  value="1" id="equipment"/>
                            <div class="checkbox__text">Техніка, обладнання</div>
                        </label>
                        <div class="input-group input-group--mini">
                            <textarea placeholder="Ви можете додати опис" name="attr_4" disabled="true"></textarea>
                        </div>
                        <label class="checkbox checkbox--mtMini checkbox--big js-checkbox-slide">
                            <input type="checkbox"  value="1" id="building"/>
                            <div class="checkbox__text">Будівлі, споруди</div>
                        </label>
                        <div class="input-group input-group--mini">
                            <textarea placeholder="Ви можете додати опис" name="attr_5" disabled="true"></textarea>
                        </div>
                        <div class="input-group input-group--mtBig">
                            <textarea name="comment_1" placeholder="Як сьогодні використовується об'єкт?"></textarea>
                        </div>
                        <div class="input-group input-group--mtBig">
                            <textarea name="comment_2" placeholder="Інші активи"></textarea>
                        </div>
                        <div class="input-group input-group--mtBig">
                            <textarea name="comment_3" placeholder="Додаткова інформація"></textarea>
                        </div>
                        <div class="mini-title mini-title--mt">
                            <p>Завантажити фотографії</p>
                        </div>
                    </div>
                    <div class="create-advert__cadastData">
                        <div class="create-advert__dataCard">
							<div class="text text--gray">
								<p>Область</p>
							</div>
                            <div class="text text--black input-group">
                                <select id="region" name="region">
									<option>Не визначено</option>
									<option>Івано-Франківська область</option>
									<option>Кримський півострів</option>
									<option>Вінницька область</option>
									<option>Волинська область</option>
									<option>Дніпропетровська область</option>
									<option>Донецька область</option>
									<option>Житомирська область</option>
									<option>Закарпатська область</option>
									<option>Запорізька область</option>
									<option>Кіровоградська область</option>
									<option>Київська область</option>
									<option>Луганська область</option>
									<option>Львівська область</option>
									<option>Миколаївська область</option>
									<option>Одеська область</option>
									<option>Полтавська область</option>
									<option>Рівненська область</option>
									<option>Севастополь</option>
									<option>Сумська область</option>
									<option>Тернопільська область</option>
									<option>Харківська область</option>
									<option>Херсонська область</option>
									<option>Хмельницька область</option>
									<option>Черкаська область</option>
									<option>Чернівецька область</option>
									<option>Чернігівська область</option>
								</select>
                            </div>
                        </div>
                        <div class="create-advert__dataCard">
                            <div class="text text--gray">
                                <p>Район</p>
                            </div>
                            <div class="text text--black input-group">
                                <select id="district" name="district">
									<option>Не визначено</option>
								</select>
                            </div>
                        </div>
                        <div class="create-advert__dataCard">
                            <div class="text text--gray">
                                <p>Місто</p>
                            </div>
                            <div class="text text--black input-group">
								<select id="city" name="city">
									<option>Не визначено</option>
								</select>
                            </div>
                        </div>
                        <div class="create-advert__dataCard">
                            <div class="text text--gray">
                                <p>Тип власності</p>
                            </div>
                            <div class="text text--black">
                                <p>{{ $cadnum->right }}</p>
                            </div>
                        </div>
                        <div class="create-advert__dataCard">
                            <div class="text text--gray">
                                <p>КОАТТУ</p>
                            </div>
                            <div class="text text--black">
                                <p>{{ $cadnum->koatuu }}</p>
                            </div>
                        </div>
                        <div class="create-advert__dataCard flex-cont">
                            <div class="create-advert__dataCard">
                                <div class="text text--gray">
                                    <p>Зона</p>
                                </div>
                                <div class="text text--black">
                                    <p>{{ $cadnum->zone }}</p>
                                </div>
                            </div>
                            <div class="create-advert__dataCard">
                                <div class="text text--gray">
                                    <p>Квартал</p>
                                </div>
                                <div class="text text--black">
                                    <p>{{ $cadnum->quartal }}</p>
                                </div>
                            </div>
                            <div class="create-advert__dataCard">
                                <div class="text text--gray">
                                    <p>Площа</p>
                                </div>
                                <div class="text text--black">
                                    <p>{{ $cadnum->area }} Га</p>
                                </div>
                            </div>
                        </div>
                        <div class="create-advert__dataCard">
                            <div class="text text--gray">
                                <p>Цільове призначення</p>
                            </div>
                            <div class="text text--black">
                                <p>
                                    {{ $cadnum->purpose }}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="upload-photos">
                        <label class="upload-photos__card">
                            <div class="upload-photos__loader"></div>
                            <div class="upload-photos__exit">
                                <img src="{{ asset('public/frontend/img/svg/close.svg') }}" alt="">
                            </div>
                            <input type="file" name="image[]" accept="image/*,image/jpeg" class="upload-photos__file">
                            <img class="upload-photos__plus" src="{{ asset('public/frontend/img/svg/plus-black.svg') }}" alt="">
                            <span>
                                <div class="error-custom">Формат фото: png, jpg (10Mb)</div>
                            </span>
                        </label>
                        <label class="upload-photos__card">
                            <div class="upload-photos__loader"></div>
                            <div class="upload-photos__exit">
                                <img src="{{ asset('public/frontend/img/svg/close.svg') }}" alt="">
                            </div>
                            <input type="file" name="image[]" accept="image/*,image/jpeg" class="upload-photos__file">
                            <img class="upload-photos__plus" src="{{ asset('public/frontend/img/svg/plus-black.svg') }}" alt="">
                            <span>
                                <div class="error-custom">Формат фото: png, jpg (10Mb)</div>
                            </span>
                        </label>
                        <label class="upload-photos__card">
                            <div class="upload-photos__loader"></div>
                            <div class="upload-photos__exit">
                                <img src="{{ asset('public/frontend/img/svg/close.svg') }}" alt="">
                            </div>
                            <input type="file" name="image[]" accept="image/*,image/jpeg" class="upload-photos__file">
                            <img class="upload-photos__plus" src="{{ asset('public/frontend/img/svg/plus-black.svg') }}" alt="">
                            <span>
                                <div class="error-custom">Формат фото: png, jpg (10Mb)</div>
                            </span>
                        </label>
                        <label class="upload-photos__card">
                            <div class="upload-photos__loader"></div>
                            <div class="upload-photos__exit">
                                <img src="{{ asset('public/frontend/img/svg/close.svg') }}" alt="">
                            </div>
                            <input type="file" name="image[]" accept="image/*,image/jpeg" class="upload-photos__file">
                            <img class="upload-photos__plus" src="{{ asset('public/frontend/img/svg/plus-black.svg') }}" alt="">
                            <span>
                                <div class="error-custom">Формат фото: png, jpg (10Mb)</div>
                            </span>
                        </label>
                        <label class="upload-photos__card">
                            <div class="upload-photos__loader"></div>
                            <div class="upload-photos__exit">
                                <img src="{{ asset('public/frontend/img/svg/close.svg') }}" alt="">
                            </div>
                            <input type="file" name="image[]" accept="image/*,image/jpeg" class="upload-photos__file">
                            <img class="upload-photos__plus" src="{{ asset('public/frontend/img/svg/plus-black.svg') }}" alt="">
                            <span>
                                <div class="error-custom">Формат фото: png, jpg (10Mb)</div>
                            </span>
                        </label>
                        <label class="upload-photos__card">
                            <div class="upload-photos__loader"></div>
                            <div class="upload-photos__exit">
                                <img src="{{ asset('public/frontend/img/svg/close.svg') }}" alt="">
                            </div>
                            <input type="file" name="image[]" accept="image/*,image/jpeg" class="upload-photos__file">
                            <img class="upload-photos__plus" src="{{ asset('public/frontend/img/svg/plus-black.svg') }}" alt="">
                            <span>
                                <div class="error-custom">Формат фото: png, jpg (10Mb)</div>
                            </span>
                        </label>
                        <label class="upload-photos__card">
                            <div class="upload-photos__loader"></div>
                            <div class="upload-photos__exit">
                                <img src="{{ asset('public/frontend/img/svg/close.svg') }}" alt="">
                            </div>
                            <input type="file" name="image[]" accept="image/*,image/jpeg" class="upload-photos__file">
                            <img class="upload-photos__plus" src="{{ asset('public/frontend/img/svg/plus-black.svg') }}" alt="">
                            <span>
                                <div class="error-custom">Формат фото: png, jpg (10Mb)</div>
                            </span>
                        </label>
                        <label class="upload-photos__card">
                            <div class="upload-photos__loader"></div>
                            <div class="upload-photos__exit">
                                <img src="{{ asset('public/frontend/img/svg/close.svg') }}" alt="">
                            </div>
                            <input type="file" name="image[]" accept="image/*,image/jpeg" class="upload-photos__file">
                            <img class="upload-photos__plus" src="{{ asset('public/frontend/img/svg/plus-black.svg') }}" alt="">
                            <span>
                                <div class="error-custom">Формат фото: png, jpg (10Mb)</div>
                            </span>
                        </label>
                        <label class="upload-photos__card">
                            <div class="upload-photos__loader"></div>
                            <div class="upload-photos__exit">
                                <img src="{{ asset('public/frontend/img/svg/close.svg') }}" alt="">
                            </div>
                            <input type="file" name="image[]" accept="image/*,image/jpeg" class="upload-photos__file">
                            <img class="upload-photos__plus" src="{{ asset('public/frontend/img/svg/plus-black.svg') }}" alt="">
                            <span>
                                <div class="error-custom">Формат фото: png, jpg (10Mb)</div>
                            </span>
                        </label>
                        <label class="upload-photos__card">
                            <div class="upload-photos__loader"></div>
                            <div class="upload-photos__exit">
                                <img src="{{ asset('public/frontend/img/svg/close.svg') }}" alt="">
                            </div>
                            <input type="file" name="image[]" accept="image/*,image/jpeg" class="upload-photos__file">
                            <img class="upload-photos__plus" src="{{ asset('public/frontend/img/svg/plus-black.svg') }}" alt="">
                            <span>
                                <div class="error-custom">Формат фото: png, jpg (10Mb)</div>
                            </span>
                        </label>
                    </div>
                    <div class="form-send form-send--mtBig form-send--100 justify-end">
                        <div class="ajax-validate-error">
                        </div>
                        <button type="submit" class="btn">ОПУБЛІКУВАТИ</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <script>
        $('.create-advert__form').on('submit', function (e) {
            e.preventDefault();
            if ($(this).valid()) {
                $('.upload-photos__card:not(.active)').find('input').removeAttr('name');

                var formData = new FormData($(this)[0]);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/customer/create-advert',
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    data: formData,
                    beforeSend: function() {
                        $('.loader').addClass('active');
                    },
                    success: function (data) {
                        $('.loader').removeClass('active');
                        $('.upload-photos__card:not(.active)').find('input').attr('name', 'image[]');
                        window.location = data;
                    },
                    error: function (data) {
                        $('.loader').removeClass('active');
                        $('.upload-photos__card:not(.active)').find('input').attr('name', 'image[]');
                        var errors = data.responseJSON;
                        $('.ajax-validate-error').html('');
                        $.each(errors.errors, function (index, value) {
                            $('.ajax-validate-error').append('<span><div class="error">' + value + '</div></span>');
                        });
                    }
                });
            }
        });

    </script>

@endsection
