@extends('frontend.includes.header')

@section('content')
    <section class="create-advert create-advert--min section-dark">
        <div class="container">
            <div class="create-advert__wrap">
                <div class="title title--sub title--center">
                    <p>Редагування оголошення</p>
                </div>
                <div class="text text--center text--mtMedium">
                    <p>Кадастровий номер</p>
                </div>
                <div class="setNumber">
                    <div class="setNumber__number">
                        <p>{{ $advert->cadnum }}</p>
                    </div>
                </div>
                <form method="POST" class="create-advert__form">
                    {{ csrf_field() }}
                    <input name="id" hidden value="{{ $advert->id }}">

                    <div class="create-advert__formData">
                        <div class="radio-wrap flex-cont align-center js-change-radio">
                            <label class="radio  @if($advert->type == 'Оренда') checked @endif">
                                <input type="radio" @if($advert->type == 'Оренда') checked @endif value="Оренда"
                                       name="type" id="rent">
                                <div class="radio__text">Оренда</div>
                            </label>
                            <label class="radio @if($advert->type == 'Продаж') checked @endif">
                                <input type="radio" @if($advert->type == 'Продаж') checked @endif value="Продаж"
                                       name="type" id="sale">
                                <div class="radio__text">Продаж</div>
                            </label>
                        </div>

                        <div class="input-group input-group--mt val">
                            <label for="ga">Яка площа здається, Га</label>
                            <input type="number" id="ga" name="ga" value="{{ $advert->ga_to_sell }}" required>
                        </div>
                        <div class="helper helper--mt helper--flex">
                            <div class="input-group input-group--100 val">
                                <label for="price">Ціна за ділянку Га</label>
                                <input type="text" pattern="^[ 0-9]+$" id="price" value="{{ $advert->price }}" name="price" required>
                            </div>
                            <div class="input-group input-group--select input-group--price val">
                                {{--<select name="currency">--}}
                                {{--<option @if($advert->currency == '₴') selected @endif value="₴">грн</option>--}}
                                {{--<option @if($advert->currency == '$') selected @endif value="$">дол</option>--}}
                                {{--<option @if($advert->currency == '€') selected @endif value="€">евро</option>--}}
                                {{--</select>--}}
                                <input type="text" readonly name="currency" value="грн." required>
                            </div>
                        </div>
                        <label
                            class="checkbox checkbox--mtMin checkbox--big @if($advert->discount == 1) checked @endif">
                            <input name="discount" type="checkbox" @if($advert->discount == 1) checked
                                   @endif  value="1"/>
                            <div class="checkbox__text">Можливий торг</div>
                        </label>
                        <div class="mini-title mini-title--mt">
                            <p>Додаткова інформація</p>
                        </div>
                        <label
                            class="checkbox checkbox--mtMini checkbox--big js-checkbox-slide @if($advert->attr_1 != '') checked @endif ">
                            <input type="checkbox" @if($advert->attr_1 != '') checked @endif  value="1"
                                   id="electricity"/>
                            <div class="checkbox__text">Електрика</div>
                        </label>
                        <div class="input-group input-group--mini  @if($advert->attr_1 != '') active-textarea @endif">
                            <textarea placeholder="Ви можете додати опис" name="attr_1"
                                      @if($advert->attr_1 == '') disabled="true" @endif >{{ $advert->attr_1 }}</textarea>
                        </div>
                        <label
                            class="checkbox checkbox--mtMini checkbox--big js-checkbox-slide @if($advert->attr_2 != '') checked @endif ">
                            <input type="checkbox" @if($advert->attr_2 != '') checked @endif value="1" id="gas"/>
                            <div class="checkbox__text">Газ</div>
                        </label>
                        <div class="input-group input-group--mini  @if($advert->attr_2 != '') active-textarea @endif">
                            <textarea placeholder="Ви можете додати опис" name="attr_2"
                                      @if($advert->attr_2 == '') disabled="true" @endif >{{ $advert->attr_2 }}</textarea>
                        </div>
                        <label
                            class="checkbox checkbox--mtMini checkbox--big js-checkbox-slide @if($advert->attr_3 != '') checked @endif ">
                            <input type="checkbox" @if($advert->attr_3 != '') checked @endif value="1" id="water"/>
                            <div class="checkbox__text">Вода, можливість поливу</div>
                        </label>
                        <div class="input-group input-group--mini  @if($advert->attr_3 != '') active-textarea @endif">
                            <textarea placeholder="Ви можете додати опис" name="attr_3"
                                      @if($advert->attr_3 == '') disabled="true" @endif >{{ $advert->attr_3 }}</textarea>
                        </div>
                        <label
                            class="checkbox checkbox--mtMini checkbox--big js-checkbox-slide @if($advert->attr_4 != '') checked @endif ">
                            <input type="checkbox" @if($advert->attr_4 != '') checked @endif value="1" id="equipment"/>
                            <div class="checkbox__text">Техніка, обладнання</div>
                        </label>
                        <div class="input-group input-group--mini  @if($advert->attr_4 != '') active-textarea @endif">
                            <textarea placeholder="Ви можете додати опис" name="attr_4"
                                      @if($advert->attr_4 == '') disabled="true" @endif >{{ $advert->attr_4 }}</textarea>
                        </div>
                        <label
                            class="checkbox checkbox--mtMini checkbox--big js-checkbox-slide @if($advert->attr_5 != '') checked @endif ">
                            <input type="checkbox" @if($advert->attr_5 != '') checked @endif value="1" id="building"/>
                            <div class="checkbox__text">Будівлі, споруди</div>
                        </label>
                        <div class="input-group input-group--mini  @if($advert->attr_5 != '') active-textarea @endif">
                            <textarea placeholder="Ви можете додати опис" name="attr_5"
                                      @if($advert->attr_5 == '') disabled="true" @endif@endif >{{ $advert->attr_5 }}</textarea>
                        </div>
                        <div class="input-group input-group--mtBig">
                            <textarea name="comment_1"
                                      placeholder="Як сьогодні використовується об'єкт?">{{ $advert->comment_1}}</textarea>
                        </div>
                        <div class="input-group input-group--mtBig">
                            <textarea name="comment_2" placeholder="Інші активи">{{ $advert->comment_2 }}</textarea>
                        </div>
                        <div class="input-group input-group--mtBig">
                            <textarea name="comment_3"
                                      placeholder="Додаткова інформація">{{ $advert->comment_3}}</textarea>
                        </div>
                        <div class="mini-title mini-title--mt">
                            <p>Завантажити фотографії</p>
                        </div>
                    </div>
                    <div class="create-advert__cadastData">
                        <div class="create-advert__dataCard">
                            <div class="text text--gray">
                                <p>Регіон</p>
                            </div>
                            <div class="text text--black">
                                <p id="region">{{ $advert->region }}</p>
                            </div>
                        </div>
                        <div class="create-advert__dataCard">
                            <div class="text text--gray">
                                <p>Район</p>
                            </div>
                            <div class="text text--black">
                                <p id="district">{{ $advert->district }}</p>
                            </div>
                        </div>
                        <div class="create-advert__dataCard">
                            <div class="text text--gray">
                                <p>Тип власності</p>
                            </div>
                            <div class="text text--black">
                                <p>{{ $advert->type }}</p>
                            </div>
                        </div>
                        <div class="create-advert__dataCard">
                            <div class="text text--gray">
                                <p>КОАТТУ</p>
                            </div>
                            <div class="text text--black">
                                <p>{{ $advert->coatuu }}</p>
                            </div>
                        </div>
                        <div class="create-advert__dataCard flex-cont">
                            <div class="create-advert__dataCard">
                                <div class="text text--gray">
                                    <p>Зона</p>
                                </div>
                                <div class="text text--black">
                                    <p>{{ $advert->zone }}</p>
                                </div>
                            </div>
                            <div class="create-advert__dataCard">
                                <div class="text text--gray">
                                    <p>Квартал</p>
                                </div>
                                <div class="text text--black">
                                    <p>{{ $advert->quartal }}</p>
                                </div>
                            </div>
                            <div class="create-advert__dataCard">
                                <div class="text text--gray">
                                    <p>Площа</p>
                                </div>
                                <div class="text text--black">
                                    <p>{{ $advert->ga }} Га</p>
                                </div>
                            </div>
                        </div>
                        <div class="create-advert__dataCard">
                            <div class="text text--gray">
                                <p>Цільове призначення</p>
                            </div>
                            <div class="text text--black">
                                <p>
                                    {{ $advert->purpose }}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="upload-photos js-edit-upload-photos">
                        @foreach($advert->images as $image)
                            <label class="upload-photos__card active"
                                   data-photo="{{ asset('public/storage/AdvertPhoto/'.$image->image) }}">
                                <div class="upload-photos__loader"></div>
                                <div data-attr-id="{{ $image->id }}"
                                     class="upload-photos__exit upload-photos__exit-ajax-delete">
                                    <img src="{{ asset('public/frontend/img/svg/close.svg') }}" alt="">
                                </div>
                                <input type="file" name="image[]" accept="image/*,image/jpeg"
                                       class="upload-photos__file">
                                <img class="upload-photos__plus"
                                     src="{{ asset('public/frontend/img/svg/plus-black.svg') }}" alt="">
                                <span>
                                    <div class="error-custom">Формат фото: png, jpg (10Mb)</div>
                                </span>
                            </label>
                        @endforeach
                        <label class="upload-photos__card js-default-upload">
                            <div class="upload-photos__loader"></div>
                            <div class="upload-photos__exit">
                                <img src="{{ asset('public/frontend/img/svg/close.svg') }}" alt="">
                            </div>
                            <input type="file" name="image[]" accept="image/*,image/jpeg" class="upload-photos__file">
                            <img class="upload-photos__plus" src="{{ asset('public/frontend/img/svg/plus-black.svg') }}" alt="">
                            <span>
                                <div class="error-custom">Формат фото: png, jpg (10Mb)</div>
                            </span>
                        </label>
                    </div>
                    <div class="form-send form-send--mtBig form-send--100 justify-end">
                        <div class="ajax-validate-error">
                        </div>
                        <button type="submit" class="btn">СОХРАНИТЬ</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <div class="popup edit-profile">
        <div class="popup__wrap popup__wrap--min">
            <div class="title title--center title--mini">
                <p>Зміни успішно збережені!</p>
            </div>
        </div>
    </div>
    <div class="popup delete-image">
        <div class="popup__wrap popup__wrap--min">
            <div class="title title--center title--mini">
                <p>Зображення успішно видалено!</p>
            </div>
        </div>
    </div>
    <script>
        $('.create-advert__form').on('submit', function (e) {
            e.preventDefault();
            if ($(this).valid()) {
                $('.upload-photos__card:not(.active)').find('input').removeAttr('name');

                var formData = new FormData($(this)[0]);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/customer/edit-advert',
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    data: formData,
                    beforeSend: function() {
                        $('.loader').addClass('active');
                    },
                    success: function (data) {
                        $('.loader').removeClass('active');
                        $('.upload-photos__card:not(.active)').find('input').attr('name', 'image[]');
                        $('.popup.edit-profile').addClass('js-popup-show');

                        setTimeout(function () {
                            $('.popup.edit-profile').removeClass('js-popup-show');
                        }, 2000);
                    },
                    error: function (data) {
                        $('.loader').removeClass('active');
                        $('.upload-photos__card:not(.active)').find('input').attr('name', 'image[]');

                        var errors = data.responseJSON;
                        $('.ajax-validate-error').html('');
                        $.each(errors.errors, function (index, value) {
                            $('.ajax-validate-error').append('<span><div class="error">' + value + '</div></span>');
                        });
                    }
                });
            }
        });
        $('.upload-photos__exit-ajax-delete').on('click', function (e) {
            e.preventDefault();
            var formData = new FormData();
            formData.append('id', $(this).attr('data-attr-id'));
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/customer/delete-photo',
                type: 'POST',
                contentType: false,
                processData: false,
                data: formData,
                success: function (data) {
                    $('.popup.delete-image').addClass('js-popup-show');

                    setTimeout(function () {
                        $('.popup.delete-image').removeClass('js-popup-show');
                    }, 2000);
                },
                error: function (data) {
                    var errors = data.responseJSON;
                    $('.ajax-validate-error').html('');
                    $.each(errors.errors, function (index, value) {
                        $('.ajax-validate-error').append('<span><div class="error">' + value + '</div></span>');
                    });
                }
            });
        });

    </script>

@endsection
