@extends('frontend.includes.header')

@section('content')
    <section class="create-advert section-dark">
        <div class="container">
            <div class="create-advert__wrap create-advert__wrap--medium">
                <div class="title title--sub title--center">
                    <p>Помилка публікації</p>
                </div>
                <div class="text text--center text--mtMin">
                    <p>Вибачте, оголошення з даними кадастровим номером вже рекламується іншим агентом</p>
                </div>
                <div class="setNumber">
                    <div class="setNumber__number">
                        <p>{{ $cadnum }}</p>
                    </div>
                </div>
                <div class="text text--mtBig text--center">
                    <a href="/" class="text__link text__link--arrow">
                        <img src="{{ asset('public/frontend/img/svg/arrow-back.svg') }}" alt="">
                        <span>Повернутися на головну</span>
                    </a>
                </div>
            </div>
        </div>
    </section>

@endsection
