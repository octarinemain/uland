@extends('frontend.includes.header')

@section('content')
    <section class="create-advert section-dark">
        <div class="container">
            <div class="create-advert__wrap create-advert__wrap--medium">
                <div class="title title--sub title--center">
                    <p>Помилка публікації</p>
                </div>
                <div class="text text--center text--mtMedium">
                    <p>Вибачте, оголошення з даними кадастровим номером вже існує в базі.</p>
                </div>
                <div class="setNumber">
                    <div class="setNumber__number">
                        <p>{{ $cadnum }}</p>
                    </div>
                </div>
                <div class="text text--center text--mtMin">
                    <p>
                        В даний момент його просуває і продає агент нашої системи. До тих пір, поки не закінчиться
                         період оплати оголошення агентом, воно буде відображатися на його сторінці.
                    </p>
                </div>
                <div class="text text--mtMin text--center">
                    <a href="/" class="text__link">
                        <span>Подивитися сторінку оголошення</span>
                    </a>
                </div>
                <div class="form-send form-send--mt form-send--center">
                    <a href="/customer/check-cadnum" class="btn">СПРОБУВАТИ ЩЕ РАЗ</a>
                </div>
            </div>
        </div>
    </section>

@endsection
