@extends('frontend.includes.header')

@section('content')
    <section class="create-advert section-dark">
        <div class="container">
            <div class="create-advert__wrap create-advert__wrap--medium">
                <div class="title title--sub title--center">
                    <p>Помилка публікації</p>
                </div>
                <div class="text text--center text--mtMedium">
                    <p>Вибачте, оголошення з даними кадастровим номером вже існує в базі.</p>
                </div>
                <div class="setNumber">
                    <div class="setNumber__number">
                        <p>{{ $cadnum }}</p>
                    </div>
                </div>
                <div class="text text--mtMin text--center">
                    <a href="/" class="text__link">
                        <span>Подивитися сторінку оголошення</span>
                    </a>
                </div>
                <div class="text text--center text--mtMin">
                    <p>Якщо Ви хочете продавати дану ділянку, зробіть запит власнику.</p>
                </div>
                <form action="" class="formCustom">
                    <input hidden value="{{ $cadnum }}" name="cadnum" >
                    <textarea class="custom-textarea custom-textarea--mt"
                              name="message"
                              placeholder="Ваше повідомлення власнику ..."
                    ></textarea>
                    <div class="ajax-validate-error"></div>
                    <button type="submit" class="btnSend">
                        <img src="{{ asset('public/frontend/img/svg/arrow-send.svg') }}" alt="">
                    </button>
                </form>
            </div>
        </div>
        <div class="popup edit-profile">
            <div class="popup__wrap popup__wrap--min">
                <div class="title title--center title--mini">
                    <p>Запит успішно відправлений!</p>
                </div>
            </div>
        </div>
    </section>
<script>
    $('.formCustom').on('submit', function (e) {
        e.preventDefault();
        $('.ajax-validate-error').html('');
        if($(this).valid()) {
            var formData = new FormData($(this)[0]);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/customer/send-message-to-owner',
                type: 'POST',
                contentType: false,
                processData: false,
                data: formData,
                success: function (data) {
                    $('.popup.edit-profile').addClass('js-popup-show');
                    $('body').addClass('no-scrolling');
                    $('.formCustom').remove();
                },
                error: function (data) {
                    var errors = data.responseJSON;
                    $('.customer-reset-password .ajax-validate-error').html('');
                    $.each(errors.errors, function (index, value) {
                        $('.customer-reset-password .ajax-validate-error').append('<span><div class="error">' + value + '</div></span>');
                    });
                }
            });
        }
    });
</script>
@endsection
