@extends('frontend.includes.header')

@section('content')
    <section class="create-advert section-dark">
        <div class="container">
            <div class="create-advert__wrap create-advert__wrap--min">
                <div class="title title--sub title--center">
                    <p>Помилка публікації</p>
                </div>
                <div class="text text--center text--mtMedium">
                    <p>Вибачте, даний кадастровий номер не існує в базі.</p>
                </div>
                <div class="setNumber">
                    <div class="setNumber__number">
                        <p>{{ $cadnum }}</p>
                    </div>
                </div>
                <div class="text text--center text--mtMin">
                    <p>Зверніться до відповідних служб для його реєстрації.</p>
                </div>
                <div class="form-send form-send--mt form-send--center">
                    <a href="/customer/check-cadnum" class="btn">СПРОБУВАТИ ЩЕ РАЗ</a>
                </div>
                <div class="text text--mtBig text--center">
                    <a href="/" class="text__link text__link--arrow">
                        <img src="{{ asset('public/frontend/img/svg/arrow-back.svg') }}" alt="">
                        <span>Повернутися на головну</span>
                    </a>
                </div>
            </div>
        </div>
    </section>

@endsection
