@extends('frontend.includes.header')

@section('content')
    <section class="create-advert section-dark">
        <div class="container">
            <div class="create-advert__wrap create-advert__wrap--medium">
                <div class="title title--sub title--center">
                    <p>Увага</p>
                </div>
                <div class="text text--center text--mtMin">
                    <p>Вибачте, оголошення з даними кадастровим номером вже існує</p>
                </div>
                <div class="setNumber">
                    <div class="setNumber__number">
                        <p>{{ $cadnum }}</p>
                    </div>
                </div>
                <form action="" class="formCustom">
                    <input hidden value="{{ $cadnum }}" name="cadnum" >
                    <div class="ajax-validate-error"></div>
                    <div class="form-send form-send--mt form-send--center">
                        <button type="submit" class="btn">СТАТИ ВЛАСНИКОМ</button>
                    </div>
                </form>
                <div class="text text--mtBig text--center">
                    <a href="/" class="text__link text__link--arrow">
                        <img src="{{ asset('public/frontend/img/svg/arrow-back.svg') }}" alt="">
                        <span>Повернутися на головну</span>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <script>
        $('.formCustom').on('submit', function (e) {
            e.preventDefault();
            $('.ajax-validate-error').html('');
            if($(this).valid()) {
                var formData = new FormData($(this)[0]);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/customer/set-owner',
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    data: formData,
                    success: function (data) {
                        window.location = '/customer/profile'
                    },
                    error: function (data) {
                        var errors = data.responseJSON;
                        $('.customer-reset-password .ajax-validate-error').html('');
                        $.each(errors.errors, function (index, value) {
                            $('.customer-reset-password .ajax-validate-error').append('<span><div class="error">' + value + '</div></span>');
                        });
                    }
                });
            }
        });
    </script>
@endsection
