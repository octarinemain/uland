@extends('frontend.includes.header')

@section('content')
    <section class="create-advert section-dark">
        <div class="container">
            <div class="create-advert__wrap create-advert__wrap--medium">
                <div class="title title--sub title--center">
                    <p>Дякую за публікацію оголошення!</p>
                </div>
                <div class="text text--center text--mtMin">
                    <p>Після того, як Ваше оголошення пройде модерацію, воно стане доступним іншим користувачам.
                        Подивитися всі Ваші оголошення Ви можете <a href="/customer/profile">тут</a>.
                    </p>
                </div>
                <div class="form-send form-send--mt form-send--center">
                    <a href="/customer/check-cadnum" class="btn">ОПУБЛІКУВАТИ ЩЕ ОГОЛОШЕННЯ</a>
                </div>
                <div class="text text--center">
                    <a class="text__link text__link--arrow text__link--mtBig text__link--hover"
                       href="/">
                        <img class="hover-off" src="{{ asset('public/frontend/img/svg/arrow-back.svg') }}" alt="">
                        <img class="hover-on" src="{{ asset('public/frontend/img/svg/arrow-back-y.svg') }}" alt="">
                        <span>Повернутися на головну</span>
                    </a>
                </div>
            </div>
        </div>
    </section>

@endsection
