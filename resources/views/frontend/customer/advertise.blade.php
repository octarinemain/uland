@extends('frontend.includes.header')

@section('content')
    <section class="come-back dark-section">
        <div class="container">
            <div class="text">
                <a href="/" class="text__link text__link--arrow">
                    <img class="hover-off" src="{{ asset('public/frontend/img/svg/arrow-back.svg') }}" alt="">
                    <img class="hover-on" src="{{ asset('public/frontend/img/svg/arrow-back-y.svg') }}" alt="">
                    <span>Повернутися на головну</span>
                </a>
            </div>
        </div>
    </section>
    <section class="promote">
        <div class="container">
            <div class="title title--sub">
                <p>Реклама оголошення</p>
            </div>
            <div class="ads ads--long ads--center ads--noPaddingR">
                <div class="ads__wrap">
                    <div class="ads__cardContent">
                        <a href="/advert/id={{ $advert->id }}" class="ads__card">
                            <div class="ads__cardWrap">
                                <div class="ads__allotment">
                                    <p>top</p>
                                </div>
                                <div class="ads__addressInfoWrap">
                                    <div class="ads__addressInfo">
                                        <div class="ads__section">
                                            <div class="text text--gray text--normal">
                                                <p>Регіон</p>
                                            </div>
                                            <div class="text text--black">
                                                <p>{{ $advert->region }}</p>
                                            </div>
                                        </div>
                                        <div class="ads__section">
                                            <div class="text text--gray text--normal">
                                                <p>Район</p>
                                            </div>
                                            <div class="text text--black">
                                                <p>{{ $advert->district }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="ads__section">
                                    <div class="text text--gray text--normal">
                                        <p>Площа, Га</p>
                                    </div>
                                    <div class="text text--black">
                                        @if($advert->type == 'Продаж')
                                            <p>{{ $advert->ga}}</p>
                                        @else
                                            <p>{{ $advert->ga_to_sell }}</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="ads__priceInfo">
                                    <div class="ads__section">
                                        <div class="text text--gray text--normal">
                                            <p>Ціна за 1 Га</p>
                                        </div>
                                        <div class="text text--black">
                                            <p><span class="js-price-num">{{ $advert->price_per_ga }}</span> грн.</p>
                                        </div>
                                    </div>
                                    <div class="ads__section">
                                        <div class="text text--gray text--normal">
                                            <p>Ціна за все</p>
                                        </div>
                                        <div class="text text--black">
                                            <p><span class="js-price-num">{{ $advert->price }}</span> грн.</p>
                                        </div>
                                    </div>
                                    <div class="ads__section ads__section--cadNum">
                                        <div class="text text--gray text--normal">
                                            <p>Кадастровий номер</p>
                                        </div>
                                        <div class="text text--black">
                                            <p>{{ $advert->cadnum }}</p>
                                        </div>
                                    </div>
                                    <div class="ads__section ads__section--extra flex-cont">
                                        @if($advert->discount == 1)
                                            <div class="extra-card">
                                                <img src="{{ asset('public/frontend/img/svg/Auction.svg') }}"
                                                     alt="">
                                                <div class="extra-card__inform">
                                                    <div class="text text--mini">
                                                        <p>Можливий торг</p>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        @if($advert->attr_5 != '')
                                            <div class="extra-card">
                                                <img src="{{ asset('public/frontend/img/svg/house.svg') }}"
                                                     alt="">
                                                <div class="extra-card__inform">
                                                    <div class="text text--mini">
                                                        <p>Є споруди</p>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        <div class="viewers">
                                            <div class="viewers__icon">
                                                <img src="{{ asset('public/frontend/img/svg/eye.svg') }}"
                                                     alt="">
                                            </div>
                                            <div class="viewers__value">
                                                <div class="text text--gray text--normal">
                                                    <p>@if($advert->views != 0) {{ $advert->views }} @else
                                                            0 @endif</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="promote__wrap">
                <div class="promote__services">
                    <div class="title title--sub">
                        <p>Послуги</p>
                    </div>
                    <form action="" id="promote-form">
                        <input hidden value="{{$advert->id}}" id="advert-id"/>
                        <ul class="promote__navServeces">
                            <li>
                                <div class="promote__navItem">
                                    <label class="checkbox checkbox--big">
                                        <input type="checkbox" id="allotment"/>
                                        <div class="checkbox__text">Виділення червоним на 30 днів</div>
                                    </label>
                                    <div class="promote__price">
                                        <p class="text"><span class="js-single-price">30</span> грн</p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="promote__navItem">
                                    <label class="checkbox checkbox--big">
                                        <input type="checkbox" value="1" id="upAdvert"/>
                                        <div class="checkbox__text">
                                            Піднімає оголошення в верх списку звичайних оголошень 1 раз в день
                                        </div>
                                    </label>
                                    <div class="promote__price">
                                        <p class="text"><span class="js-single-price">10</span> грн</p>
                                    </div>
                                </div>
                                <div class="input-group input-group--select input-group--select-min js-disabled">
                                    <select id="upAdvert-select">
                                        <option value="1 день (зараз)" data-value="10" data-days="1">1 день (зараз)
                                        </option>
                                        <option value="7 день (зараз)" data-value="56" data-days="7">7 дней (зараз)
                                        </option>
                                        <option value="30 день (зараз)" data-value="180" data-days="30">30 дней
                                            (зараз)
                                        </option>
                                    </select>
                                </div>
                            </li>
                            <li>
                                <div class="promote__navItem">
                                    <label class="checkbox checkbox--big">
                                        <input type="checkbox" value="1" id="top"/>
                                        <div class="checkbox__text">
                                            TOP-оголошення (вивід оголошення до списку TOP оголошень)
                                        </div>
                                    </label>
                                    <div class="promote__price">
                                        <p class="text"><span class="js-single-price">70</span> грн</p>
                                    </div>
                                </div>
                                <div class="input-group input-group--select input-group--select-min js-disabled">
                                    <select id="top-select">
                                        <option value="На 7 дней" data-value="70" data-days="7">На 7 дней</option>
                                        <option value="На 30 дней" data-value="240" data-days="30">На 30 дней</option>
                                    </select>
                                </div>
                            </li>
                        </ul>
                        <div class="form-send form-send--mt">
                            <div class="flex-cont align-center">
                                <div class="title title--mini">
                                    <p>Загальна вартість:</p>
                                </div>
                                <div class="promote__price promote__price--minMargL">
                                    <p class="text"><span class="js-all-sum">0</span> грн</p>
                                </div>
                            </div>
                            <button type="submit" class="btn" disabled="disabled">СПЛАТИТИ</button>
                        </div>
                    </form>
                </div>
                <div class="promote__servicesPackages">
                    <div class="promote__servicesPackagesTitle flex-cont align-center justify-sb">
                        <div class="title title--sub">
                            <p>Пакети</p>
                        </div>
                        <div class="text text--gray">
                            <p>Пакети дозволяють отримати більше послуг, але за вигідною ціною</p>
                        </div>
                    </div>
                    <div class="promote__servicesCardWrap">
                        <div class="promote__servicesCard">
                            <div class="promote__servicesCardTitle">
                                <div class="text text--bold text--ttu text--ls text--center">
                                    <p>СТАНДАРТНИЙ</p>
                                </div>
                            </div>
                            <div class="promote__servicesCardContent">
                                <div class="promote__price promote__price--MargNo">
                                    <p class="text text--center"><b><span>60</span> грн</b></p>
                                    <p class="text text--gray text--center">замість 80 грн</p>
                                </div>
                            </div>
                            <ul>
                                <li class="flex-cont justify-sb">
                                    <p class="text text--min">Підняття в верх списку</p>
                                    <p class="text text--min">1 раз</p>
                                </li>
                                <li class="flex-cont justify-sb">
                                    <p class="text text--min">TOP - оголошення</p>
                                    <p class="text text--min">на 7 дней</p>
                                </li>
                                <li class="flex-cont justify-sb">
                                    <p class="text text--min">Виділення червоним</p>
                                    <p class="text text--min">на 30 дней</p>
                                </li>
                            </ul>
                            <div class="promote__servicesCardButton">
                                <a target="_blank" href="/customer/advertise/id={{ $advert->id }}/type=1"
                                   class="btn btn--custom">СПЛАТИТИ</a>
                            </div>
                        </div>
                        <div class="promote__servicesCard promote__servicesCard--custom">
                            <div class="promote__servicesCardTitle">
                                <div class="text text--bold text--ttu text--ls text--center text--blue">
                                    <p>СУПЕР ВИГІДНИЙ</p>
                                </div>
                                <div class="text text--miniPlus text--center">
                                    <p>в 5 разів більше переглядів</p>
                                </div>
                            </div>
                            <div class="promote__servicesCardContent">
                                <div class="promote__price promote__price--MargNo">
                                    <p class="text text--center"><b><span>120</span> грн</b></p>
                                    <p class="text text--gray text--center">замість 200 грн</p>
                                </div>
                            </div>
                            <ul>
                                <li class="flex-cont justify-sb">
                                    <p class="text text--min">Підняття в верх списку</p>
                                    <p class="text text--min">7 раз</p>
                                </li>
                                <li class="flex-cont justify-sb">
                                    <p class="text text--min">TOP - оголошення</p>
                                    <p class="text text--min">на 14 днів</p>
                                </li>
                                <li class="flex-cont justify-sb">
                                    <p class="text text--min">Виділення червоним</p>
                                    <p class="text text--min">на 30 днів</p>
                                </li>
                            </ul>
                            <div class="promote__servicesCardButton">
                                <a target="_blank" href="/customer/advertise/id={{ $advert->id }}/type=2"
                                   class="btn btn--custom">СПЛАТИТИ</a>
                            </div>
                        </div>
                        <div class="promote__servicesCard">
                            <div class="promote__servicesCardTitle">
                                <div class="text text--bold text--ttu text--ls text--center">
                                    <p>МАКСИМАЛЬНИЙ</p>
                                </div>
                                <div class="text text--grayColor text--miniPlus text--center">
                                    <p>в 20 разів більше переглядів</p>
                                </div>
                            </div>
                            <div class="promote__servicesCardContent">
                                <div class="promote__price promote__price--MargNo">
                                    <p class="text text--center"><b><span>250</span> грн</b></p>
                                    <p class="text text--gray text--center">замість 400 грн</p>
                                </div>
                            </div>
                            <ul>
                                <li class="flex-cont justify-sb">
                                    <p class="text text--min">Підняття в верх списку</p>
                                    <p class="text text--min">30 раз</p>
                                </li>
                                <li class="flex-cont justify-sb">
                                    <p class="text text--min">TOP - оголошення</p>
                                    <p class="text text--min">на 30 днів</p>
                                </li>
                                <li class="flex-cont justify-sb">
                                    <p class="text text--min">Виділення червоним</p>
                                    <p class="text text--min">на 30 днів</p>
                                </li>
                            </ul>
                            <div class="promote__servicesCardButton">
                                <a target="_blank" href="/customer/advertise/id={{ $advert->id }}/type=3"
                                   class="btn btn--custom">СПЛАТИТИ</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
