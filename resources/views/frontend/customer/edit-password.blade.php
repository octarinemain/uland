@extends('frontend.includes.header')

@section('content')
    <section class="registration section-dark">
        <div class="container">
            <div class="registration__form">
                <div class="title title--sub title--center">
                    <p>Змінити пароль</p>
                </div>
                <form method="POST" class="customer-reset-password">
                    {{ csrf_field() }}
                    <div class="input-group input-group--mt">
                        <label for="pass">Введіть старий пароль</label>
                        <input type="password" name="old_password" required>
                        <div class="ajax-validate-error"></div>
                    </div>
                    <div class="input-group input-group--mt">
                        <label for="pass">Придумайте новий пароль</label>
                        <input type="password" id="password" name="password" required>
                    </div>
                    <div class="input-group input-group--mt">
                        <label for="password_confirmation">Повторіть пароль</label>
                        <input type="password" id="cfmPassword" name="password_confirmation" required>
                    </div>
                    <div class="form-send form-send--mt justify-end">
                        <button type="submit" class="btn">ЗБЕРЕГТИ</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <script>
        $('.customer-reset-password').on('submit', function (e) {
            e.preventDefault();
            $('.ajax-validate-error').html('');

            if($(this).valid()) {
                var formData = new FormData($(this)[0]);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/customer/edit-password',
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    data: formData,
                    beforeSend: function() {
                        $('.loader').addClass('active');
                    },
                    success: function (data) {
                        $('.loader').removeClass('active');
                        window.location = data;
                    },
                    error: function (data) {
                        $('.loader').removeClass('active');
                        var errors = data.responseJSON;
                        $('.customer-reset-password .ajax-validate-error').html('');
                        $.each(errors.errors, function (index, value) {
                            $('.customer-reset-password .ajax-validate-error').append('<span><div class="error">' + value + '</div></span>');
                        });
                    }
                });
            }
        });

    </script>
@endsection
