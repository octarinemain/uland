@extends('frontend.includes.header')

@section('content')
    <section class="registration section-dark">
        <div class="container">
            <div class="registration__form registration__form--big">
                <div class="title title--sub title--center">
                    <p>Редагувати особисті дані</p>
                </div>
                <form class="edit-customer">
                    {{ csrf_field() }}
                    <div class="register-step-wrap">
                        <div class="input-group val">
                            <label for="name">Ім'я</label>
                            <input type="text" id="name" name="name" required
                                   value="{{ Auth::guard('customer')->user()->name }}">
                            <div class="ajax-validate-error">
                                <span>
                                    <div class="error-step-3 type-name"></div>
                                </span>
                            </div>
                        </div>
                        <div class="input-group val">
                            <label for="surname">Прізвище</label>
                            <input type="text" id="surname" name="surname" required
                                   value="{{ Auth::guard('customer')->user()->surname }}">
                            <div class="ajax-validate-error">
                                <span>
                                    <div class="error-step-3 type-surname"></div>
                                </span>
                            </div>
                        </div>
                        <div class="input-group val disabled-input">
                            <label for="surname">Телефон</label>
                            <input readonly value="{{ Auth::guard('customer')->user()->phone }}">
                            <img src="{{ asset('public/frontend/img/check.png') }}" class="success-data" alt="">
                            <div class="ajax-validate-error">
                                <span>
                                    <div class="error-step-3 type-surname"></div>
                                </span>
                            </div>
                        </div>
                        <div
                            class="input-group val @if(Auth::guard('customer')->user()->email_is_active == 1) disabled-input @endif">
                            <label for="email">Email</label>
                            <input @if(Auth::guard('customer')->user()->email_is_active == 1) readonly
                                   @endif type="email" id="email" name="email" required
                                   value="{{ Auth::guard('customer')->user()->email }}">
                            @if(Auth::guard('customer')->user()->email_is_active == 0)
                                <a class="input-group__link" id="send-email-activate" href="#">Підтвердити</a>
                            @else
                                <img src="{{ asset('public/frontend/img/check.png') }}" class="success-data" alt="">
                            @endif
                            <div class="ajax-validate-error">
                                <span>
                                    <div class="error-step-3 type-email"></div>
                                </span>
                            </div>
                        </div>
                        {{--<div class="input-group js-selectCurrent-1 val js-active-input get-api-region">--}}
                        {{--@if(Auth::guard('customer')->user()->role == 'Я собственник')--}}
                        {{--<label>Місто проживання</label>--}}
                        {{--<input type="text" id="city" name="city" class="city-request-api" required--}}
                        {{--value="{{ Auth::guard('customer')->user()->city }}">--}}
                        {{--@else--}}
                        {{--<label>Регіон роботи</label>--}}
                        {{--<input type="text" id="city" name="city" class="region-request-api" required--}}
                        {{--value="{{ Auth::guard('customer')->user()->city }}">--}}
                        {{--@endif--}}
                        {{--<div class="ajax-validate-error">--}}
                        {{--<span>--}}
                        {{--<div class="error-step-3 type-city"></div>--}}
                        {{--</span>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        @if(Auth::guard('customer')->user()->role == 'Я собственник')
                            <div class="input-group js-selectCurrent-1 val js-active-input get-api-region">
                                <label>Місто проживання</label>
                                <input type="text" id="city" name="city" class="city-request-api" required
                                       value="{{ Auth::guard('customer')->user()->city }}">
                                <div class="ajax-validate-error">
                                    <div>
                                        <div class="error-step-3 type-city"></div>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="input-group js-selectCurrent-2 val js-active-input get-api-region">
                                <label>Регіон роботи</label>
                                <input type="text" id="city" name="city" class="region-request-api" required
                                       value="{{ Auth::guard('customer')->user()->city }}">
                                <div class="ajax-validate-error">
                                    <div>
                                        <div class="error-step-3 type-city"></div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="input-group val">
                            <label for="birthday">Дата народження</label>
                            <input type="text" id="birthday" name="birthday" required
                                   value="{{ Auth::guard('customer')->user()->birthday }}">
                            <div class="ajax-validate-error">
                                <span>
                                    <div class="error-step-3 type-birthday"></div>
                                </span>
                            </div>
                        </div>
                        <div class="input-group val">
                            <label for="pass">Пароль</label>
                            <input type="password" id="password" name="password" value="qqqqqq" readonly required>
                            <a class="input-group__link" href="/customer/edit-password">Змінити пароль</a>

                            <div class="ajax-validate-error">
                                <span>
                                    <div class="error-step-3 type-password"></div>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-send form-send--centerMob form-send--mt">
                        <label
                            class="registration__photo-wrap @if(Auth::guard('customer')->user()->avatar != '') upload-success @endif">
                            <div class="registration__photo">
                                @if(Auth::guard('customer')->user()->avatar != '')
                                    <img
                                        src="{{ asset('public/storage/Avatar/'.Auth::guard('customer')->user()->avatar) }}"
                                        alt="" id="userPhoto">
                                @else
                                    <img src="{{ asset('public/frontend/img/svg/plus-black.svg') }}" class="user-plus"
                                         id="userPhoto" alt="">
                                @endif
                            </div>
                            <div class="form-send__text">
                                <span class="upload-start">Завантажте Ваше фото</span>
                                <span class="upload-change">Змінити фото</span>
                            </div>
                            <input type="file" id="upload">
                            <input type="text" id="upload-success" name="avatar" hidden>
                            <span>
                                <div class="error-custom">Формат фото: png, jpg <br> Не більше 5Mb</div>
                            </span>
                        </label>
                        <button type="submit" class="btn">ЗБЕРЕГТИ ЗМІНИ</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- Popup -->
    <div class="popup edit-email">
        <div class="popup__wrap">
            <div class="popup__close js-close-popup">
                <img src="{{ asset('public/frontend/img/svg/close.svg') }}" alt="">
            </div>
            <div class="title title--center title--mini">
                <p>Підтвердіть email адрес</p>
            </div>
            <div class="text text--mtMedium text--center">
                <p>
                    На вказаний email адреса був відправлений лист. Перейдіть по посиланню <br>
                    в листі для підтвердження свого email.
                </p>
            </div>
            <div class="btn-wrap btn-wrap--mtMedium">
                <a href="#" class="btn js-close-popup">ДОБРЕ</a>
            </div>
        </div>
    </div>
    <div class="popup edit-profile">
        <div class="popup__wrap popup__wrap--min">
            <div class="title title--center title--mini">
                <p>Зміни успішно збережені!</p>
            </div>
        </div>
    </div>
    <!-- Popup -->
    <div class="popup cropper">
        <div class="popup__wrap">
            <div class="popup__close js-close-popup">
                <img src="{{ asset('public/frontend/img/svg/close.svg') }}" alt="">
            </div>
            <div id="upload-demo"></div>
            <a href="#" class="upload-result btn">Зберегти</a>
        </div>
    </div>
    <script>
        const region = [
            'Автономна Республіка Крим',
            'Вінницька область',
            'Волинська область',
            'Дніпропетровська область',
            'Донецька область',
            'Житомирська область',
            'Закарпатська область',
            'Запорізька область',
            'Івано-Франківська область',
            'Київська область',
            'Кіровоградська область',
            'Луганська область',
            'Львівська область',
            'Миколаївська область',
            'Одеська область',
            'Полтавська область',
            'Рівненська область',
            'Сумська область',
            'Тернопільська область',
            'Харківська область',
            'Херсонська область',
            'Хмельницька область',
            'Черкаська область',
            'Чернівецька область',
            'Чернігівська область',
        ];
        let citys = [];

        $.getJSON('../public/citys.json', (data) => {
            data.map((e) => {
                citys.push(e);
            });
        });

        $('.js-selectCurrent-1 input, .js-selectCurrent-2 input').on('keyup change', function () {
            $('.type-city').text('');
        });

        $('.edit-customer').on('submit', function (e) {
            e.preventDefault();
            $('.ajax-validate-error .error-step-3').html('');
            const activeGeo = $('.js-active-input');
            const activeGeoInput = $('.js-active-input input');
            const activeGeoValue = activeGeoInput.val();
            let validGeo = false;

            if (activeGeo.hasClass('js-selectCurrent-1')) {
                validGeo = citys.indexOf(activeGeoValue) !== -1;
            }

            if (activeGeo.hasClass('js-selectCurrent-2')) {
                validGeo = region.indexOf(activeGeoValue) !== -1;
            }

            if (!validGeo && activeGeoValue.length !== 0) {
                $('.type-city').text('Виберіть один з варіантів');
            }

            if ($(this).valid() && validGeo) {
                var formData = new FormData($(this)[0]);
                $('.registration__photo-wrap:not(.upload-success)').find('#upload-success').removeAttr('name');

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/customer/edit-customer',
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    data: formData,
                    success: function (data) {
                        $('.registration__photo-wrap:not(.upload-success)').find('#upload-success').attr('name', 'avatar');

                        $('.popup.edit-profile').addClass('js-popup-show');

                        setTimeout(function () {
                            $('.popup.edit-profile').removeClass('js-popup-show');
                        }, 2000);
                    },
                    error: function (data) {
                        $('.registration__photo-wrap:not(.upload-success)').find('#upload-success').attr('name', 'avatar');

                        var errors = data.responseJSON;
                        $('.edit-customer .error').html('');
                        if (errors.errors.email) $('.type-email').text(errors.errors.email[0]);
                        if (errors.errors.name) $('.register-step-3 .type-name').text(errors.errors.name[0]);
                        if (errors.errors.surname) $('.register-step-3 .type-surname').text(errors.errors.surname[0]);
                        if (errors.errors.city) $('.register-step-3 .type-city').text(errors.errors.city[0]);
                        if (errors.errors.birthday) $('.register-step-3 .type-birthday').text(errors.errors.birthday[0]);
                    }
                });
            }
        });

        $('#send-email-activate').on('click', function (e) {
            e.preventDefault();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/customer/reSend-email-activate-link',
                type: 'POST',
                contentType: false,
                processData: false,
                data: '',
                success: function (data) {
                    $('.popup.edit-email').addClass('js-popup-show');
                }
            });
        });

    </script>

@endsection
