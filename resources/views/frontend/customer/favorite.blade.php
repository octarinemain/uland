@extends('frontend.includes.header')

@section('content')
    <section class="come-back dark-section">
        <div class="container">
            <div class="text">
                <a href="/" class="text__link text__link--arrow">
                    <img class="hover-off" src="{{ asset('public/frontend/img/svg/arrow-back.svg') }}" alt="">
                    <img class="hover-on" src="{{ asset('public/frontend/img/svg/arrow-back-y.svg') }}" alt="">
                    <span>Повернутися на сторінку профілю</span>
                </a>
            </div>
        </div>
    </section>
    <section class="favorites">
        <div class="container">
            <div class="title title--sub flex-cont align-center">
                <?php
                function plural_form($number, $after)
                {
                    $cases = array(2, 0, 1, 1, 1, 2);
                    echo $after[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
                }

                ?>
                <?php $adverts = Auth::guard('customer')->user()->favorites ?>


                @if(count($adverts) != 0)
                    <p>Вибрані оголошення</p>
                    <div class="signature signature--ml">
                        {{ count($adverts) }}
                        <?php
                        plural_form(
                            count($adverts),
                            /* варианты написания для количества 1, 2 и 5 */
                            array('оголошення', 'оголошення', 'оголошень')
                        );
                        ?>
                    </div>
                    @else
                        <p>У вас ще немає обраних оголошень</p>
                    @endif
            </div>
            <div class="ads ads--long ads--paddingMin">
                <div class="ads__wrap">
                    @foreach($adverts as $favorite)
                        <div class="ads__cardContent @if($favorite->advert->top != NULL && $favorite->advert->top != '0000-00-00 00:00:00') active @endif @if($favorite->advert->red != NULL && $favorite->advert->red != '0000-00-00 00:00:00') allotment @endif">
                            <a href="/advert/id={{ $favorite->advert->id }}" class="ads__card">
                                <div class="ads__type">
                                    <div class="text text--ttu text--blue text--bold text--mini-10">
                                        <p>{{ $favorite->advert->type }}</p>
                                    </div>
                                </div>
                                <div class="ads__cardWrap">
                                    <div class="ads__allotment">
                                        <p>top</p>
                                    </div>
                                    <div class="ads__addressInfoWrap">
                                        <div class="ads__addressInfo">
                                            <div class="ads__section">
                                                <div class="text text--gray text--normal">
                                                    <p>Регіон</p>
                                                </div>
                                                <div class="text text--black">
                                                    <p>{{ $favorite->advert->region }}</p>
                                                </div>
                                            </div>
                                            <div class="ads__section">
                                                <div class="text text--gray text--normal">
                                                    <p>Район</p>
                                                </div>
                                                <div class="text text--black">
                                                    <p>{{ $favorite->advert->district }}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ads__section">
                                        <div class="text text--gray text--normal">
                                            <p>Площа, Га</p>
                                        </div>
                                        <div class="text text--black">
                                            @if($favorite->advert->type == 'Продаж')
                                                <p>{{ $favorite->advert->ga}}</p>
                                            @else
                                                <p>{{ $favorite->advert->ga_to_sell }}</p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="ads__priceInfo">
                                        <div class="ads__section">
                                            <div class="text text--gray text--normal">
                                                <p>Ціна за 1 Га</p>
                                            </div>
                                            <div class="text text--black">
                                                <p><span class="js-price-num">{{ $favorite->advert->price_per_ga }}</span> грн.</p>
                                            </div>
                                        </div>
                                        <div class="ads__section">
                                            <div class="text text--gray text--normal">
                                                <p>Ціна за все</p>
                                            </div>
                                            <div class="text text--black">
                                                <p><span class="js-price-num">{{ $favorite->advert->price }}</span> грн.</p>
                                            </div>
                                        </div>
                                        <div class="ads__section ads__section--extra flex-cont">
                                            @if($favorite->advert->discount == 1)
                                                <div class="extra-card">
                                                    <img src="{{ asset('public/frontend/img/svg/Auction.svg') }}"
                                                         alt="">
                                                    <div class="extra-card__inform">
                                                        <div class="text text--mini">
                                                            <p>Можливий торг</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            @if($favorite->advert->attr_5 != '')
                                                <div class="extra-card">
                                                    <img src="{{ asset('public/frontend/img/svg/house.svg') }}"
                                                         alt="">
                                                    <div class="extra-card__inform">
                                                        <div class="text text--mini">
                                                            <p>Є споруди</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="viewers">
                                                <div class="viewers__icon">
                                                    <img src="{{ asset('public/frontend/img/svg/eye.svg') }}"
                                                         alt="">
                                                </div>
                                                <div class="viewers__value">
                                                    <div class="text text--gray text--normal">
                                                        <p>@if($favorite->advert->views != 0) {{ $favorite->advert->views }} @else
                                                                0 @endif</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ads__section ads__section--date">
                                            <div class="text text--gray text--normal">
                                                <p>Дата публікації</p>
                                            </div>
                                            <div class="text text--black">
                                                <p>{{ date('d.m.Y',strtotime($favorite->advert->created_at)) }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <div class="ads__section ads__section--star js-favorite active" data-advert-id="{{ $favorite->advert->id }}">
                                <i class="icon-star-outline">
                                    <img src="{{ asset('public/frontend/img/svg/star-outline.svg') }}" alt="">
                                </i>
                                <i class="icon-star">
                                    <img src="{{ asset('public/frontend/img/svg/star.svg') }}" alt="">
                                </i>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    <script>
        $('.event-advert').on('click', function (e) {
            e.preventDefault();
            if (confirm('Ви підтверджуєте дію?')) {
                window.location = $(this).attr('href');
            }
        })
    </script>
@endsection
