@extends('frontend.includes.header')

@section('content')
    <section class="come-back dark-section">
        <div class="container">
            <div class="text">
                <a href="/" class="text__link text__link--arrow">
                    <img class="hover-off" src="{{ asset('public/frontend/img/svg/arrow-back.svg') }}" alt="">
                    <img class="hover-on" src="{{ asset('public/frontend/img/svg/arrow-back-y.svg') }}" alt="">
                    <span>Повернутися на головну</span>
                </a>
            </div>
        </div>
    </section>
    <section class="profile">
        <div class="container">
            <div class="profile__wrap">
                <div class="profile__main">
                    <?php
                    function plural_form($number, $after)
                    {
                        $cases = array(2, 0, 1, 1, 1, 2);
                        echo $after[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
                    }
                    ?>
                    @if(Auth::guard('customer')->user()->role == 'Я агент по продаже')
                        <?php $adverts = Auth::guard('customer')->user()->agent_adverts(); ?>
                    @else
                        <?php $adverts = Auth::guard('customer')->user()->all_adverts(); ?>
                    @endif
                    @if(count($adverts) != 0)
                        <div class="ads ads__profile">
                            <div class="title title--sub flex-cont align-center">
                                <p>Мої оголощення</p>
                                <div class="signature signature--ml">
                                    <p>
                                        {{ $adverts->total() }}
                                        <?php
                                        plural_form(
                                            count($adverts),
                                            /* варианты написания для количества 1, 2 и 5 */
                                            array('оголошення', 'оголошення', 'оголошень')
                                        );
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <div class="ads__wrap">
                                @foreach($adverts as $advert)
                                    <div class="ads__cardContent @if($advert->agent_id != 0 && $advert->customer_id == Auth::guard('customer')->user()->id) assigned @endif @if($advert->top != NULL && $advert->top != '0000-00-00 00:00:00') active adv-period @endif @if($advert->red != NULL && $advert->red != '0000-00-00 00:00:00') allotment adv-period @endif @if($advert->once_top_day != NULL && $advert->once_top_day != '0000-00-00 00:00:00') adv-period @endif">
										<div class="ads__allotment">
											<p>top</p>
										</div>
                                        <a href="/advert/id={{ $advert->id }}" class="ads__card">
                                            <div class="ads__cardWrap">
												<div class="ads__type">
													<div class="text text--ttu text--blue text--bold text--mini-10">
														<p>{{ $advert->type }}</p>
													</div>
												</div>
												<div class="ads__one-info ads__one-info--region">
													<div class="ads__section">
														<div class="text text--gray text--normal text--break-word">
															<p>Регіон</p>
														</div>
														<div class="text text--black">
															<p>{{ $advert->region }}</p>
														</div>
													</div>
												</div>
												<div class="ads__one-info ads__one-info--distr">
													<div class="ads__section">
														<div class="text text--gray text--normal">
															<p>Район</p>
														</div>
														<div class="text text--black text--break-word">
															<p>{{ $advert->district }}</p>
														</div>
													</div>
												</div>
												<div class="ads__one-info ads__one-info--area">
													<div class="ads__section">
                                                        <div class="text text--gray text--no-wrap text--normal">
                                                            <p>Площа, Га</p>
                                                        </div>
                                                        <div class="text text--black text--no-wrap">
                                                            @if($advert->type == 'Продаж')
                                                                <p>{{ $advert->ga}}</p>
                                                            @else
                                                                <p>{{ $advert->ga_to_sell }}</p>
                                                            @endif
                                                        </div>
                                                    </div>
												</div>
												<div class="ads__one-info ads__one-info--empty"></div>
												<div class="ads__one-info ads__one-info--custom">
													<div class="ads__section">
														<div class="text text--gray text--no-wrap text--normal">
															<p>Ціна за 1 Га</p>
														</div>
														<div class="text text--black">
															<p><span class="js-price-num">{{ $advert->price_per_ga }}</span> грн.</p>
														</div>
													</div>
												</div>
												<div class="ads__one-info ads__one-info--custom">
													<div class="ads__section">
														<div class="text text--gray text--no-wrap text--normal">
															<p>Ціна за все</p>
														</div>
														<div class="text text--black">
															<p><span class="js-price-num">{{ $advert->price }}</span> грн.</p>
														</div>
													</div>
												</div>
												<div class="ads__one-info ads__one-info--date">
													<div class="ads__section">
														<div class="text text--gray text--no-wrap text--normal">
															<p>Дата публікації</p>
														</div>
														<div class="text text--black text--no-wrap">
															<p>{{ date('d.m.Y',strtotime($advert->created_at)) }}</p>
														</div>
													</div>
												</div>
												@if($advert->discount == 1 || $advert->attr_5 != '')
													<div class="ads__one-info ads__one-info--extra">
														<div class="ads__section ads__section--extra flex-cont">
															@if($advert->discount == 1)
																<div class="extra-card">
																	<img
																		src="{{ asset('public/frontend/img/svg/Auction.svg') }}"
																		alt="">
																	<div class="extra-card__inform">
																		<div class="text text--mini">
																			<p>Можливий торг</p>
																		</div>
																	</div>
																</div>
															@endif
															@if($advert->attr_5 != '')
																<div class="extra-card">
																	<img
																		src="{{ asset('public/frontend/img/svg/house.svg') }}"
																		alt="">
																	<div class="extra-card__inform">
																		<div class="text text--mini">
																			<p>Є споруди</p>
																		</div>
																	</div>
																</div>
															@endif
														</div>
													</div>
												@else
												<div class="ads__one-info ads__one-info--empty-extra"></div>
												@endif
												<div class="ads__one-info ads__one-info--empty"></div>
												<div class="ads__one-info ads__one-info--viewers">
													<div class="ads__section ads__section--extra flex-cont">
                                                        <div class="viewers">
                                                            <div class="viewers__icon">
                                                                <img
                                                                    src="{{ asset('public/frontend/img/svg/eye.svg') }}"
                                                                    alt="">
                                                            </div>
                                                            <div class="viewers__value">
                                                                <div class="text text--gray text--normal text--line-height-1">
                                                                    <p>@if($advert->views != 0) {{ $advert->views }} @else
                                                                            0 @endif</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
												</div>
												<div class="ads__info">
													<div class="ads__section">
														<div class="text text--min text--turquoise text--bold" id="ads__addAgent">
															<p>Закріплено за агентом</p>
														</div>
													</div>
												</div>
											</div>
										</a>
                                        <div class="ads__otherFunction">
                                            <div class="ads__section ads__section--menu flex-cont align-center">
												<div class="ads__top-section">
													@if($advert->search_status == 'Активно')
														<div class="text text--ttu text--bold text--mini active-state ads__status">
															<p>АКТИВНО</p>
														</div>
													@elseif($advert->search_status == 'Відключено')
														<div class="text text--ttu text--bold text--mini text--red-second ads__status">
															<p>ВІДКЛЮЧЕНО</p>
														</div>
													@elseif($advert->search_status == 'На модерації')
														<div class="text text--ttu text--bold text--mini moderation-state ads__status">
															<p>НА МОДЕРАЦІЇ</p>
														</div>
													@endif
													<div class="ads__menu dropdown-main">
														<div class="ads__dots dropdown-active">
															<div class="ads__dot"></div>
															<div class="ads__dot"></div>
															<div class="ads__dot"></div>
														</div>
														<ul class="ads__menuDrop dropdown dropdown--dark">
															@if(Auth::guard('customer')->user()->role == 'Я собственник' && $advert->agent_id == 0)
																<li><a href="/customer/edit-advert/id={{ $advert->id }}">Редагувати</a>
																</li>
															@endif
															@if($advert->agent_id == Auth::guard('customer')->user()->id && $advert->agent_id != 0)
																<li><a href="/customer/edit-advert/id={{ $advert->id }}">Редагувати</a>
																</li>
															@endif

															@if($advert->search_status != 'На модерації')

																@if($advert->customer_id == Auth::guard('customer')->user()->id)
																	<li><a class="event-advert"
																		href="/customer/unfasten-owner/id={{ $advert->id }}">
																			Я не власник, відкріпити
																		</a></li>
																@endif

																@if($advert->agent_id != 0 && $advert->customer_id == Auth::guard('customer')->user()->id)
																	<li><a class="event-advert"
																		href="/customer/unfasten-agent/id={{ $advert->id }}">Відв'язати агента</a></li>
																@endif
															@endif
														</ul>
													</div>
													@if($advert->search_status != 'На модерації')
														@if($advert->search_status == 'Відключено')
															@if(Auth::guard('customer')->user()->role == 'Я собственник' && $advert->agent_id == 0 ||
															$advert->agent_id == Auth::guard('customer')->user()->id && $advert->agent_id != 0)
																<a href="#" class="btn btn--activate" data-id="{{ $advert->id }}">Активувати</a>
															@else
																<a href="#" class="btn btn--disable">Активувати</a>
															@endif
														@else
															{{--Блок для кнопки закрыть где будет ПОПАП с инпутом цены и две кнопки
																и у каждой кнопки свой аякс будет--}}
															@if(Auth::guard('customer')->user()->role == 'Я собственник' && $advert->agent_id == 0 ||
															$advert->agent_id == Auth::guard('customer')->user()->id && $advert->agent_id != 0)
																<a href="#" class="btn btn--deactivate js-popup-button" data-id="{{ $advert->id }}" data-type="{{ $advert->type }}" data-popupShow="close-advert">Відключити</a>
															@else
																<a href="#" class="btn btn--disable">Відключити</a>
															@endif
														@endif
													@else
														<a href="#" class="btn btn--disable">Відключити</a>
													@endif
												</div>
												<div class="ads__bottom-section">
													{{--этот блок идёт если обьява не на модерации, то тогда мы видим норм кнопки активировать\закрыть обьявление--}}
													@if($advert->search_status != 'На модерації')
														@if(Auth::guard('customer')->user()->role == 'Я собственник' && $advert->agent_id == 0 ||
														$advert->agent_id == Auth::guard('customer')->user()->id && $advert->agent_id != 0)
															<a href="/customer/advertise/id={{ $advert->id }}" class="btn btn--publicity">Рекламувати</a>
															<div class="text text--turquoise text--min text--mt-5 js-popup-button" id="ads__period" data-popupShow="ads-period-{{ $advert->id }}">
																<p>Термін дії реклами</p>
															</div>
														@else
															<a href="#" class="btn btn--disable" disable>Рекламувати</a>
														@endif
													@else
														<a href="#" class="btn btn--disable" disable>Рекламувати</a>
													@endif
												</div>
                                            </div>
                                        </div>
                                        <!-- Popup -->
                                        <div class="popup ads-period-{{ $advert->id }}">
                                            <div class="popup__wrap">
                                                <div class="popup__close js-close-popup">
                                                    <img src="{{ asset('public/frontend/img/svg/close.svg') }}" alt="">
                                                </div>
                                                <div class="title title--center title--mini">
                                                    <p>Реклама оголошення</p>
                                                </div>
                                                <div class="advertising-info">
                                                    <div class="advertising-info__card">
                                                        <div
                                                            class="advertising-info__column advertising-info__column--name">
                                                            <div class="text">
                                                                <p>Топ-оголошення</p>
                                                            </div>
                                                        </div>
                                                        <div
                                                            class="advertising-info__column advertising-info__column--date">
                                                            @if($advert->top != NULL && $advert->top != '0000-00-00 00:00:00')
                                                                <div class="text">
                                                                    <p>діє до</p>
                                                                </div>
                                                                <div class="text text--grayColor">
                                                                    <p>{{ date('d-m-Y', strtotime($advert->top)) }}</p>
                                                                </div>
                                                            @else
                                                                <div class="text text--red">
                                                                    <p>не діє</p>
                                                                </div>
                                                            @endif
                                                        </div>
                                                        <div
                                                            class="advertising-info__column advertising-info__column--status">
                                                            @if($advert->top != NULL && $advert->top != '0000-00-00 00:00:00')
                                                                <div class="text">
                                                                    <a href="/customer/advertise/id={{ $advert->id }}">Продовжити</a>
                                                                </div>
                                                            @else
                                                                <div class="text">
                                                                    <a href="/customer/advertise/id={{ $advert->id }}">Купити</a>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="advertising-info__card">
                                                        <div
                                                            class="advertising-info__column advertising-info__column--name">
                                                            <div class="text">
                                                                <p>Підняття вгору списку</p>
                                                            </div>
                                                        </div>
                                                        <div
                                                            class="advertising-info__column advertising-info__column--date">
                                                            @if($advert->once_top_day != NULL && $advert->once_top_day != '0000-00-00 00:00:00')
                                                                <div class="text">
                                                                    <p>діє до</p>
                                                                </div>
                                                                <div class="text text--grayColor">
                                                                    <p>{{ date('d-m-Y', strtotime($advert->once_top_day)) }}</p>
                                                                </div>
                                                            @else
                                                                <div class="text text--red">
                                                                    <p>не діє</p>
                                                                </div>
                                                            @endif
                                                        </div>
                                                        <div
                                                            class="advertising-info__column advertising-info__column--status">
                                                            @if($advert->once_top_day != NULL && $advert->once_top_day != '0000-00-00 00:00:00')
                                                                <div class="text">
                                                                    <a href="/customer/advertise/id={{ $advert->id }}">Продовжити</a>
                                                                </div>
                                                            @else
                                                                <div class="text">
                                                                    <a href="/customer/advertise/id={{ $advert->id }}">Купити</a>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="advertising-info__card">
                                                        <div
                                                            class="advertising-info__column advertising-info__column--name">
                                                            <div class="text">
                                                                <p>Виділення червоним на 30 днів</p>
                                                            </div>
                                                        </div>
                                                        <div
                                                            class="advertising-info__column advertising-info__column--date">
                                                            @if($advert->red != NULL && $advert->red != '0000-00-00 00:00:00')
                                                                <div class="text">
                                                                    <p>діє до</p>
                                                                </div>
                                                                <div class="text text--grayColor">
                                                                    <p>{{ date('d-m-Y', strtotime($advert->red)) }}</p>
                                                                </div>
                                                            @else
                                                                <div class="text text--red">
                                                                    <p>не діє</p>
                                                                </div>
                                                            @endif
                                                        </div>
                                                        <div
                                                            class="advertising-info__column advertising-info__column--status">
                                                            @if($advert->red != NULL && $advert->red != '0000-00-00 00:00:00')
                                                                <div class="text">
                                                                    <a href="/customer/advertise/id={{ $advert->id }}">Продовжити</a>
                                                                </div>
                                                            @else
                                                                <div class="text">
                                                                    <a href="/customer/advertise/id={{ $advert->id }}">Купити</a>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
							</div>
							@include('frontend.layouts.pagination')
                        </div>
                    @else
                        <div class="ads">
                            <div class="title title--sub">
                                <p>Мої оголошення</p>
                            </div>
                            <div class="text text--mtMedium">
                                <p>У вас ще немає оголошень, <a href="/customer/check-cadnum"
                                                                class="text__link text__link--normal">додайте нове</a>.
                                </p>
                            </div>
                        </div>
                    @endif
                </div>
                <?php $customer = Auth::guard('customer')->user(); ?>
                @include('frontend.layouts.customer-card')
            </div>
        </div>
    </section>
    <script>
        $('.event-advert').on('click', function (e) {
            e.preventDefault();
            if (confirm('Ви підтверджуєте дію?')) {
                window.location = $(this).attr('href');
            }
        });
    </script>
@endsection
