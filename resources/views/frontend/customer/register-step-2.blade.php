@extends('frontend.includes.header')

@section('content')
    <section class="registration section-dark">
        <div class="container">
            <div class="registration__form">
                <div class="title title--sub title--center">
                    <p>Реєстрація</p>
                </div>
                <div class="text text--center text--mtBig">
                    <p>Ми відправили СМС з кодом для підтвердження, на номер:</p>
                </div>
                <div class="setNumber">
                    <div class="setNumber__number">
                        <p>{{ $phone }}</p>
                    </div>
                    <a href="/register-step-1">Змінити номер</a>
                </div>
                <form class="register-step-2">
                    {{ csrf_field() }}
                    <div class="input-group input-group--mt">
                        <label for="code">Код підтвердження</label>
                        <input type="number" id="code" name="code" required>
                        <div class="ajax-validate-error"></div>
                    </div>
                    <div class="form-send form-send--centerMob form-send--mt">
                        <div class="form-send__timing">
                            <div class="text text--gray">Залишилось:</div>
                            <p class="form-send__second"><span class="js-min"></span> : <span class="js-sec"></span></p>
                        </div>
                        <p class="form-send__text js-timing-step-2">СМС не прийшло? <span id="reset-message-token">Відправити ще раз</span></p>
                        <p class="form-send__text js-timing-step-3">Немає СМС? <span>Зверніться в тех підтримку</span></p>
                        <button type="submit" class="btn">ПІДТВЕРДИТИ КОД</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <script>
        $('.register-step-2').on('submit',function(e) {
            e.preventDefault();
            $('.ajax-validate-error').html('');

            if($(this).valid()){
                var formData = new FormData($(this)[0]);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/register-step-2',
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    data: formData,
                    beforeSend: function() {
                        $('.loader').addClass('active');
                    },
                    success: function (data) {
                        $('.loader').removeClass('active');
                        window.location = data;
                    },
                    error: function (data) {
                        $('.loader').removeClass('active');
                        var errors = data.responseJSON;
                        $('.register-step-2 .ajax-validate-error').html('');
                        $.each(errors.errors, function (index, value) {
                            $('.register-step-2 .ajax-validate-error').append('<span><div class="error">' + value + '</div></span>');
                        });
                    }
                });
            }
        });
        $('#reset-message-token').on('click',function() {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/reSend',
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    data: '',
                    success: function (data) {
                        $('.js-timing-step-2').css('display', 'none');
                        $('.js-timing-step-3').css('display', 'block');
                    },
                });
        });

    </script>

@endsection
