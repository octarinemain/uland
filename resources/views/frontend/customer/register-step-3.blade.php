@extends('frontend.includes.header')

@section('content')
    <section class="registration section-dark">
        <div class="container">
            <div class="registration__form registration__form--big">
                <div class="title title--sub title--center">
                    <p>Завершення реєстрації</p>
                </div>
                <form class="register-step-3 get-api-region" data-valid="true">
                    {{ csrf_field() }}
                    <div class="register-step-wrap">
                        <div class="input-group input-group--select input-group--select-opted js-select-form">
                            <label>Хто ви</label>
                            <select name="role" required>
                                <option value="Я собственник">Я власник</option>
                                <option value="Я агент по продаже">Я агент з продажу</option>
                            </select>
                            <div class="ajax-validate-error">
                                <span>
                                    <div class="error-step-3 type-role"></div>
                                </span>
                            </div>
                        </div>
                        <div class="input-group">
                            <label for="birthday">Дата народження</label>
                            <input type="text" id="birthday" name="birthday" required>
                            <div class="ajax-validate-error">
                                <span>
                                    <div class="error-step-3 type-birthday"></div>
                                </span>
                            </div>
                        </div>
                        <div class="input-group">
                            <label for="name">Ваше ім'я</label>
                            <input type="text" id="name" name="name" required>
                            <div class="ajax-validate-error">
                                <span>
                                    <div class="error-step-3 type-name"></div>
                                </span>
                            </div>
                        </div>
                        <div class="input-group">
                            <label for="surname">Прізвище</label>
                            <input type="text" id="surname" name="surname" required>
                            <div class="ajax-validate-error">
                                <span>
                                    <div class="error-step-3 type-surname"></div>
                                </span>
                            </div>
                        </div>
                        <div class="input-group js-selectCurrent-1">
                            <label for="city">Місто проживання</label>
                            <input type="text" class="city-request" id="city" name="city" required>
                            <div class="ajax-validate-error">
                                <div>
                                    <div class="error-step-3 type-city"></div>
                                </div>
                            </div>
                        </div>
                        <div class="input-group js-selectCurrent-2">
                            <label for="city">Регіон роботи</label>
                            <input type="text" class="city-request" id="region" disabled="true" hidden="true" name="city" required>
                            <div class="ajax-validate-error">
                                <div>
                                    <div class="error-step-3 type-city"></div>
                                </div>
                            </div>
                        </div>
                        <div class="input-group">
                            <label for="email">Email</label>
                            <input type="email" id="email" name="email" required>
                            <div class="ajax-validate-error">
                                <span>
                                    <div class="error-step-3 type-email"></div>
                                </span>
                            </div>
                        </div>
                        <div class="input-group">
                            <label for="pass">Пароль</label>
                            <input type="password" id="password" name="password" required>
                            <div class="ajax-validate-error">
                                <span>
                                    <div class="error-step-3 type-password"></div>
                                </span>
                            </div>
                        </div>
                        <div class="input-group">
                            <label for="password_confirmation">Повторіть пароль</label>
                            <input type="password" id="cfmPassword" name="password_confirmation" required>
                            <div class="ajax-validate-error"></div>
                        </div>
                    </div>
                    <div class="form-send form-send--centerMob form-send--mt">
                        <label class="registration__photo-wrap">
                            <div class="registration__photo">
                                <img src="{{ asset('public/frontend/img/svg/plus-black.svg') }}" class="user-plus" id="userPhoto" alt="">
                            </div>
                            <div class="form-send__text">
                                <span class="upload-start">Завантажте Ваше фото</span>
                                <span class="upload-change">Змінити фото</span>
                            </div>
                            <input type="file" id="upload">
                            <input type="text" id="upload-success" name="avatar" hidden>
                            <span>
                                <div class="error-custom">Формат фото: png, jpg <br> Не більше 5Mb</div>
                            </span>
                        </label>
                        <button type="submit" class="btn">ЗАВЕРШИТИ РЕЄСТРАЦІЮ</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- Popup -->
    <div class="popup cropper">
        <div class="popup__wrap">
            <div class="popup__close js-close-popup">
                <img src="{{ asset('public/frontend/img/svg/close.svg') }}" alt="">
            </div>
            <div id="upload-demo"></div>
            <a href="#" class="upload-result btn">Зберегти</a>
        </div>
    </div>
    <script>
        const region = [
            'Автономна Республіка Крим',
            'Вінницька область',
            'Волинська область',
            'Дніпропетровська область',
            'Донецька область',
            'Житомирська область',
            'Закарпатська область',
            'Запорізька область',
            'Івано-Франківська область',
            'Київська область',
            'Кіровоградська область',
            'Луганська область',
            'Львівська область',
            'Миколаївська область',
            'Одеська область',
            'Полтавська область',
            'Рівненська область',
            'Сумська область',
            'Тернопільська область',
            'Харківська область',
            'Херсонська область',
            'Хмельницька область',
            'Черкаська область',
            'Чернівецька область',
            'Чернігівська область',
        ];
        let citys = [];

        $.getJSON('../public/citys.json', (data) => {
            data.map((e) => {
                citys.push(e);
            });
        });

        $('.js-selectCurrent-1 input, .js-selectCurrent-2 input').on('keyup change', function () {
            $('.type-city').text('');
        });

        $('.register-step-3').on('submit', function (e) {
            e.preventDefault();
            $('.ajax-validate-error .error-step-3').html('');
            $('.registration__photo-wrap:not(.upload-success)').find('#upload-success').removeAttr('name');
            const activeGeo = $('.js-active-input');
            const activeGeoInput = $('.js-active-input input');
            const activeGeoValue = activeGeoInput.val();
            let validGeo = false;

            if (activeGeo.hasClass('js-selectCurrent-1')) {
                validGeo = citys.indexOf(activeGeoValue) !== -1;
            }

            if (activeGeo.hasClass('js-selectCurrent-2')) {
                validGeo = region.indexOf(activeGeoValue) !== -1;
            }

            if (!validGeo && activeGeoValue.length !== 0) {
                $('.type-city').text('Виберіть один з варіантів');
            }

            if ($(this).valid() && validGeo) {
                var formData = new FormData($(this)[0]);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/register-step-3',
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    data: formData,
                    beforeSend: function() {
                        $('.loader').addClass('active');
                    },
                    success: function (data) {
                        $('.loader').removeClass('active');
                        $('.registration__photo-wrap:not(.upload-success)').find('#upload-success').attr('name', 'avatar');
                        window.location = data;
                    },
                    error: function (data) {
                        $('.loader').removeClass('active');
                        $('.registration__photo-wrap:not(.upload-success)').find('#upload-success').attr('name', 'avatar');
                        var errors = data.responseJSON;
                        $('.register-step-3 .error').html('');
                        if (errors.errors.email) $('.type-email').text(errors.errors.email[0]);
                        if (errors.errors.name) $('.register-step-3 .type-name').text(errors.errors.name[0]);
                        if (errors.errors.surname) $('.register-step-3 .type-surname').text(errors.errors.surname[0]);
                        if (errors.errors.city) $('.register-step-3 .type-city').text(errors.errors.city[0]);
                        if (errors.errors.role) $('.register-step-3 .type-role').text(errors.errors.role[0]);
                        if (errors.errors.password) $('.register-step-3 .type-password').text(errors.errors.password[0]);
                        if (errors.errors.birthday) $('.register-step-3 .type-birthday').text(errors.errors.birthday[0]);
                    }
                });
            }
        });

    </script>

@endsection
