</div>
</div>

{{-- Footer --}}
<footer class="footer">
    <div class="container">
        <div class="footer__card footer__card--custom">
            <a href="/" class="logo">
                <img src="{{ asset('public/frontend/img/svg/logo-w.svg') }}" alt="">
            </a>
            <div class="footer__copyright">
                <p>Copyright 2020. Всі права захищені.</p>
            </div>
            <div class="footer__pay">
                <img src="{{ asset('public/frontend/img/Mastercard-logo.png') }}" alt="">
                <img src="{{ asset('public/frontend/img/Visa.png') }}" alt="">
            </div>
        </div>
        <ul class="footer__nav">
            <li><a href="/support">Допомога і Зворотній зв'язок</a></li>
            <li><a href="/platni-poslugi">Платні послуги</a></li>
            <li><a href="/yak-pratsyuvati-z-sistemoyu">Як працювати з системою?</a></li>
        </ul>
        <ul class="footer__nav">
            <li><a href="/search">Оголошення</a></li>
            <li><a href="/articles">Новини</a></li>
        </ul>
        <ul class="footer__nav">
            <li><a href="/umovi-vikoristannya">Умови використання</a></li>
            <li><a href="/politika-konfidentsiynosti">Політика конфіденційності</a></li>
        </ul>
        <div class="footer__card footer__card--right">
            <div class="footer__social">
                <p>Ми в соціальних мережах:</p>
                <ul class="social-nav">
                    <li>
                        <a href="https://www.facebook.com/ULAND-101028784636794/">
                            <img src="{{ asset('public/frontend/img/svg/fb.svg') }}" alt="">
                        </a>
                    </li>
                </ul>
            </div>
            <a href="mailto:info@uland.com.ua" class="footer__mail">
                <img src="{{ asset('public/frontend/img/svg/mail-g.svg') }}" alt="">
                <p>info@uland.com.ua</p>
            </a>
            <div class="footer__development">
                <p>Розробка і підтримка від <a href="https://octarine.com.ua/" target="_blank">Octarine Studio</a></p>
            </div>
        </div>
    </div>
</footer>
<!-- Popup -->
<div class="popup favorites-login">
	<div class="popup__wrap">
		<div class="popup__close js-close-popup">
			<img src="{{ asset('public/frontend/img/svg/close.svg') }}" alt="">
		</div>
		<div class="title title--center title--mini">
			<p>Подади оголошення</p>
		</div>
		<div class="text text--mtMedium text--center">
			<p>Щоб подати оголошення, потрібно авторизуватись або зареєструватись</p>
		</div>
		<div class="helper helper--mt helper--center">
			<a href="/customer/login" class="btn btn--same">авторизуватись</a>
			<a href="/register-step-1" class="btn btn--same btn--ml-20">зареєструватись</a>
		</div>
	</div>
</div>
<div class="popup favorites-login-second">
	<div class="popup__wrap">
		<div class="popup__close js-close-popup">
			<img src="{{ asset('public/frontend/img/svg/close.svg') }}" alt="">
		</div>
		<div class="title title--center title--mini">
			<p>Ви не авторизовані</p>
		</div>
		<div class="text text--mtMedium text--center">
			<p>Щоб додати оголошення в обрані потрібно авторизуватись або зареєструватись</p>
		</div>
		<div class="helper helper--mt helper--center">
			<a href="/customer/login" class="btn btn--same">авторизуватись</a>
			<a href="/register-step-1" class="btn btn--same btn--ml-20">зареєструватись</a>
		</div>
	</div>
</div>
<div class="popup close-advert">
	<div class="popup__wrap">
		<div class="popup__close js-close-popup">
			<img src="{{ asset('public/frontend/img/svg/close.svg') }}" alt="">
		</div>
		<div class="title title--center title--mini">
			<p>Відключення оголошення</p>
		</div>
		<div class="text text--mtMedium text--center close-advert__sum">
			<span id="text-description">Вкажіть вартість, по якій Ви здали в оренду землю:</span>
			<input type="text" pattern="^[ 0-9]+$" id="soldPrice">
			<span>грн.</span>
		</div>
		<div class="helper helper--mt helper--center">
			<a href="#" class="btn btn--custom btn--same" id="close-advert-btn">Не сдана</a>
			<a href="#" class="btn btn--same btn--ml-20" id="close-advert-btn-ok">Земля сдана</a>
		</div>
	</div>
</div>
<div class="popup close-advert-end">
	<div class="popup__wrap popup__wrap--min">
		<div class="title title--center title--mini">
			<p>Оголошення успішно відключено і відкріплено від Вашого профілю!</p>
		</div>
	</div>
</div>
{{-- Loader --}}
<div class="loader">
    <div class="loader__item"></div>
</div>

{{-- Add advert --}}
<a href="/customer/login" class="advert-action btn">
    <p>Подати оголошення</p>
    <img src="{{ asset('public/frontend/img/svg/plus-black.svg') }}" alt="">
</a>
<script src="{{ asset('public/frontend/js/app.js') }}"></script>

<script>
    window.Echo.channel('customer_r')
        .listen('CustomerRegistered', function(e) {
            Notification.requestPermission( function(permission) {
                var notification = new Notification('Новий Юзер на сайті', {
                    body: 'До нашого сервісу приєднався ще один чоловік! Вітаємо', // content for the alert
                    icon: "https://uland.ua/public/storage/Avatar/"+e.avatar // optional image url
                });
            });
        });
</script>
{{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>--}}

{{--<script src="{{ asset('public/frontend/js/ajax.js') }}"></script>--}}
{{--<script async src="https://static.addtoany.com/menu/page.js"></script>--}}
<script type="text/javascript" src="{{ asset('public/frontend/js/library.min.js') }}"></script>
<script type="text/javascript" src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBT9V7PtXQCVT5wFlkamJueRXOLgYSnNQk&language=uk"></script>
<script type="text/javascript" src="{{ asset('public/frontend/js/main.min.js') }}"></script>
<script type="text/javascript">
    var isInIFrame = (window.location != window.parent.location);
    if(isInIFrame==true){
        document.querySelector('header').style.display = "none";
        document.querySelector('footer').style.display = "none";
        document.getElementById('come-back').style.display = "none";
    }
</script>
</body>
</html>
