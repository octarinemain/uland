<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $title }}</title>
    @if(isset($meta_description))
        <meta name="description" content="{{ $meta_description }}"/>
    @endif
    @if(isset($meta_key))
        <meta name="keywords" content="{{ $meta_key }}" />
    @endif
    <link rel="shortcut icon" href="{{ asset('public/frontend/img/favicon/logoblack.ico') }}" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('public/frontend/css/library.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/frontend/css/main.min.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/proj4js/2.5.0/proj4-src.js"></script>
    <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @if(Auth::guard('customer')->check())
        <meta name="customer" content="">
    @endif
    {{--{!! NoCaptcha::renderJs() !!}--}}
<!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-TV6G2HM');</script>
    <!-- End Google Tag Manager -->
    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '305762463615804');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1"
             src="https://www.facebook.com/tr?id=305762463615804&ev=PageView
&noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TV6G2HM" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="height-helper">
    <div class="content-wrap">
        {{-- Header --}}
        <header class="header">
            <div class="container">
                <div class="header__burger">
                    <span></span>
                    <span></span>
                </div>
                <a href="/" class="logo">
                    <img src="{{ asset('public/frontend/img/svg/logo.svg') }}" alt="">
                </a>
                <nav class="header__nav">
                    <a href="/search">Оголошення</a>
                    <a href="/search-agent">Агенти</a>
                    <a href="/articles">Новини</a>
                    <a href="/yak-pratsyuvati-z-sistemoyu">Як працювати з системою?</a>
					<a href="@if(Auth::guard('customer')->check()) /customer/check-cadnum @else # @endif" class="button-link @if(!Auth::guard('customer')->check()) js-popup-button @endif" data-popupShow="favorites-login">Подати оголошення</a>
                </nav>
                <div class="header__personal">
                    @if(!Auth::guard('customer')->check())
                        <a href="/customer/login" class="header__login">
                            <div class="header__loginIcon">
                                <img class="icon-default" src="{{ asset('public/frontend/img/svg/Login.svg') }}" alt="">
                                <img class="icon-hover" src="{{ asset('public/frontend/img/svg/Login-hover.svg') }}"
                                     alt="">
                            </div>
                            <span>Увійти</span>
                        </a>
                    @else
                        <div class="header__user active">
                            {{--<a href="/" class="header__mailUser">--}}
                            {{--<img src="{{ asset('public/frontend/img/mail-ok.png') }}" alt="">--}}
                            {{--<span class="header__mailCalc">2</span>--}}
                            {{--</a>--}}
                            <div class="header__profile dropdown-main">
                                <div class="header__profile-wrap dropdown-active">
                                    <div class="header__userPhoto">
                                        @if(Auth::guard('customer')->user()->avatar != '')
                                            <img
                                                src="{{ asset('public/storage/Avatar/'.Auth::guard('customer')->user()->avatar) }}"
                                                alt="">
                                        @else
                                            <img src="{{ asset('public/frontend/img/no-avatar.png') }}" alt="">
                                        @endif
                                        {{--<span class="header__mailCalc">2</span>--}}
                                    </div>
                                    <div class="header__userName">
                                        <?php $surname = substr(Auth::guard('customer')->user()->surname, 0, 2); ?>
                                        <p>{{ Auth::guard('customer')->user()->name }} {{ $surname }}.</p>
                                    </div>
                                    <img class="arrow" src="{{ asset('public/frontend/img/svg/arrow-down.svg') }}"
                                         alt="">
                                </div>
                                <ul class="header__profileMenu dropdown">
                                    <li><a href="/customer/profile">Мій профіль</a></li>
                                    <li><a href="/customer/edit">Редагувати профіль</a></li>
                                    {{--<li><a href="#">Повідомлення</a></li>--}}
                                    <li><a href="/customer/favorites">Обрані</a></li>
                                    <li class="exit">
                                        <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                                            Вихід
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                              style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                    <img src="{{ asset('public/frontend/img/svg/arrow-black.svg') }}" alt=""
                                         class="exit-menu">
                                </ul>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </header>

@yield('content')
@extends('frontend.includes.footer')
