<div class="personCard personCard--main js-adaptive-person">
    <input hidden value="{{ $customer->id }}" class="customer_id" />
    <div class="personCard__roleWrap">
        <div class="personCard__role">
            <div class="text text--min">
                @if($customer->role == 'Я собственник')
                    <p>Власник</p>
                @else
                    <p>Агент</p>
                @endif
            </div>
        </div>
    </div>
    <div class="personCard__fullInfo">
        <div class="personCard__data">
            <a href="/customer/id={{ $customer->id }}" class="personCard__photo">
                @if($customer->avatar != '')
                    <img src="{{ asset('public/storage/Avatar/'.$customer->avatar) }}" alt="">
                @else
                    <img src="{{ asset('public/frontend/img/no-avatar.png') }}" alt="">
                @endif
            </a>
            <div class="personCard__otherData"></div>
            <div class="personCard__grade">
                <div class="personCard__gradeCard">
                    <div class="text text--big">
                        <p>{{ $adverts->total() }}</p>
                    </div>
                    <div class="text text--grayColor text--miniPlus">
                        <p>
                            <?php
                            plural_form(
                                $adverts->total(),
                                /* варианты написания для количества 1, 2 и 5 */
                                array('оголошення', 'оголошення', 'оголошень')
                            );
                            ?>
                        </p>
                    </div>
                </div>
                <div class="personCard__gradeCard">
                    <div class="text text--big">
                        <?php
                        function date_count($number, $after)
                        {
                            $cases = array(2, 0, 1, 1, 1, 2);
                            echo $after[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
                        }
                        ?>
                        <p><?php
                            $cDate = Carbon\Carbon::parse($customer->created_at);
                            echo $cDate->diffInDays();
                            ?></p>
                    </div>
                    <div class="text text--grayColor text--miniPlus">
                        <p><?php
                            date_count(
                                $cDate->diffInDays(),
                                /* варианты написания для количества 1, 2 и 5 */
                                array('добу', 'доби', 'діб')
                            );
                            ?> з нами</p>
                    </div>
                </div>
                {{--<div class="personCard__gradeCard">--}}
                {{--<div class="text text--big flex-cont align-center">--}}
                {{--<p>4.5</p>--}}
                {{--<img src="{{ asset('public/frontend/img/star2.png') }}" alt="">--}}
                {{--</div>--}}
                {{--<div class="text text--grayColor text--miniPlus">--}}
                {{--<p>рейтинг</p>--}}
                {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
        <div class="personCard__informWrap">
            <a href="/customer/id={{ $customer->id }}" class="personCard__name">
                <?php $customer->surname = substr($customer->surname, 0, 2); ?>
                <p>{{ $customer->name }} {{ $customer->surname }}.</p>
            </a>
            @if(Auth::guard('customer')->check())
                @if($customer->id == Auth::guard('customer')->user()->id)
                    <ul class="personCard__info">
                        <li>
                            <div class="personCard__icon">
                                <img src="{{ asset('public/frontend/img/svg/location-red.svg') }}" alt="">
                            </div>
                            <div class="text text--grayColor">
                                <p>{{ $customer->city }}</p>
                            </div>
                        </li>
                        <li>
                            <div class="personCard__icon">
                                <img src="{{ asset('public/frontend/img/svg/telephone.svg') }}" alt="">
                            </div>
                            <a href="tel:{{ $customer->phone }}" class="text">
                                <p>{{ $customer->phone }}</p>
                            </a>
                        </li>
                        <li class="personCard__infoError @if($customer->email_is_active == 0) active @endif">
                            <div class="personCard__icon">
                                <img src="{{ asset('public/frontend/img/svg/mail.svg') }}" alt="">
                            </div>
                            <a href="mailto:{{ $customer->email }}" class="text">
                                <p>{{ $customer->email }}</p>
                            </a>
                            <span>
                            <div class="error">Email не підтверджений</div>
                        </span>
                        </li>
                    </ul>
                @else
                    <ul class="personCard__info">
                        <li>
                            <div class="personCard__icon">
                                <img src="{{ asset('public/frontend/img/svg/location-red.svg') }}" alt="">
                            </div>
                            <div class="text text--grayColor">
                                <p>{{ $customer->city }}</p>
                            </div>
                        </li>
                    </ul>
                    <ul class="personCard__info js-contacts">
                        <li>
                            <div class="personCard__icon">
                                <img src="{{ asset('public/frontend/img/svg/telephone.svg') }}" alt="">
                            </div>
                            <a href="tel:" class="text short_phone_attr">
                                <p class="short_phone"></p>
                            </a>
                        </li>
                        <li class="personCard__infoError">
                            <div class="personCard__icon">
                                <img src="{{ asset('public/frontend/img/svg/mail.svg') }}" alt="">
                            </div>
                            <a href="mailto:" class="text short_email_attr">
                                <p class="short_email"></p>
                            </a>
                        </li>
                        <li class="text js-show-contacts">
                            <p class="text__link">Показати контакти</p>
                        </li>
                    </ul>
                @endif
            @else
                <ul class="personCard__info">
                    <li>
                        <div class="personCard__icon">
                            <img src="{{ asset('public/frontend/img/svg/location-red.svg') }}" alt="">
                        </div>
                        <div class="text text--grayColor">
                            <p>{{ $customer->city }}</p>
                        </div>
                    </li>
                </ul>
                <ul class="personCard__info js-contacts">
                    <li>
                        <div class="personCard__icon">
                            <img src="{{ asset('public/frontend/img/svg/telephone.svg') }}" alt="">
                        </div>
                        <a href="tel:{{ $customer->phone }}" class="text">
                            <p>{{ $customer->phone }}</p>
                        </a>
                    </li>
                    <li class="personCard__infoError">
                        <div class="personCard__icon">
                            <img src="{{ asset('public/frontend/img/svg/mail.svg') }}" alt="">
                        </div>
                        <a href="mailto:{{ $customer->email }}" class="text">
                            <p>{{ $customer->email }}</p>
                        </a>
                    </li>
                    <li class="text js-show-contacts">
                        <p class="text__link">Показати контакти</p>
                    </li>
                </ul>
            @endif

        </div>
    </div>
    @if(Auth::guard('customer')->check() && $customer->id == Auth::guard('customer')->user()->id)
        <div class="personCard__btn">
            <a href="/customer/edit" class="btn btn--custom btn--100">РЕДАГУВАТИ ДАНІ</a>
        {{--@if(Auth::guard('customer')->check() && $customer->id != Auth::guard('customer')->user()->id)--}}
            {{--<a href="/" class="btn btn--custom btn--100">ДОБАВИТЬ ОТЗЫВ</a>--}}
            {{--@endif--}}
        </div>
    @endif
</div>
