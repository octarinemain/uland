@extends('frontend.includes.header')

@section('content')
<section class="error-page section-dark">
    <div class="container">
        <div class="error-page__title">
            <p>404</p>
        </div>
        <div class="title title--sub title--center">
            <p>Сторінку не знайдено ...</p>
        </div>
        <div class="text text--center text--mtMin">
            <p>Сторінка, на яку Ви хотіли перейти, не знайдено.</p>
        </div>
        <div class="text text--center">
            <p>Можливо, введений адрес некоректний  або сторінка була видалена.</p>
        </div>
        <div class="form-send form-send--mt form-send--center">
            <a href="/" class="btn">ПОВЕРНУТИСЯ НА ГОЛОВНУ</a>
        </div>
    </div>
</section>
@endsection
