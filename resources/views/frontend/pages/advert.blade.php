@extends('frontend.includes.header')

@section('content')
    <section id="come-back" class="come-back dark-section">
        <div class="container">
            <div class="text">
                <a href="/search" class="text__link text__link--arrow">
                    <img class="hover-off" src="{{ asset('public/frontend/img/svg/arrow-back.svg') }}" alt="">
                    <img class="hover-on" src="{{ asset('public/frontend/img/svg/arrow-back-y.svg') }}" alt="">
                    <span>Повернутися до результатів пошуку</span>
                </a>
            </div>
        </div>
    </section>
    <section class="advert">
        <div class="container">
            <div class="advert__wrap">
                <div class="advert__main">
                    <div class="advert__top-wrap ads">
                        <div id="ads-wrap-first" class="ads__wrap">
                            <div class="advert__main-type" data-type="{{ $advert->type }}">
                                @if($advert->top != NULL && $advert->top != '0000-00-00 00:00:00')
                                    <div class="advert__allotment">
                                        <p>top</p>
                                    </div>
                                @endif
                                <div class="text text--bold text--grayColor text--ttu text--mini-10 advert__text-type">
                                    @if($advert->type == 'Продаж')
                                        <p>ПРОДАМ</p>
                                    @else
                                        <p>ЗДАМ В ОРЕНДУ</p>
                                    @endif
                                </div>
                            </div>
                            <div class="title title--sub title--mtMin">
                                <p>{{ $advert->region }}, {{ $advert->district }}</p>
                            </div>
                            <div class="advert__headInfo flex-cont justify-sb">
                                <div class="fresh-info fresh-info--advert">
                                    <div class="fresh-info__card">
                                        <div class="fresh-info__value">
                                            <p>{{ $advert->ga }}</p>
                                        </div>
                                        <div class="signature signature--mt">
                                            <p>загальна площа, Га</p>
                                        </div>
                                    </div>
                                    <div class="fresh-info__card">
                                        <div class="fresh-info__value">
                                            @if($advert->type == 'Продаж')
                                                <p>{{ $advert->ga }}</p>
                                            @else
                                                <p>{{ $advert->ga_to_sell }}</p>
                                            @endif
                                        </div>
                                        <div class="signature signature--mt">
                                            @if($advert->type == 'Продаж')
                                                <p>площа яка продається, Га</p>
                                            @else
                                                <p>площа яка сдається, Га</p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="fresh-info__card">
                                        <div class="fresh-info__value">
                                            <p><span class="js-price-num">{{ intval($advert->price_per_ga) }}</span> грн.</p>
                                        </div>
                                        <div class="signature signature--mt">
                                            <p>ціна за Га</p>
                                        </div>
                                    </div>
                                    <div class="fresh-info__card">
                                        <div class="fresh-info__value">
                                            <p><span class="js-price-num">{{ intval($advert->price) }}</span> грн.</p>
                                        </div>
                                        <div class="signature signature--mt">
                                            <p>загальна вартість</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <ul class="advert__listInfo">
                                <li>
                                    <div class="text text--bold">
                                        <p>Кадастровий номер:</p>
                                    </div>
                                    <div class="text text--light advert__cadastre-info">
                                        <p>{{ $advert->cadnum }}</p>
                                        <div class="advert__extra-card">
                                            <div class="extra-card">
                                                <img src="{{ asset('public/frontend/img/check.png') }}"
                                                     alt="">
                                                <div class="extra-card__inform">
                                                    <div class="text text--mini">
                                                        <p>Перевірено в держ гео кадастрі</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="text text--bold">
                                        <p>Цільове призначення:</p>
                                    </div>
                                    <div class="text text--light">
                                        <p>{{ $advert->purpose }}</p>
                                    </div>
                                </li>
                                {{--<li>--}}
                                {{--<div class="text text--bold">--}}
                                {{--<p>Какие права продаются:</p>--}}
                                {{--</div>--}}
                                {{--<div class="text text--light">--}}
                                {{--<p>{{ $advert->use_с }}</p>--}}
                                {{--</div>--}}
                                {{--</li>--}}
                            </ul>
                        </div>
                        <div id="ads-wrap-last" class="ads__wrap">
                            @if(Auth::guard('customer')->check())
                                @if($advert->customer_id == Auth::guard('customer')->user()->id || $advert->agent_id == Auth::guard('customer')->user()->id)
                                    <div id="ads-main-info" class="ads__main-info">
                                        <div class="ads__top-section">
                                            @if($advert->search_status == 'Активно')
                                                <div class="text text--ttu text--bold text--mini active-state ads__status">
                                                    <p>АКТИВНО</p>
                                                </div>
                                            @elseif($advert->search_status == 'Відключено')
                                                <div class="text text--ttu text--bold text--mini  text--red-second ads__status">
                                                    <p>ВІДКЛЮЧЕНО</p>
                                                </div>
                                            @elseif($advert->search_status == 'На модерації')
                                                <div class="text text--ttu text--bold text--mini moderation-state ads__status">
                                                    <p>НА МОДЕРАЦІЇ</p>
                                                </div>
                                            @endif
                                            <div class="ads__menu dropdown-main">
                                                <div class="ads__dots dropdown-active">
                                                    <div class="ads__dot"></div>
                                                    <div class="ads__dot"></div>
                                                    <div class="ads__dot"></div>
                                                </div>
                                                <ul class="ads__menuDrop dropdown dropdown--dark">
                                                    @if(Auth::guard('customer')->user()->role == 'Я собственник' && $advert->agent_id == 0)
                                                        <li><a href="/customer/edit-advert/id={{ $advert->id }}">Редагувати</a>
                                                        </li>
                                                    @endif
                                                    @if($advert->agent_id == Auth::guard('customer')->user()->id && $advert->agent_id != 0)
                                                        <li><a href="/customer/edit-advert/id={{ $advert->id }}">Редагувати</a>
                                                        </li>
                                                    @endif

                                                    @if($advert->search_status != 'На модерації')

                                                        @if($advert->customer_id == Auth::guard('customer')->user()->id)
                                                            <li><a class="event-advert"
                                                                   href="/customer/unfasten-owner/id={{ $advert->id }}">
                                                                    Я не власник, відкріпити
                                                                </a></li>
                                                        @endif

                                                        @if($advert->agent_id != 0 && $advert->customer_id == Auth::guard('customer')->user()->id)
                                                            <li><a class="event-advert"
                                                                   href="/customer/unfasten-agent/id={{ $advert->id }}">Відв'язати агента</a></li>
                                                        @endif
                                                    @endif
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="ads__bottom-section">
                                            @if($advert->search_status != 'На модерації')
                                                @if($advert->search_status == 'Відключено')
                                                    @if(Auth::guard('customer')->user()->role == 'Я собственник' && $advert->agent_id == 0 ||
                                                    $advert->agent_id == Auth::guard('customer')->user()->id && $advert->agent_id != 0)
                                                        <a href="#" class="btn btn--activate" data-id="{{ $advert->id }}">Активувати</a>
                                                    @else
                                                        <a href="#" class="btn btn--disable">Активувати</a>
                                                    @endif
                                                @else
                                                    {{--Блок для кнопки закрыть где будет ПОПАП с инпутом цены и две кнопки
                                                        и у каждой кнопки свой аякс будет--}}
                                                    @if(Auth::guard('customer')->user()->role == 'Я собственник' && $advert->agent_id == 0 ||
                                                    $advert->agent_id == Auth::guard('customer')->user()->id && $advert->agent_id != 0)
                                                        <a href="#" class="btn btn--deactivate js-popup-button" data-id="{{ $advert->id }}" data-type="{{ $advert->type }}" data-popupShow="close-advert">Відключити</a>
                                                    @else
                                                        <a href="#" class="btn btn--disable">Відключити</a>
                                                    @endif
                                                @endif
                                            @else
                                                <a href="#" class="btn btn--disable">Відключити</a>
                                            @endif
                                            {{--этот блок идёт если обьява не на модерации, то тогда мы видим норм кнопки активировать\закрыть обьявление--}}
                                            @if($advert->search_status != 'На модерації')
                                                @if(Auth::guard('customer')->user()->role == 'Я собственник' && $advert->agent_id == 0 ||
                                                $advert->agent_id == Auth::guard('customer')->user()->id && $advert->agent_id != 0)
                                                    <a href="/customer/advertise/id={{ $advert->id }}" class="btn btn--publicity">Рекламувати</a>
                                                @else
                                                    <a href="#" class="btn btn--disable" disable>Рекламувати</a>
                                                @endif
                                            @else
                                                <a href="#" class="btn btn--disable" disable>Рекламувати</a>
                                            @endif
                                        </div>
                                    </div>
                                @endif
                            @endif
                            <div id="ads-other-info" class="ads__otherInfo">
                                <div id="date-info" class="advert__otherInfoCard advert__otherInfoCard--date">
                                    <div class="text text--miniPlus text--grayColor">
                                        <p>
                                            <span>Опубліковано: </span>
                                            <span>{{ date('d.m.Y',strtotime($advert->created_at)) }}</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="advert__otherInfoCard advert__otherInfoCard--extra flex-cont align-center">
                                    @if($advert->discount == 1)
                                        <div class="extra-card">
                                            <img src="{{ asset('public/frontend/img/svg/Auction.svg') }}"
                                                 alt="">
                                            <div class="extra-card__inform">
                                                <div class="text text--mini">
                                                    <p>Можливий торг</p>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if($advert->attr_5 != '')
                                        <div class="extra-card">
                                            <img src="{{ asset('public/frontend/img/svg/house.svg') }}" alt="">
                                            <div class="extra-card__inform">
                                                <div class="text text--mini">
                                                    <p>Є споруди</p>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    <div
                                            class="favoritesButton favoritesButton--ml js-favorite @if($advert->favorite == 1) active @endif"
                                            data-advert-id="{{ $advert->id }}">
                                        <i class="icon-star-outline">
                                            <img src="{{ asset('public/frontend/img/svg/star-outline.svg') }}" alt="">
                                        </i>
                                        <i class="icon-star">
                                            <img src="{{ asset('public/frontend/img/svg/star.svg') }}" alt="">
                                        </i>
                                    </div>
                                </div>
                                <div class="advert__otherInfoCard">
                                    <div class="viewers">
                                        <div class="viewers__icon">
                                            <img src="{{ asset('public/frontend/img/svg/eye.svg') }}" alt="">
                                        </div>
                                        <div class="viewers__value">
                                            <div class="text text--gray text--normal">
                                                <p>{{ $advert->views }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="advert__swiper">
                        <div class="swiper-wrapper">
                            @foreach($advert->images as $image)
                                <div class="swiper-slide">
                                    <img src="{{ asset('public/storage/AdvertPhoto/'.$image->image) }}" alt="">
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="slider-control slider-control--mt slider-control--hover js-slider-control">
                        <img class="prev revert" src="{{ asset('public/frontend/img/svg/arrow-black.svg') }}" alt="">
                        <img class="next" src="{{ asset('public/frontend/img/svg/arrow-black.svg') }}" alt="">
                    </div>
                    <ul class="advert__listInfo advert__listInfo--column">
                        @if($advert->attr_1 != '')
                            <li>
                                <div class="text text--bold">
                                    <p>Електрика</p>
                                </div>
                                <div class="text text--light">
                                    <p>
                                        {{ $advert->attr_1 }}
                                    </p>
                                </div>
                            </li>
                        @endif
                        @if($advert->attr_2 != '')
                            <li>
                                <div class="text text--bold">
                                    <p>Газ</p>
                                </div>
                                <div class="text text--light">
                                    <p>
                                        {{ $advert->attr_2 }}
                                    </p>
                                </div>
                            </li>
                        @endif
                        @if($advert->attr_3 != '')
                            <li>
                                <div class="text text--bold">
                                    <p>Вода, можливість поливу</p>
                                </div>
                                <div class="text text--light">
                                    <p>
                                        {{ $advert->attr_3 }}
                                    </p>
                                </div>
                            </li>
                        @endif
                        @if($advert->attr_4 != '')
                            <li>
                                <div class="text text--bold">
                                    <p>Техніка, обладнання</p>
                                </div>
                                <div class="text text--light">
                                    <p>
                                        {{ $advert->attr_4 }}
                                    </p>
                                </div>
                            </li>
                        @endif
                        @if($advert->attr_5 != '')
                            <li>
                                <div class="text text--bold">
                                    <p>Будівлі, споруди</p>
                                </div>
                                <div class="text text--light">
                                    <p>
                                        {{ $advert->attr_5 }}
                                    </p>
                                </div>
                            </li>
                        @endif
                        @if($advert->comment_1 != '')
                            <li>
                                <div class="text text--bold">
                                    <p>Як сьогодні використовується об'єкт?</p>
                                </div>
                                <div class="text text--light">
                                    <p>
                                        {{ $advert->comment_1 }}
                                    </p>
                                </div>
                            </li>
                        @endif
                        @if($advert->comment_2 != '')
                            <li>
                                <div class="text text--bold">
                                    <p>інші активи</p>
                                </div>
                                <div class="text text--light">
                                    <p>
                                        {{ $advert->comment_2 }}
                                    </p>
                                </div>
                            </li>
                        @endif
                        @if($advert->comment_3 != '')
                            <li>
                                <div class="text text--bold">
                                    <p>Додаткова інформація</p>
                                </div>
                                <div class="text text--light">
                                    <p>
                                        {{ $advert->comment_3 }}
                                    </p>
                                </div>
                            </li>
                        @endif
                    </ul>
                    <div class="advert__map" id="advert__map" data-y="{{ $advert->x }}" data-x="{{ $advert->y }}"></div>
                </div>
                @if($advert->agent_id != 0)
                    <?php $customer = $advert->agent_customer; ?>
                @else
                    <?php $customer = $advert->customer; ?>
                @endif

                @if($customer->role == 'Я агент по продаже')
                    <?php
                    $adverts = App\Models\Advert::where('agent_id', $customer->id)->whereNotIn('search_status', ['На модерації', 'Відключено'])->paginate(50);
                    ?>
                @else
                    <?php
                    $adverts = App\Models\Advert::where('customer_id', $customer->id)->whereNotIn('search_status', ['На модерації', 'Відключено'])->paginate(50);
                    ?>
                @endif

                <?php
                function plural_form($number, $after)
                {
                    $cases = array(2, 0, 1, 1, 1, 2);
                    echo $after[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
                }
                ?>
                @include('frontend.layouts.customer-card')
            </div>
            @if(count($similar_adverts))
                <div class="similar-listings">
                    <div class="title title--sub">
                        <p>Схожі оголошення</p>
                        <div class="signature signature--mtBig">
                            <p>знайдено <?php echo count($similar_adverts) . ' ';
                                plural_form(
                                    count($similar_adverts),
                                    /* варианты написания для количества 1, 2 и 5 */
                                    array('оголошення', 'оголошення', 'оголошень')
                                );
                                ?></p>
                        </div>
                    </div>
                    <div class="ads ads--long">
                        <div class="ads__wrap">
                            @foreach($similar_adverts as $similar_advert)
                                <div class="ads__cardContent @if($similar_advert->top != NULL && $similar_advert->top != '0000-00-00 00:00:00') active @endif @if($similar_advert->red != NULL && $similar_advert->red != '0000-00-00 00:00:00') allotment @endif">
                                    <a href="/advert/id={{ $similar_advert->id }}" class="ads__card">
                                        <div class="ads__cardWrap">
                                            <div class="ads__allotment">
                                                <p>top</p>
                                            </div>
                                            <div class="ads__addressInfoWrap">
                                                <div class="ads__addressInfo">
                                                    <div class="ads__section">
                                                        <div class="text text--gray text--normal">
                                                            <p>Регіон</p>
                                                        </div>
                                                        <div class="text text--black">
                                                            <p>{{ $similar_advert->region }}</p>
                                                        </div>
                                                    </div>
                                                    <div class="ads__section">
                                                        <div class="text text--gray text--normal">
                                                            <p>Район</p>
                                                        </div>
                                                        <div class="text text--black">
                                                            <p>{{ $similar_advert->district }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ads__section">
                                                <div class="text text--gray text--normal">
                                                    <p>Площа, Га</p>
                                                </div>
                                                <div class="text text--black">
                                                    @if($similar_advert->type == 'Продаж')
                                                        <p>{{ $similar_advert->ga}}</p>
                                                    @else
                                                        <p>{{ $similar_advert->ga_to_sell }}</p>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="ads__priceInfo">
                                                <div class="ads__section">
                                                    <div class="text text--gray text--normal">
                                                        <p>Ціна за 1 Га</p>
                                                    </div>
                                                    <div class="text text--black">
                                                        <p><span class="js-price-num">{{ $similar_advert->price_per_ga }}</span>
                                                            грн.</p>
                                                    </div>
                                                </div>
                                                <div class="ads__section">
                                                    <div class="text text--gray text--normal">
                                                        <p>Ціна за все</p>
                                                    </div>
                                                    <div class="text text--black">
                                                        <p><span class="js-price-num">{{ $similar_advert->price }}</span> грн.
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="ads__section ads__section--extra flex-cont">
                                                    @if($similar_advert->discount == 1)
                                                        <div class="extra-card">
                                                            <img
                                                                    src="{{ asset('public/frontend/img/svg/Auction.svg') }}"
                                                                    alt="">
                                                            <div class="extra-card__inform">
                                                                <div class="text text--mini">
                                                                    <p>Можливий торг</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    @if($similar_advert->attr_5 != '')
                                                        <div class="extra-card">
                                                            <img src="{{ asset('public/frontend/img/svg/house.svg') }}"
                                                                 alt="">
                                                            <div class="extra-card__inform">
                                                                <div class="text text--mini">
                                                                    <p>Є споруди</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    <div class="viewers">
                                                        <div class="viewers__icon">
                                                            <img src="{{ asset('public/frontend/img/svg/eye.svg') }}"
                                                                 alt="">
                                                        </div>
                                                        <div class="viewers__value">
                                                            <div class="text text--gray text--normal">
                                                                <p>@if($similar_advert->views != 0) {{ $similar_advert->views }} @else
                                                                        0 @endif</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="ads__section ads__section--date">
                                                    <div class="text text--gray text--normal">
                                                        <p>Дата публікації</p>
                                                    </div>
                                                    <div class="text text--black">
                                                        <p>{{ date('d.m.Y',strtotime($similar_advert->created_at)) }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="ads__section ads__section--star js-favorite"
                                         data-advert-id="{{ $similar_advert->id }}">
                                        <i class="icon-star-outline">
                                            <img src="{{ asset('public/frontend/img/svg/star-outline.svg') }}" alt="">
                                        </i>
                                        <i class="icon-star">
                                            <img src="{{ asset('public/frontend/img/svg/star.svg') }}" alt="">
                                        </i>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </section>
    {{-- popup --}}
    <div class="popup swiperBig">
        <div class="popup__wrap popup__wrap--big">
            <div class="popup__advert-swiper">
                <div class="swiper-wrapper">
                    @foreach($advert->images as $image)
                        <div class="swiper-slide">
                            <div class="popup__advert-wrap">
                                <div class="popup__close js-close-popup">
                                    <img src="{{ asset('public/frontend/img/svg/close.svg') }}" alt="">
                                </div>
                                <img src="{{ asset('public/storage/AdvertPhoto/'.$image->image) }}" alt="">
                                <div class="popup__advert-control">
                                    <div class="popup__advert-controlCard prev">
                                        <img class="revert" src="{{ asset('public/frontend/img/svg/arrow-black.svg') }}"
                                             alt="">
                                    </div>
                                    <div class="popup__advert-controlCard next">
                                        <img src="{{ asset('public/frontend/img/svg/arrow-black.svg') }}" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="popup favorites-login">
        <div class="popup__wrap">
            <div class="popup__close js-close-popup">
                <img src="{{ asset('public/frontend/img/svg/close.svg') }}" alt="">
            </div>
            <div class="title title--center title--mini">
                <p>Ви не авторизовані</p>
            </div>
            <div class="text text--mtMedium text--center">
                <p>Щоб додати оголошення в обрані потрібно
                    <a href="/customer/login">увійти</a>
                    або
                    <a href="/register-step-1">зареєструватися</a>
                </p>
            </div>
        </div>
    </div>
@endsection