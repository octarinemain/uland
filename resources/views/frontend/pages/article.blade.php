@extends('frontend.includes.header')
<meta property="og:title" content="{{ $article->title }}"/>
<meta property="og:description" content="{{ $article->small_description }}"/>
<meta property="og:image" content="{{ asset('public/storage/Article/'.$article->preview) }}">
<meta property="og:image:secure_url" content="{{ asset('public/storage/Article/'.$article->preview) }}">

@section('content')
    <section class="come-back dark-section">
        <div class="container">
            <div class="text">
                <a href="/" class="text__link text__link--arrow">
                    <img class="hover-off" src="{{ asset('public/frontend/img/svg/arrow-back.svg') }}" alt="">
                    <img class="hover-on" src="{{ asset('public/frontend/img/svg/arrow-back-y.svg') }}" alt="">
                    <span>Повернутися на головну</span>
                </a>
            </div>
        </div>
    </section>
    <section class="single-news">
        <div class="container container--min">
            <div class="title title--sub title--center">
                <h1>{{ $article->title }}</h1>
            </div>
            <div class="text text--gray text--op text--center text--mtMini">
                <p>{{ date('d-m-Y', strtotime($article->created_at)) }}</p>
            </div>
            <div class="single-news__wrap">
                <div class="share">
                    <div class="share__icon">
                        <a href="https://www.facebook.com/sharer/sharer.php?u={{URL::to('/'.$article->slug)}}"
                           class="a2a_button_facebook" target="_blank">
                            <img class="single-news__photo" src="{{ asset('public/frontend/img/svg/share-fb.svg') }}"
                                 alt="">
                        </a>
                        <a href="https://twitter.com/intent/tweet?text={{ $article->title }} {{URL::to('/'.$article->slug)}}"
                           class="a2a_button_twitter twitter-share-button" target="_blank">
                            <img class="single-news__photo" src="{{ asset('public/frontend/img/svg/share-tw.svg') }}"
                                 alt="">
                        </a>
                        <a href="https://plus.google.com/share?url={{URL::to('/'.$article->slug)}}"
                           class="a2a_button_google_plus" target="_blank">
                            <img class="single-news__photo" src="{{ asset('public/frontend/img/svg/share-g+.svg') }}"
                                 alt="">
                        </a>
                    </div>
                    <div class="text">
                        <p>Поділитися:</p>
                    </div>
                </div>
                <div class="single-news__container">
                    <div class="single-news__showcase">
                        <img class="single-news__photo" src="{{ asset('public/storage/Article/'.$article->preview) }}" alt="">
                    </div>
                    <div class="single-news__content visual-editor">
                      {!! $article->description !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
