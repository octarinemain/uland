@extends('frontend.includes.header')

@section('content')
    <section class="come-back dark-section">
        <div class="container">
            <div class="text">
                <a href="/" class="text__link text__link--arrow">
                    <img class="hover-off" src="{{ asset('public/frontend/img/svg/arrow-back.svg') }}" alt="">
                    <img class="hover-on" src="{{ asset('public/frontend/img/svg/arrow-back-y.svg') }}" alt="">
                    <span>Повернутися на головну</span>
                </a>
            </div>
        </div>
    </section>
    <section class="news-section news-section--paddingCustom">
        <div class="container">
            <div class="title title--sub">
                <p>Новини</p>
            </div>
            <div class="news-section__wrap">
                @foreach($articles as $article)
                    <div class="news-section__card">
                    <div class="news-section__date">
                        <p>{{ date('d-m-Y', strtotime($article->created_at)) }}</p>
                    </div>
                    <a href="/{{ $article->slug }}" class="news-section__title">{{ $article->title }}</a>
                    <div class="news-section__content">
                        <p>
                            {{ $article->small_description }}
                        </p>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
