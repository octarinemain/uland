@extends('frontend.includes.header')

@section('content')
    <section class="come-back dark-section">
        <div class="container">
            <div class="text">
                <a href="/" class="text__link text__link--arrow">
                    <img class="hover-off" src="{{ asset('public/frontend/img/svg/arrow-back.svg') }}" alt="">
                    <img class="hover-on" src="{{ asset('public/frontend/img/svg/arrow-back-y.svg') }}" alt="">
                    <span>Повернутися на головну</span>
                </a>
            </div>
        </div>
    </section>
    <section class="single-page">
        <div class="container container--min">
            <div class="title title--sub title--center">
                <h1>{{ $page->title }}</h1>
            </div>
            <div class="single-page__content">
                <div class="visual-editor">
                    {!! $page->description !!}
                </div>
            </div>
        </div>
    </section>
@endsection
