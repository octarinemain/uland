@extends('frontend.includes.header')

@section('content')
    <section class="come-back dark-section">
        <div class="container">
            <div class="text">
                <a href="/" class="text__link text__link--arrow">
                    <img class="hover-off" src="{{ asset('public/frontend/img/svg/arrow-back.svg') }}" alt="">
                    <img class="hover-on" src="{{ asset('public/frontend/img/svg/arrow-back-y.svg') }}" alt="">
                    <span>Повернутися на головну</span>
                </a>
            </div>
        </div>
    </section>
    <section class="profile">
        <div class="container">
            <div class="profile__wrap">
                <div class="profile__main">
                    <?php
                    function plural_form($number, $after)
                    {
                        $cases = array(2, 0, 1, 1, 1, 2);
                        echo $after[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
                    }
                    ?>

                    @if(count($adverts) != 0)
                        <div class="ads">
                            <div class="title title--sub flex-cont align-center">
                                <p>Оголошення</p>
                                <div class="signature signature--ml">
                                    <p>
                                        {{ $adverts->total() }}
                                        <?php
                                        plural_form(
                                            $adverts->total(),
                                            /* варианты написания для количества 1, 2 и 5 */
                                            array('оголошення', 'оголошення', 'оголошень')
                                        );
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <div class="ads__wrap">
                                @foreach($adverts as $advert)
                                    <div class="ads__cardContent @if($advert->top != NULL && $advert->top != '0000-00-00 00:00:00') active @endif @if($advert->red != NULL && $advert->red != '0000-00-00 00:00:00') allotment @endif">
                                        <a href="/advert/id={{ $advert->id }}" class="ads__card">
                                            <div class="ads__allotment">
                                                <p>top</p>
                                            </div>
                                            <div class="ads__type">
                                                <div class="text text--ttu text--blue text--bold text--mini-10">
                                                    <p>{{ $advert->type }}</p>
                                                </div>
                                            </div>
                                            <div class="ads__cardWrap">
                                                <div class="ads__addressInfoWrap">
                                                    <div class="ads__addressInfo">
                                                        <div class="ads__section">
                                                            <div class="text text--gray text--normal">
                                                                <p>Регіон</p>
                                                            </div>
                                                            <div class="text text--black">
                                                                <p>{{ $advert->region }}</p>
                                                            </div>
                                                        </div>
                                                        <div class="ads__section">
                                                            <div class="text text--gray text--normal">
                                                                <p>Район</p>
                                                            </div>
                                                            <div class="text text--black">
                                                                <p>{{ $advert->district }}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="ads__priceInfo">
                                                    <div class="ads__section">
                                                        <div class="text text--gray text--normal">
                                                            <p>Площа, Га</p>
                                                        </div>
                                                        <div class="text text--black">
                                                            @if($advert->type == 'Продаж')
                                                                <p>{{ $advert->ga}}</p>
                                                            @else
                                                                <p>{{ $advert->ga_to_sell }}</p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="ads__section">
                                                        <div class="text text--gray text--normal">
                                                            <p>Ціна за 1 Га</p>
                                                        </div>
                                                        <div class="text text--black">
                                                            <p><span class="js-price-num">{{ $advert->price_per_ga }}</span> грн.</p>
                                                        </div>
                                                    </div>
                                                    <div class="ads__section">
                                                        <div class="text text--gray text--normal">
                                                            <p>Ціна за все</p>
                                                        </div>
                                                        <div class="text text--black">
                                                            <p><span class="js-price-num">{{ $advert->price }}</span> грн.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="ads__otherInfo">
                                                    <div class="ads__section ads__section--extra flex-cont">
                                                        @if($advert->discount == 1)
                                                            <div class="extra-card">
                                                            <img src="{{ asset('public/frontend/img/svg/Auction.svg') }}"
                                                                 alt="">
                                                            <div class="extra-card__inform">
                                                                <div class="text text--mini">
                                                                    <p>Можливий торг</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @if($advert->attr_5 != '')
                                                            <div class="extra-card">
                                                                <img src="{{ asset('public/frontend/img/svg/house.svg') }}"
                                                                     alt="">
                                                                <div class="extra-card__inform">
                                                                    <div class="text text--mini">
                                                                        <p>є споруди</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                        <div class="viewers viewers--ml">
                                                            <div class="viewers__icon">
                                                                <img src="{{ asset('public/frontend/img/svg/eye.svg') }}"
                                                                     alt="">
                                                            </div>
                                                            <div class="viewers__value">
                                                                <div class="text text--gray text--normal">
                                                                    <p>@if($advert->views != 0) {{ $advert->views }} @else
                                                                            0 @endif</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="ads__section">
                                                        <div class="text text--gray text--normal">
                                                            <p>Дата публікації</p>
                                                        </div>
                                                        <div class="text text--black">
                                                            <p>{{ date('d.m.Y',strtotime($advert->created_at)) }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="ads__clearSection"></div>
                                            </div>
                                        </a>
                                        <div class="ads__otherFunction">
                                            <div class="ads__section ads__section--star js-favorite @if($advert->favorite == 1) active @endif" data-advert-id="{{ $advert->id }}">
                                                <i class="icon-star-outline">
                                                    <img src="{{ asset('public/frontend/img/svg/star-outline.svg') }}" alt="">
                                                </i>
                                                <i class="icon-star">
                                                    <img src="{{ asset('public/frontend/img/svg/star.svg') }}" alt="">
                                                </i>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                @include('frontend.layouts.pagination')
                            </div>
                        </div>
                    @else
                        <div class="ads">
                            <div class="title title--sub">
                                <p>Всі оголошення</p>
                            </div>
                            <div class="text text--mtMedium">
                                <p>У даного користувача немає оголошень.</a>.
                                </p>
                            </div>
                        </div>
                    @endif
                </div>
                @include('frontend.layouts.customer-card')
            </div>
        </div>
    </section>
@endsection
