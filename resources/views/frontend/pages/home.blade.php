@extends('frontend.includes.header')

@section('content')
    <section class="callboard get-api-region">
        <div class="container">
            <div class="callboard__main">
                <div class="callboard__search">
                    <div class="text text--ttu text--bold text--miniPlus">
                        <p>ДОШКА ОГОЛОШЕНЬ</p>
                    </div>
                    <div class="title">
                        <h1>Купівля, продаж і оренда землі</h1>
                    </div>
                    <div class="callboard__description">
                        <p>
                            За допомогою сервісу uland.com.ua, ви можете купити або продати, зняти або
                            здати в оренду будь-які земельні ділянки.
                            Особлива система пошуку по кадастровим номерам, допоможе вам швидко знайти, чи подати
                            оголошення.
                        </p>
                    </div>
                    <form method="POST" class="callboard__form" action="{{ route('search-home.submit') }}">
                        {{ csrf_field() }}
                        <div class="callboard__select">
                            <label class="radio checked">
                                <input type="radio" value="Оренда" name="type" id="rent" checked="">
                                <div class="radio__text">Оренда</div>
                            </label>
                            <label class="radio">
                                <input type="radio" value="Продаж" name="type" id="sale">
                                <div class="radio__text">Продаж</div>
                            </label>
                        </div>
                        <div class="callboard__data">
                            <div class="callboard__input">
                                <div class="callboard__region">
                                    <img src="{{ asset('public/frontend/img/svg/search.svg') }}" alt="">
                                    <div class="input-group__clear">
                                        <img src="{{ asset('public/frontend/img/svg/close.svg') }}" alt="">
                                    </div>
                                    <input
                                        type="text"
                                        placeholder="Введіть область або кадастровий номер"
                                        class="searchRegion"
                                        id="searchRegion"
                                        name="row"
                                        autocomplete="new-password"
                                    >
                                    <input type="text" class="search_type" name="s_city" value="" hidden>
                                    <input type="text" class="search_type" name="s_district" value="" hidden>
                                    <input type="text" class="search_type" name="s_region" value="" hidden>
                                    <ul class="drop-search-region"></ul>
                                </div>
                            </div>
                            <button type="submit" class="btn btn--def"><span>ШУКАТИ</span></button>
                        </div>
                    </form>
                </div>
                <div class="callboard__photo">
                    <img src="{{ asset('public/frontend/img/illust.jpeg') }}" alt="">
                </div>
            </div>
            <div class="fresh-info fresh-info--mt">
                <div class="fresh-info__card">
                    <div class="fresh-info__value">
                        <p>{{ $customer }}</p>
                    </div>
                    <div class="signature signature--mt">
                        <p>користувачів</p>
                    </div>
                </div>
                <div class="fresh-info__card">
                    <div class="fresh-info__value">
                        <p>{{ $advert }}</p>
                    </div>
                    <div class="signature signature--mt">
                        <p>всього оголошень</p>
                    </div>
                </div>
                <div class="fresh-info__card">
                    <div class="fresh-info__value">
                        <p>+ {{ $advert_today }}</p>
                    </div>
                    <div class="signature signature--mt">
                        <p>додано сьогодні</p>
                    </div>
                </div>
            </div>
        </div>
	</section>
	@if($top_sale_adverts != '[]')
	<section class="top-adverts">
		<div class="container">
			<div id="top-adverts-sale" class="top-adverts__wrap">
				<div class="top-adverts__wrap-title">
					<h2 class="title title--sub top-adverts__title">Топ оголошень з продажу</h2>
					<div class="text">
						<a href="#" class="top-adverts__link top-adverts__link--sale text text--turquoise">Переглянути всі оголошення</a>
					</div>
				</div>
				<div class="ads ads--long">
					<div class="ads__wrap ads__wrap--mtBig">
						@foreach($top_sale_adverts as $similar_advert)
							<div class="ads__cardContent @if($similar_advert->top != NULL && $similar_advert->top != '0000-00-00 00:00:00') active @endif @if($similar_advert->red != NULL && $similar_advert->red != '0000-00-00 00:00:00') allotment @endif">
								<a href="/advert/id={{ $similar_advert->id }}" class="ads__card">
									<div class="ads__cardWrap">
										<div class="ads__allotment">
											<p>top</p>
										</div>
										<div class="ads__addressInfoWrap">
											<div class="ads__addressInfo">
												<div class="ads__section ads__section--first">
													<div class="text text--gray text--normal">
														<p>Регіон</p>
													</div>
													<div class="text text--black">
														<p>{{ $similar_advert->region }}</p>
													</div>
												</div>
												<div class="ads__section">
													<div class="text text--gray text--normal">
														<p>Район</p>
													</div>
													<div class="text text--black">
														<p>{{ $similar_advert->district }}</p>
													</div>
												</div>
											</div>
										</div>
										<div class="ads__section">
											<div class="text text--gray text--normal">
												<p>Площа, Га</p>
											</div>
											<div class="text text--black">
												@if($similar_advert->type == 'Продаж')
													<p>{{ $similar_advert->ga}}</p>
												@else
													<p>{{ $similar_advert->ga_to_sell }}</p>
												@endif
											</div>
										</div>
										<div class="ads__priceInfo">
											<div class="ads__section">
												<div class="text text--gray text--normal">
													<p>Ціна за 1 Га</p>
												</div>
												<div class="text text--black">
													<p><span class="js-price-num">{{ $similar_advert->price_per_ga }}</span>
														грн.</p>
												</div>
											</div>
											<div class="ads__section">
												<div class="text text--gray text--normal">
													<p>Ціна за все</p>
												</div>
												<div class="text text--black">
													<p><span class="js-price-num">{{ $similar_advert->price }}</span> грн.
													</p>
												</div>
											</div>
											<div class="ads__section ads__section--extra flex-cont">
												@if($similar_advert->discount == 1)
													<div class="extra-card">
														<img
																src="{{ asset('public/frontend/img/svg/Auction.svg') }}"
																alt="">
														<div class="extra-card__inform">
															<div class="text text--mini">
																<p>Можливий торг</p>
															</div>
														</div>
													</div>
												@endif
												@if($similar_advert->attr_5 != '')
													<div class="extra-card">
														<img src="{{ asset('public/frontend/img/svg/house.svg') }}"
															alt="">
														<div class="extra-card__inform">
															<div class="text text--mini">
																<p>Є споруди</p>
															</div>
														</div>
													</div>
												@endif
												<div class="viewers">
													<div class="viewers__icon">
														<img src="{{ asset('public/frontend/img/svg/eye.svg') }}"
															alt="">
													</div>
													<div class="viewers__value">
														<div class="text text--gray text--normal">
															<p>@if($similar_advert->views != 0) {{ $similar_advert->views }} @else
																	0 @endif</p>
														</div>
													</div>
												</div>
											</div>
											<div class="ads__section ads__section--date">
												<div class="text text--gray text--normal">
													<p>Дата публікації</p>
												</div>
												<div class="text text--black">
													<p>{{ date('d.m.Y',strtotime($similar_advert->created_at)) }}</p>
												</div>
											</div>
										</div>
									</div>
								</a>
								<div class="ads__section ads__section--star js-favorite"
									data-advert-id="{{ $similar_advert->id }}">
									<i class="icon-star-outline">
										<img src="{{ asset('public/frontend/img/svg/star-outline.svg') }}" alt="">
									</i>
									<i class="icon-star">
										<img src="{{ asset('public/frontend/img/svg/star.svg') }}" alt="">
									</i>
								</div>
							</div>
						@endforeach
					</div>
					<a href="#" class="btn btn--show-adverts">Переглянути всі оголошення</a>
				</div>
			</div>
		</div>
	</section>
	@endif
	@if($top_rent_adverts != '[]')
	<section class="top-adverts">
		<div class="container">
			<div id="top-adverts-rent" class="top-adverts__wrap">
				<div class="top-adverts__wrap-title">
					<h2 class="title title--sub top-adverts__title">Топ оголошень з оренди</h2>
					<div class="text">
						<a href="/search" class="top-adverts__link text text--turquoise">Переглянути всі оголошення</a>
					</div>
				</div>
				<div class="ads ads--long">
					<div class="ads__wrap ads__wrap--mtBig">
						@foreach($top_rent_adverts as $similar_advert)
							<div class="ads__cardContent @if($similar_advert->top != NULL && $similar_advert->top != '0000-00-00 00:00:00') active @endif @if($similar_advert->red != NULL && $similar_advert->red != '0000-00-00 00:00:00') allotment @endif">
								<a href="/advert/id={{ $similar_advert->id }}" class="ads__card">
									<div class="ads__cardWrap">
										<div class="ads__allotment">
											<p>top</p>
										</div>
										<div class="ads__addressInfoWrap">
											<div class="ads__addressInfo">
												<div class="ads__section ads__section--first">
													<div class="text text--gray text--normal">
														<p>Регіон</p>
													</div>
													<div class="text text--black">
														<p>{{ $similar_advert->region }}</p>
													</div>
												</div>
												<div class="ads__section">
													<div class="text text--gray text--normal">
														<p>Район</p>
													</div>
													<div class="text text--black">
														<p>{{ $similar_advert->district }}</p>
													</div>
												</div>
											</div>
										</div>
										<div class="ads__section">
											<div class="text text--gray text--normal">
												<p>Площа, Га</p>
											</div>
											<div class="text text--black">
												@if($similar_advert->type == 'Продаж')
													<p>{{ $similar_advert->ga}}</p>
												@else
													<p>{{ $similar_advert->ga_to_sell }}</p>
												@endif
											</div>
										</div>
										<div class="ads__priceInfo">
											<div class="ads__section">
												<div class="text text--gray text--normal">
													<p>Ціна за 1 Га</p>
												</div>
												<div class="text text--black">
													<p><span class="js-price-num">{{ $similar_advert->price_per_ga }}</span>
														грн.</p>
												</div>
											</div>
											<div class="ads__section">
												<div class="text text--gray text--normal">
													<p>Ціна за все</p>
												</div>
												<div class="text text--black">
													<p><span class="js-price-num">{{ $similar_advert->price }}</span> грн.
													</p>
												</div>
											</div>
											<div class="ads__section ads__section--extra flex-cont">
												@if($similar_advert->discount == 1)
													<div class="extra-card">
														<img
																src="{{ asset('public/frontend/img/svg/Auction.svg') }}"
																alt="">
														<div class="extra-card__inform">
															<div class="text text--mini">
																<p>Можливий торг</p>
															</div>
														</div>
													</div>
												@endif
												@if($similar_advert->attr_5 != '')
													<div class="extra-card">
														<img src="{{ asset('public/frontend/img/svg/house.svg') }}"
															alt="">
														<div class="extra-card__inform">
															<div class="text text--mini">
																<p>Є споруди</p>
															</div>
														</div>
													</div>
												@endif
												<div class="viewers">
													<div class="viewers__icon">
														<img src="{{ asset('public/frontend/img/svg/eye.svg') }}"
															alt="">
													</div>
													<div class="viewers__value">
														<div class="text text--gray text--normal">
															<p>@if($similar_advert->views != 0) {{ $similar_advert->views }} @else
																	0 @endif</p>
														</div>
													</div>
												</div>
											</div>
											<div class="ads__section ads__section--date">
												<div class="text text--gray text--normal">
													<p>Дата публікації</p>
												</div>
												<div class="text text--black">
													<p>{{ date('d.m.Y',strtotime($similar_advert->created_at)) }}</p>
												</div>
											</div>
										</div>
									</div>
								</a>
								<div class="ads__section ads__section--star js-favorite"
									data-advert-id="{{ $similar_advert->id }}">
									<i class="icon-star-outline">
										<img src="{{ asset('public/frontend/img/svg/star-outline.svg') }}" alt="">
									</i>
									<i class="icon-star">
										<img src="{{ asset('public/frontend/img/svg/star.svg') }}" alt="">
									</i>
								</div>
							</div>
						@endforeach
					</div>
					<a href="#" class="btn btn--show-adverts">Переглянути всі оголошення</a>
				</div>
			</div>
		</div>
	</section>
	@endif
    <section class="news-section dark-section">
        <div class="container">
            <div class="title title--sub flex-cont justify-sb align-center">
                <p>Новини</p>
                <div class="slider-control slider-control--hover">
                    <img class="prev revert" src="{{ asset('public/frontend/img/svg/arrow-black.svg') }}" alt="">
                    <img class="next" src="{{ asset('public/frontend/img/svg/arrow-black.svg') }}" alt="">
                </div>
            </div>
            <div class="news-section__swiper">
                <div class="swiper-wrapper">
                    @foreach($articles as $article)
                        <div class="swiper-slide">
                            <div class="news-section__date">
                                <p>{{ date('d-m-Y', strtotime($article->created_at)) }}</p>
                            </div>
                            <a href="/{{ $article->slug }}" class="news-section__title">{{ $article->title }}</a>
                            <div class="news-section__content">
                                <p>
                                    {{ $article->small_description }}
                                </p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection
