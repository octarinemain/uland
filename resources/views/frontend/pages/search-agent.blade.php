@extends('frontend.includes.header')

@section('content')
    <section class="search-agent">
        <div class="search-agent__info">
            <div class="container">
                <div class="search-agent__formWrap">
                    <div class="search-agent__form">
                        <div class="title title--sub">
                            <p>Пошук агента</p>
                        </div>
                        <form class="ajax-searchAgent-form get-api-region search-agent">
                            <div class="search-agent__content">
                                <div class="input-group">
                                    <label for="searchRegionArent">Регіон</label>
                                    <input type="text" id="searchRegionArent" name="region">
                                </div>
                                <button type="submit" class="btn">ШУКАТИ</button>
                            </div>
                        </form>
                    </div>
                    <div class="search-agent__procedureAgent">
                        <div class="title title--mini">
                            <p>Порядок роботи з агентом:</p>
                        </div>
                        <div class="search-agent__procedureCards">
                            <div class="search-agent__procedureCard">
                                <div class="search-agent__procedureNum"></div>
                                <p class="text text--blackColor text--miniPlus">Виберіть відповідного Вам агента</p>
                            </div>
                            <div class="search-agent__procedureCard">
                                <div class="search-agent__procedureNum"></div>
                                <p class="text text--blackColor text--miniPlus">Напишіть йому і узгодьте умови роботи</p>
                            </div>
                            <div class="search-agent__procedureCard">
                                <div class="search-agent__procedureNum"></div>
                                <p class="text text--blackColor text--miniPlus">Виберіть агента для свого оголошення, перейшовши на сторінку агента.</p>
                            </div>
                            <div class="search-agent__procedureCard">
                                <div class="search-agent__procedureNum"></div>
                                <p class="text text--blackColor text--miniPlus">Успішної  Вам роботи!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="search-agent__wrap">
                <div class="title title--sub">
                    <p>Всі агенти</p>
                    <div class="signature signature--mtBig">
                        <p>
                            знайдено <span class="js-quantity-search"></span>
                        </p>
                    </div>
                </div>
                <div class="search-agent__wrapCard"></div>
                <div class="btn-wrap btn-wrap--mtMedium js-loadMoreWrap preload">
                    <div class="btn btn--def btn--loadMore js-loadMore">
                        <div class="btn__icon">
                            <img src="{{ asset('public/frontend/img/svg/refresh.svg') }}" alt="">
                        </div>
                        <p>Показати ще</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
