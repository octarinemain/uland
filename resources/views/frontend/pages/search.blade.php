@extends('frontend.includes.header')

@section('content')
    <section class="search get-api-region">
        <div class="search__main">
            <div class="container">
                <div class="search__mainWrap">
                    <div class="search__form">
                        <div class="search__formMainWrap">
                            <div class="title title--sub">
                                <p>Пошук оголошення</p>
                            </div>
                            <form class="ajax-search-form">
                                <div class="search__formWrap">
                                    <div class="radio-wrap flex-cont align-center js-submit-radio-search">
                                        <label class="radio  @if(isset($type) && $type == 'Оренда') checked @endif @if(!isset($type)) checked @endif">
                                            <input type="radio"   @if(isset($type) && $type == 'Оренда') checked="" @endif @if(!isset($type)) checked="" @endif value="Оренда" name="type" id="rent">
                                            <div class="radio__text">Оренда</div>
                                        </label>
                                        <label class="radio @if(isset($type) && $type == 'Продаж') checked @endif" >
                                            <input @if(isset($type) && $type == 'Продаж') checked="" @endif type="radio" value="Продаж" name="type" id="sale">
                                            <div class="radio__text">Продаж</div>
                                        </label>
                                    </div>

                                    <div class="input-group input-group--clear input-group--mtBig input-group--searchRegion @if(isset($region)) focus @endif">
                                        <label for="searchRegion">Регіон</label>
                                        <input @if(isset($region)) value="{{ $region }}" @endif type="text"
                                               class="searchRegion"
                                               id="searchRegion"
                                               name="row"
                                               autocomplete="off"
                                        >
                                        <div class="input-group__clear">
                                            <img src="{{ asset('public/frontend/img/svg/close.svg') }}" alt="">
                                        </div>
                                        <input type="text" class="search_type" name="s_city" @if(isset($city)) value="{{ $city }}" @endif hidden>
                                        <input type="text" class="search_type" name="s_district" @if(isset($district)) value="{{ $district }}" @endif hidden>
                                        <input type="text" class="search_type" name="s_region" @if(isset($hidden_region)) value="{{ $hidden_region }}" @endif hidden>
                                        <ul class="drop-search-region"></ul>
                                    </div>
                                    <div class="helper helper--mtBig helper--flex helper--flexStart helper--ga">
                                        <div class="input-group">
                                            <label for="minGa">Мін. га</label>
                                            <input type="number" id="minGa" name="minGa">
                                        </div>
                                        <div class="input-group">
                                            <label for="maxGa">Макс. Га</label>
                                            <input type="number" id="maxGa" name="maxGa">
                                        </div>
                                    </div>
                                    <div class="helper helper--mtBig helper--flex helper--flexStart helper--price">
                                        <div class="input-group">
                                            <label for="minPriceGa">Мін. ціна ділянки</label>
                                            <input type="text" pattern="^[ 0-9]+$" id="minPriceGa" name="minPrice">
                                        </div>
                                        <div class="input-group">
                                            <label for="maxPriceGa">Макс. ціна ділянки</label>
                                            <input type="text" pattern="^[ 0-9]+$" id="maxPriceGa" name="maxPrice">
                                        </div>
                                    </div>
                                    <div class="input-group input-group--mtBig input-group--cadNumSearch">
                                        <label for="cadNumSearch">Кадастровий номер</label>
                                        <input type="text" id="cadNumSearch" name="cadnumSearch" @if(isset($cadnum)) value="{{ $cadnum }}" @endif  maxlength="22">
                                    </div>
                                    <div class="form-send form-send--mt flex-cont form-send--end">
                                        <button type="submit" class="btn">ШУКАТИ</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="search__toggleMap">
                            <img src="{{ asset('public/frontend/img/svg/arrow-black.svg') }}" alt="">
                            <p class="text text--min text--blackColor text--ttu text--bold">Фільтр</p>
                        </div>
                    </div>
                    <div class="search__mapWrap">
                        <div class="search__advert">
                            <div class="search__advertClose">
                                <img src="{{ asset('public/frontend/img/svg/close.svg') }}" alt="">
                            </div>
                            <div class="flex-cont align-center">
                                <div class="search__advertStar js-favorite" data-advert-id="">
                                    <i class="icon-star-outline">
                                        <img src="{{ asset('public/frontend/img/svg/star-outline.svg') }}" alt="">
                                    </i>
                                    <i class="icon-star">
                                        <img src="{{ asset('public/frontend/img/svg/star.svg') }}" alt="">
                                    </i>
                                </div>
                                <div class="extra-card extra-card--min" id="discount">
                                    <img src="{{ asset('public/frontend/img/svg/Auction.svg') }}"
                                         alt="">
                                    <div class="extra-card__inform">
                                        <div class="text text--mini">
                                            <p>Можливий торг</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="extra-card extra-card--min" id="attr_5">
                                    <img src="{{ asset('public/frontend/img/svg/house.svg') }}" alt="">
                                    <div class="extra-card__inform">
                                        <div class="text text--mini">
                                            <p>Є споруди</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="viewers">
                                    <div class="viewers__icon">
                                        <img src="{{ asset('public/frontend/img/svg/eye.svg') }}"
                                             alt="">
                                    </div>
                                    <div class="viewers__value">
                                        <div class="text text--gray text--normal">
                                            <p id="js-search_viewers"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="search__advertCard">
                                <div class="text text--gray">
                                    <p>Регіон</p>
                                </div>
                                <div class="text text--blackColor">
                                    <p id="js-search_region"></p>
                                </div>
                            </div>
                            <div class="search__advertCard">
                                <div class="text text--gray">
                                    <p>Район</p>
                                </div>
                                <div class="text text--blackColor">
                                    <p id="js-search_district"></p>
                                </div>
                            </div>
                            <div class="search__advertCard">
                                <div class="text text--gray">
                                    <p>Площа</p>
                                </div>
                                <div class="text text--blackColor">
                                    <p id="js-search_ga"></p>
                                </div>
                            </div>
                            <div class="search__advertCard">
                                <div class="text text--gray">
                                    <p>Ціна</p>
                                </div>
                                <div class="text text--blackColor">
                                    <p id="js-search_price_per_ga"></p>
                                </div>
                            </div>
                            <div class="search__advertCard">
                                <div class="text text--gray">
                                    <p>Загальна</p>
                                </div>
                                <div class="text text--blackColor">
                                    <p id="js-search_price"></p>
                                </div>
                            </div>
                            <div class="text">
                                <a href="/" class="text__link text__link--arrowRight" target="_blank"
                                   id="js-search_link">
                                    <span></span>
                                    <img class="revert" src="{{ asset('public/frontend/img/svg/arrow-back.svg') }}" alt="">
                                </a>
                            </div>
                        </div>
                        <div id="search__map" class="search__map"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="search__result">
                <div class="title title--sub">
                    <p>Всі оголошення</p>
                    <div class="signature signature--mtBig">
                        <p>
                            знайдено <span class="js-quantity-search"></span>
                        </p>
                    </div>
                </div>
                <div class="ads ads--long">
                    <div class="ads__wrap ads__wrap--mtBig"></div>
                    <div class="btn-wrap btn-wrap--mtMedium js-loadMoreWrap preload">
                        <div class="btn btn--def btn--loadMore js-loadMore">
                            <div class="btn__icon">
                                <img src="{{ asset('public/frontend/img/svg/refresh.svg') }}" alt="">
                            </div>
                            <p>Показати ще</p>
                        </div>
                    </div>
                </div>
			</div>
			<div id="similar-result" class="search__similar-result">
                <div class="title title--sub">
                    <p>Схожі оголошення</p>
                </div>
                <div class="ads ads--long">
					<div class="ads__wrap-similar"></div>
                </div>
            </div>
        </div>
    </section>
@endsection
