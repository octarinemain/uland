@extends('frontend.includes.header')

@section('content')
    <section class="registration section-dark">
        <div class="container">
            <div class="registration__form registration__form--big">
                <div class="title title--sub title--center">
                    <p>Ви успішно закріпили агента!</p>
                </div>
                <div class="text text--center text--mtBig">
                    <p>
                        Тепер Вашим оголошенням буде керувати агент, якщо виникнуть якісь непорозуміння ви завжди можете звернутися в службу підтримки.
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection
