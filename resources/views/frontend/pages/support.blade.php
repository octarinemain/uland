@extends('frontend.includes.header')

@section('content')
    <section class="come-back dark-section">
        <div class="container">
            <div class="text">
                <a href="/" class="text__link text__link--arrow">
                    <img class="hover-off" src="{{ asset('public/frontend/img/svg/arrow-back.svg') }}" alt="">
                    <img class="hover-on" src="{{ asset('public/frontend/img/svg/arrow-back-y.svg') }}" alt="">
                    <span>Повернутися на головну</span>
                </a>
            </div>
        </div>
    </section>
    <section class="support">
        <div class="container container--min">
            <div class="title title--sub title--center">
                @if (Session::has('success'))
                    <h1>Ваше повідомлення було успішно відправлено!</h1>
                @else
                    <h1>Повідомити про проблему</h1>
                @endif
            </div>
            @if (Session::has('success'))
            @else
                <div class="support__form">
                <form method="POST" action="{{ route('support.submit') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="support__selectPost">
                        <div class="text text--gray">
                            <p>Ви хочете звернутися в</p>
                        </div>
                        <label class="radio checked">
                            <input type="radio" value="Тех підтримку" name="type" id="sale" checked="">
                            <div class="radio__text">Тех підтримку</div>
                        </label>
                        <label class="radio">
                            <input type="radio" value="Відділ по рекламі" name="type" id="advertising">
                            <div class="radio__text">Відділ по рекламі</div>
                        </label>
                    </div>
                    <div class="support__person">
                        <div class="input-group">
                            <label for="name">Ваше ім'я</label>
                            <input required type="text" name="name" id="name">
                        </div>
                        <div class="input-group">
                            <label for="email">Ваш Email</label>
                            <input required type="email" id="email" name="email">
                        </div>
                    </div>
                    <div class="support__msg">
                        <div class="text text--gray">
                            <p>Детально опишіть проблему, з якою Ви зіткнулися.</p>
                        </div>
                        <div class="support__msg-wrap">
                            <textarea name="description" placeholder="Ваше повідомлення..."></textarea>
                            <label class="support__addFile">
                                <input name="file" type="file">
                                <img src="{{ asset('public/frontend/img/svg/attach.svg') }}" alt="">
                                <span>
                                    <div class="error-custom">Не більше 1 файлу (10 MB)</div>
                                </span>
                            </label>
                        </div>
                        <div class="form-send form-send--mt form-send--start">
                            <div class="support__addingFiles"></div>
                            <button type="submit" class="btn">НАДІСЛАТИ</button>
                        </div>
                    </div>
                </form>
            </div>
            @endif
        </div>
    </section>
@endsection
