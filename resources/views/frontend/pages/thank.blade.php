@extends('frontend.includes.header')

@section('content')
    <section class="registration section-dark">
        <div class="container">
            <div class="registration__form registration__form--big">
                <div class="title title--sub title--center">
                    <p>Оплата пройшла успішно</p>
                </div>
                <div class="text text--center text--mtBig">
                    <p>
                        Ваше оголошення рекламується
                    </p>
                    <a class="text__link text__link--arrow text__link--mtBig text__link--hover" href="/customer/profile">
                        <img class="hover-off" src="{{ asset('public/frontend/img/svg/arrow-back.svg') }}" alt="">
                        <img class="hover-on" src="{{ asset('public/frontend/img/svg/arrow-back-y.svg') }}" alt="">
                        <span>Увійти до Особистого кабінету</span>
                    </a>
                </div>
            </div>
        </div>
    </section>
@endsection
