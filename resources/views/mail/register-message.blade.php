<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=display-width, initial-scale=1.0, maximum-scale=1.0,">
    <title>Uland</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
    <style type="text/css">
        html {
            width: 100%;
            background-color: transparent;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            -webkit-text-size-adjust: none;
            -ms-text-size-adjust: none;
            font-family: 'Roboto', sans-serif;
        }

        img {
            border: 0;
            -ms-interpolation-mode: bicubic;
        }

        a {
            color: #9596A4;
            text-decoration: underline;
            white-space: nowrap;
        }

        a:hover {
            text-decoration: underline;
        }
    </style>
    <!--[if mso]>
    <style>
        .MsoNormal {
            font-family: Arial, Helvetica Neue, Helvetica, sans-serif !important;
        }

        .heading {
            font-family: Arial, Helvetica Neue, Helvetica, sans-serif !important;
        }
    </style>
    <![endif]-->
</head>
<body>

<table bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 650px; margin: 0 auto;">
    <tbody>
    <tr>
        <td align="center">
            <table bgcolor="#FFC000" border="0" class="display-width" cellpadding="0" cellspacing="0"
                   style="width: 100%; text-align: center;">
                <tbody>
                <tr>
                    <td style="padding: 20px 0;">
                        <a href="https://uland.com.ua" style="width: 100%;">
                            <img src="https://uland.com.ua/public/frontend/img/logo.png" alt="" width="165"
                                 style="margin: 0 auto;">
                        </a>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center">
            <table border="0" class="display-width" cellpadding="0" cellspacing="0"
                   style="width: 100%; text-align: center; padding: 50px 0;">
                <tbody>
                <tr>
                    <td>
                        <img src="https://uland.com.ua/public/frontend/img/letter.jpeg" alt="" width="115"
                             height="115" style="margin: 0 auto;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <p style="font-size: 26px; font-weight: bold;">Дякуємо за реєстрацію!</p>
                    </td>
                </tr>
                <tr>
                    <td style="color: #9596A4; font-size: 15px;">
                        <p>Для підтвердження Вашої електронної пошти клацніть на кнопку нижче</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="{{ $link }}" style="display: inline-block; padding: 15px 40px; color: #000; border-radius: 30px; background-color: #ffc000; font-size: 13px; font-weight: bold; margin-top: 20px; text-decoration: none">ПІДТВЕРДИТИ</a>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center">
            <table bgcolor="#F3F6F8" border="0" class="display-width" cellpadding="0" cellspacing="0"
                   style="width: 100%; text-align: center; padding: 25px 0 30px;">
                <tbody>
                <tr>
                    <td style="text-align: center;">
                        <a href="/" style="display: inline-block;">
                            <img src="https://uland.com.ua/public/frontend/img/fb.png" alt="" width="35"
                                 height="35">
                        </a>
                        <a href="/" style="display: inline-block; margin-left: 10px;">
                            <img src="https://uland.com.ua/public/frontend/img/google +.png" alt="" width="35"
                                 height="35">
                        </a>
                        <a href="/" style="display: inline-block; margin-left: 10px;">
                            <img src="https://uland.com.ua/public/frontend/img/twitter.png" alt="" width="35"
                                 height="35">
                        </a>
                    </td>
                </tr>
                <tr>
                    <td style="color: #9596A4; font-size: 13px;">
                        <p>© 2020 ULAND. Якщо у Вас виникли питання, зверніться в <a href="https://uland.com.ua/support">
                                техпідтримку.
                            </a></p>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>
