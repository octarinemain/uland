<?php

Route::get('/', 'Frontend\PageController@home');
Route::get('/articles', 'Frontend\PageController@articles');
Route::get('/support', 'Frontend\PageController@support');
Route::post('/support', 'Frontend\SupportController@create')->name('support.submit');

Auth::routes();

Route::get('/customer/login', 'Auth\CustomerLoginController@showLoginForm')->name('customer.login');
Route::post('/login', 'Auth\CustomerLoginController@login')->name('customer.login.submit');

/*
 * Register steps
 */
Route::get('/register-step-1', 'Frontend\PageController@registerStepOne');
Route::post('/register-step-1', 'Frontend\Customer\CustomerController@register_step_1');
Route::post('/reSend', 'Frontend\Customer\CustomerController@reSend');
Route::post('/getShortData', 'Frontend\Customer\CustomerController@getShortData');

Route::get('/register-step-2', 'Frontend\PageController@registerStepTwo');
Route::post('/register-step-2', 'Frontend\Customer\CustomerController@register_step_2');

Route::get('/register-step-3', 'Frontend\PageController@registerStepThree');
Route::post('/register-step-3', 'Frontend\Customer\CustomerController@create')->name('customer.register');
Route::post('/search', 'Frontend\Customer\AdvertController@search');
Route::get('/arenda-zemli/{area}', 'Frontend\PageController@seoSearchArenda');
Route::get('/prodaja-zemli/{area}', 'Frontend\PageController@seoSearchProdaja');
Route::get('/successful-register', 'Frontend\PageController@successfulRegister');

Route::post('/search-agent', 'Frontend\Customer\CustomerController@searchAgent');
Route::get('/search-agent', 'Frontend\PageController@searchAgent');

/* End Register */
Route::get('/thank', 'Frontend\PageController@thank');

/*
 * Reset passwords steps
 */
Route::get('/reset-step-1', 'Frontend\PageController@resetStepOne');
Route::post('/reset-step-1', 'Frontend\Customer\CustomerController@reset_step_1');
Route::post('/reSendResetToken', 'Frontend\Customer\CustomerController@reSendResetToken');

Route::get('/reset-step-2', 'Frontend\PageController@resetStepTwo');
Route::post('/reset-step-2', 'Frontend\Customer\CustomerController@reset_step_2');

Route::get('/reset-step-3', 'Frontend\PageController@resetStepThree');
Route::post('/reset-step-3', 'Frontend\Customer\CustomerController@reset_step_3');
Route::get('/successful-reset', 'Frontend\PageController@successfulReset');

Route::get('/customer/token={token}', 'Frontend\Customer\CustomerController@activateEmail');
Route::get('/advert/id={id}', 'Frontend\Customer\AdvertController@get');
Route::get('/customer/id={id}', 'Frontend\Customer\CustomerController@get');
Route::get('/search', 'Frontend\PageController@search');

Route::post('/search-home', 'Frontend\PageController@searchHome')->name('search-home.submit');


/* End Reset steps */

/*
 *  Payment
 */

Route::post('/checkPacketPayment', 'Frontend\Customer\AdvertController@checkPacketPayment');
Route::post('/checkCustomPacketPayment', 'Frontend\Customer\AdvertController@checkCustomPacketPayment');

/* End Payment steps */


/*
 *  Routes for maps
 */
Route::post('/map/regions', 'Frontend\PageController@mapRegions');
Route::post('/map/getAdvert', 'Frontend\Customer\AdvertController@getMapAdvert');
/* End maps routes */



Route::post('/search/get-similar-advert', 'Frontend\Customer\AdvertController@getSimilarAdvert');

Route::post('/customer/getCoordinateInfo', 'Frontend\Customer\AdvertController@getCoordinateInfo');

Route::group(['middleware' => ['customer']], function () {

    Route::prefix('customer')->group(function () {

        Route::get('/profile', 'Frontend\PageController@profile');
        Route::get('/edit', 'Frontend\PageController@edit');
        Route::post('/edit-customer', 'Frontend\Customer\CustomerController@edit');

        Route::get('/edit-password', 'Frontend\PageController@editPassword');
        Route::post('/edit-password', 'Frontend\Customer\CustomerController@editPassword');
        Route::post('/reSend-email-activate-link', 'Frontend\Customer\CustomerController@reSendActivateMail');

        /*
         * Adverts
         */
        Route::get('/check-cadnum', 'Frontend\PageController@checkCadnum');
        Route::post('/check-cadnum', 'Frontend\Customer\AdvertController@checkCadnum');
        Route::post('/send-message-to-owner', 'Frontend\Customer\AdvertController@sendMessagetoOwner');

        Route::get('/error-cadnum', 'Frontend\PageController@errorCadnum');
        Route::get('/agent-message-cadnum', 'Frontend\PageController@agentMessageCadnum');
        Route::get('/agent-confirm/advert-id={id}&agent-id={agent_id}&x={x}', 'Frontend\PageController@agentConfirm');

        Route::get('/agent-isset-cadnum', 'Frontend\PageController@agentIssetCadnum');
        Route::get('/owner-message-cadnum', 'Frontend\PageController@ownerMessageCadnum');
        Route::get('/create-advert', 'Frontend\PageController@createAdvert');

        Route::post('/create-advert', 'Frontend\Customer\AdvertController@create');
        Route::get('/successful-publish', 'Frontend\PageController@successfulPublish');
        Route::get('/edit-advert/id={id}', 'Frontend\PageController@editAdvert');

        Route::post('/edit-advert', 'Frontend\Customer\AdvertController@edit');
        Route::post('/delete-photo', 'Frontend\Customer\AdvertController@deletePhoto');
        Route::post('/deactivate', 'Frontend\Customer\AdvertController@deactivate');
        Route::post('/deactivate-and-selling', 'Frontend\Customer\AdvertController@deactivateAndSelling');
        Route::post('/activate', 'Frontend\Customer\AdvertController@activate');
        Route::get('/delete/id={id}', 'Frontend\Customer\AdvertController@delete');
        Route::get('/unfasten-agent/id={id}', 'Frontend\Customer\AdvertController@unfastenAgent');
        Route::get('/unfasten-owner/id={id}', 'Frontend\Customer\AdvertController@unfastenOwner');

        Route::get('/agent-confirm-cadnum', 'Frontend\PageController@agentConfirmCadnum');
        Route::get('/owner-confirm-cadnum', 'Frontend\PageController@ownerConfirmCadnum');
        Route::get('/agent-confirm-cadnum-with-advertising', 'Frontend\PageController@agentConfirmCadnumWithAdvertising');
        Route::get('/owner-confirm-cadnum-with-advertising', 'Frontend\PageController@ownerConfirmCadnumWithAdvertising');
        Route::post('/set-agent', 'Frontend\Customer\AdvertController@setAgent');
        Route::post('/set-owner', 'Frontend\Customer\AdvertController@setOwner');

        /*
         * Favorites
         */
        Route::post('/create-favorite', 'Frontend\Customer\AdvertController@createFavorite');
        Route::post('/delete-favorite', 'Frontend\Customer\AdvertController@deleteFavorite');

        Route::get('/favorites', 'Frontend\PageController@favorites');

        Route::get('/advertise/id={id}', 'Frontend\PageController@advertise');

        Route::get('/advertise/id={id}/type={type}',  'Frontend\Customer\AdvertController@payType');
        Route::post('/custom-type',  'Frontend\Customer\AdvertController@payCustomType');

    });
});

Route::prefix('admin')->group(function () {
    Route::get('/', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::group(['middleware' => ['admin']], function () {

        /*
        * Advert
        */
        Route::post('/adverts', function () {
            $adverts = App\Models\Advert::orderBy('id', 'DESC')->take(100)->get();
            $adverts->load('customer');
            $adverts->load('agent_customer');
            return $adverts;
        });
        Route::post('/advert/get', function (\Illuminate\Http\Request $request) {
            $advert = App\Models\Advert::with(['customer', 'agent_customer'])->find($request->id);
            return $advert;
        });
        Route::post('/regions', function () {
            $data = App\Models\Advert::select('region')->distinct()->get();
            $regions = [];
            foreach($data as $region){
                $new_data = (object)[];
                $new_data->label = $region->region;
                $new_data->value = $region->region;
                array_push($regions, $new_data);
            }
            return $regions;
        });

        Route::get('/adverts', function () {
            return view('admin.advert-list');
        });

        Route::get('/advert-{id}', function ($id) {
            return view('admin.advert', ['advert_id' => $id]);
        });

        Route::get('/advertAdvertising-{id}', function ($id) {
            return view('admin.advert-advertising', ['advert_id' => $id]);
        });

        Route::post('/advert/filter', 'Admin\AdvertController@filter');
        Route::post('/advert/count', 'Admin\AdvertController@getCount');
        Route::post('/advert/update-count', 'Admin\AdvertController@updateCount');
        Route::post('/advert/create', 'Admin\AdvertController@create');
        Route::post('/advert/update-red', 'Admin\AdvertController@updateRed');
        Route::post('/advert/update-order', 'Admin\AdvertController@updateOrder');
        Route::post('/advert/update-top', 'Admin\AdvertController@updateTop');
        Route::post('/unfasten-agent', 'Admin\AdvertController@unfastenAgent');
        Route::post('/unfasten-owner', 'Admin\AdvertController@unfastenOwner');


       /*
      * Customer
      */
        Route::post('/customers', function () {
            $customers = App\Models\Customer::orderBy('id', 'DESC')->get();
            App\Models\Customer::where('is_new', 1)->update([
                'is_new' => 0
            ]);
            return $customers;
        });
        Route::get('/customers', function () {
            return view('admin.customer');
        });
        Route::post('/customer/filter', 'Admin\ACustomerController@filter');
        Route::post('/customer/count', 'Admin\ACustomerController@getCount');


        /*
         * Article
         */
        Route::post('/articles', function () {
            $articles = App\Models\Article::orderBy('id', 'DESC')->get();
            return $articles;
        });
        Route::get('/articles', function () {
            return view('admin.article');
        });
        Route::post('/article/create', 'Admin\ArticleController@create');
        Route::post('/article/delete/id={id}', 'Admin\ArticleController@delete');


        /*
         * Page
         */
        Route::post('/pages', function () {
            $pages = App\Models\Page::orderBy('id', 'DESC')->get();
            return $pages;
        });
        Route::get('/pages', function () {
            return view('admin.page');
        });
        Route::post('/page/create', 'Admin\PageController@create');
        Route::post('/page/delete/id={id}', 'Admin\PageController@delete');

        /*
         * Support
         */
        Route::post('/supports', function () {
            $supports = App\Models\Support::orderBy('id', 'DESC')->get();
            return $supports;
        });
        Route::get('/supports', function () {
            return view('admin.support');
        });
        Route::post('/support/delete/id={id}', 'Admin\SupportController@delete');
        Route::post('/support/create', 'Admin\SupportController@create');


        /*
         * Payment
         */
        Route::post('/payments', function () {
            $payments = App\Models\Payment::orderBy('id', 'DESC')->get();
            return $payments;
        });
        Route::get('/payments', function () {
            return view('admin.payment');
        });

    });
});


Route::get('/{slug}', function ($slug) {
    $page = App\Models\Page::where('slug', $slug)->first();
    $article = App\Models\Article::where('slug', $slug)->first();
    if($page == '' && $article == ''){
        $title = '404';
        $meta_key = '';
        $meta_description = '';
        return view('frontend.pages.404',compact(['title', 'meta_key', 'meta_description']));
    }elseif ($page != ''){
        /*
         * Common page
         */
        $title = $page->title;
        $meta_key = $page->meta_keywords;
        $meta_description = $page->meta_description;
        return view('frontend.pages.common-page')->with(compact(['page', 'title', 'meta_key', 'meta_description']));
    }else{
        /*
         * Article
         */
        $title = $article->name;
        $meta_key = $article->meta_keywords;
        $meta_description = $article->meta_description;
        return view('frontend.pages.article')->with(compact(['article', 'title', 'meta_key', 'meta_description']));
    }

});